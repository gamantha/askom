<?php

namespace app\controllers;

use Yii;
use app\models\Observation;
use app\models\File;
use app\models\Asesor;
use app\models\Data;
use app\models\Result;
use app\models\Assessment;
use app\models\PsikotesResult;
use app\models\RefCompetency;
use app\models\RefGolonganPegawai;
use app\models\RefPsikotesSubtest;
use app\models\PsikotesSubtestResult;
use app\models\ResultCompetency;
use app\models\ObservationGroup;
use app\models\Pegawai;
use app\models\ObservationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\mpdf\Pdf;

/**
 * ObservationController implements the CRUD actions for Observation model.
 */
class ObservationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Observation models.
     * @return mixed
     */
    public function actionIndex($project_id)
    {
        $searchModel = new ObservationSearch();
        $searchparams = Yii::$app->request->queryParams;
        $searchparams["FileSearch"]["project_id"] = $_REQUEST['project_id'] ;
        $dataProvider = $searchModel->search($searchparams);

        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);


        echo 'sada';
    }

    /**
     * Displays a single Observation model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Observation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Observation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Observation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Observation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }




public function actionPrint()
{


 return $this->renderPartial('print');



}


    public function actionProcess_psikotes($id,$aid,$csid)
    {
     $model = File::findOne($id);
     $assessment_id = $aid;
     $competency_set_id = $csid;
     $gol_pegawai_id = 1;


     $inputFileName = \Yii::$app->basePath . '/web/uploads/' . $model->file_location;

     $inputFileType = 'Excel2007';
     $sheetname = 'PSIKOTES';
     $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
      $objReader->setLoadSheetsOnly($sheetname);
     $objPHPExcel = $objReader->load($inputFileName);
     $worksheet = $objPHPExcel->getActiveSheet();


     $highestRow = $worksheet->getHighestRow(); // e.g. 10
     $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
     $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

     $jumlahmejadalamsebaris = Yii::$app->params['settings']['jumlahmejadalamsebaris'];
     $jumlahsubtest = Yii::$app->params['settings']['jumlahsubtest'];
     $kolomawalsubtest = Yii::$app->params['settings']['kolomawalsubtest'];
     $barisawalsubtest = Yii::$app->params['settings']['barisawalsubtest'];


   //  echo '<table>' . "\n";

     for ($row = $barisawalsubtest; $row <= $highestRow; ++$row) {

      $rowofdata = new Data();
      $datastring = '';
      $rowofdata->status = 'unprocessed';
      $rowofdata->tipe = 'psikotes';
      $rowofdata->file_id = $model->id;
      $rowiterator = $worksheet->getRowIterator($row)->current();
      $cellIterator = $rowiterator->getCellIterator();
      $cellIterator->setIterateOnlyExistingCells(false);
      foreach ($cellIterator as $cell) {
       $datastring = $datastring . ',' .  $cell->getValue();
      }
      $rowofdata->data = substr($datastring,1);
      $rowofdata->created_at = date('Y-m-d H:i:s');
      $rowofdata->save();

       //echo '<tr>' . PHP_EOL;
       $mejaawal = 0;
       $mejaakhir = 0;
       $nomeja = '';


       for ($col = 0; $col <= $kolomawalsubtest; ++$col) {
             //  echo '<br/>:';
        if ($col == 0)
        {
        $daerah_asesor_meja = $worksheet->getCellByColumnAndRow($col, $row)->getValue();

         $nomeja =  substr($daerah_asesor_meja, -3, 3);
         $kode_daerah = substr($daerah_asesor_meja, 0,1);
         $kode_asesor = substr($daerah_asesor_meja, 3,1);
        } else if ($col == 1) {
         $nama_peserta = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 2) {
         $nip = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 3) {
         $kodebuku = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 4) {
          $tanggal_tes = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 5) {
           $tanggal_lahir = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 6) {
            $usia = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 7) {
             $rumpun = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 8) {
              $pendidikan = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 9) {
               $jabatan = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 10) {
                $gol_keg_1 = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        } else if ($col == 11) {
                 $gol_keg_2 = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                }
        //echo '<td>' . $worksheet->getCellByColumnAndRow($col, $row)->getValue() .' | </td>' . PHP_EOL;
       }

$observation_group = ObservationGroup::find()
   ->andWhere(['no_meja' => $nomeja])
    ->andWhere(['group_meja' => 'group meja BPS'])
    ->One();

    $observation =  $observation_group->observation_id;

    $psikotes_result = new PsikotesResult;
    $psikotes_result->kode_daerah = $kode_daerah;
    $psikotes_result->no_meja = $nomeja;
    $psikotes_result->nama_peserta = $nama_peserta;
    $psikotes_result->nip = $nip;
    $psikotes_result->kode_buku = $kodebuku;
    $psikotes_result->tanggal_tes = $tanggal_tes;
    $psikotes_result->tanggal_lahir = $tanggal_lahir;
    $psikotes_result->usia = $usia;
    $psikotes_result->rumpun = $rumpun;
    $psikotes_result->pendidikan = $pendidikan;
    $psikotes_result->jabatan = $jabatan;
    $psikotes_result->golkeg1 = $gol_keg_1;
    $psikotes_result->golkeg2 = $gol_keg_2;
    //$asesor = Asesor::find()->andWhere(['kode_asesor' => $kode_asesor])->One();
   $psikotes_result->asesor_id = $kode_asesor;
    $psikotes_result->observation_id = $observation;
    $psikotes_result->save();
            print_r($psikotes_result->getErrors());

$assessment = Assessment::findOne($aid);

$subtests = RefPsikotesSubtest::find()->andWhere(['project_id' => $assessment->project_id])->All();
$indexsubtest = 0;

//echo '<pre>';
//print_r($subtests);
//echo '</pre>';

foreach ($subtests as $subtest){
      echo $subtest->subtest_name;
            echo ' = ';
            echo $psikotes_result->id;
      echo ' : ';


       $col_subtest = $kolomawalsubtest + 1 + (3 * $indexsubtest);
       echo $truecount = $worksheet->getCellByColumnAndRow($col_subtest, $row)->getValue();
        echo $falsecount = $worksheet->getCellByColumnAndRow($col_subtest + 1, $row)->getValue();
        echo $blankcount = $worksheet->getCellByColumnAndRow($col_subtest + 2, $row)->getValue();

       $psikotes_subtest_result = new PsikotesSubtestResult;
       $psikotes_subtest_result->psikotes_result_id = $psikotes_result->id;
       $psikotes_subtest_result->psikotes_subtest_name = $subtest->subtest_name;
              $psikotes_subtest_result->psikotes_project_id = $subtest->project_id;


       if ($subtest->subtest_name != 'kostick') {
                 $psikotes_subtest_result->true_count = $truecount;
                 $psikotes_subtest_result->false_count = $falsecount;
                 $psikotes_subtest_result->blank_count = $blankcount;
                } else if ($subtest->subtest_name == 'kostick')
                {
                 $psikotes_subtest_result->data = $truecount;
                }
       $psikotes_subtest_result->save();
        print_r($psikotes_subtest_result->getErrors());

      $indexsubtest++;
  //     echo $subtest->subtest_name;
       echo '<br/>';

}

}
Yii::$app->getSession()->setFlash('success', 'psikotes di proses');
return $this->redirect(['file/index']);

    }


































public function actionProcess_linkage($id,$aid,$csid)
 {
   $model = File::findOne($id);
   $assessment_id = $aid;
   $competency_set_id = $csid;
   $gol_pegawai_id = 1;
   $inputFileName = \Yii::$app->basePath . '/web/uploads/' . $model->file_location;
   $inputFileType = 'Excel2007';
   $sheetname = 'LINK';
   $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
   $objReader->setLoadSheetsOnly($sheetname);
   $objPHPExcel = $objReader->load($inputFileName);
   $worksheet = $objPHPExcel->getActiveSheet();
   $highestRow = $worksheet->getHighestRow(); // e.g. 10
   $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
   $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
   $jumlahmejadalamsebaris = Yii::$app->params['settings']['jumlahmejadalamsebaris'];
   $jumlahkompetensi = Yii::$app->params['settings']['jumlahkompetensi'];
   $kolomawalkompetensi = Yii::$app->params['settings']['kolomawalkompetensi'];
   $barisawalkompetensi = Yii::$app->params['settings']['barisawalkompetensi'];

   for ($row = $barisawalkompetensi; $row <= $highestRow; ++$row) {
      $rowofdata = new Data();
      $datastring = '';
      $rowofdata->status = 'unprocessed';
      $rowofdata->tipe = 'linkage';
      $rowofdata->file_id = $model->id;
      $rowiterator = $worksheet->getRowIterator($row)->current();
      $cellIterator = $rowiterator->getCellIterator();
      $cellIterator->setIterateOnlyExistingCells(false);
              foreach ($cellIterator as $cell) {
               $datastring = $datastring . ',' .  $cell->getValue();
              }
      $rowofdata->data = substr($datastring,1);
      $rowofdata->created_at = date('Y-m-d H:i:s');
      $rowofdata->save();

       //echo '<tr>' . PHP_EOL;
       $mejaawal = 0;
       $mejaakhir = 0;
       $nomeja = '';
       $kode_daerah='';
       $kode_asesor='';

                       for ($col = 0; $col <= $kolomawalkompetensi; ++$col) {
                        if ($col == 0)
                         $kode_daerah = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        if ($col == 1)
                         $kode_asesor = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        if ($col == 2)
                         $mejaawal = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        if ($col == 3)
                         $mejaakhir = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        //echo '<td>' . $worksheet->getCellByColumnAndRow($col, $row)->getValue() .' | </td>' . PHP_EOL;
                       }

       $asesor = Asesor::find()
       ->andWhere(['id' => $kode_asesor])
       ->One();
       $nomeja = $mejaawal;
       $indexmeja = 0;

       while ($nomeja <= $mejaakhir)
       {
        $assessment_model = Assessment::find()
        ->andWhere(['id' => $assessment_id])
        ->One();

        $competencies_distinct = RefCompetency::find()
        ->andWhere(['competency_set_id' => $competency_set_id])
            ->andWhere(['uselinkage' => 'true'])
        ->select('competency')
        ->orderBy('order')
        ->distinct()
        ->asArray()
        ->All();


        $competencies = RefCompetency::find()
        ->andWhere(['competency_set_id' => $competency_set_id])
        ->andWhere(['uselinkage' => 'true'])
        ->orderBy('order')
        ->asArray()
        ->All();

        //$subcompetency_array = ArrayHelper::map($competencies, 'id', 'sub_competency','competency');
                        $subcompetency_array = ArrayHelper::map($competencies, 'sub_competency','linkage','competency');

    $observation_group = ObservationGroup::find()
       ->andWhere(['no_meja' => $nomeja])
        ->andWhere(['group_meja' => 'group meja BPS'])
        ->one();

        $competency_column_index = 0;
        $knowledge = '';
        $skill = '';
        $sub_competency_index = 0;

        //foreach($competencies_distinct as $competency_distinct)


        foreach($subcompetency_array as $competency_key => $competency_value)
           {
            $col_competency = $kolomawalkompetensi + 1 + (2 * $indexmeja) + ($competency_column_index * $jumlahmejadalamsebaris * 1);
            $cellvalues = $worksheet->getCellByColumnAndRow($col_competency, $row)->getValue();
            $description_values = $worksheet->getCellByColumnAndRow($col_competency + 1, $row)->getValue();
            $competencies_values = $cellvalues;
/*
             echo '<pre>';
                           echo 'competency key: ' . $competency_key;
                                      echo '<br/>value: ' . $cellvalues ;
                           echo '<br/>';
            print_r($competency_value);
             echo '</pre>';
*/
            $competencies_values_length = strlen($competencies_values);
         //   $subcomps = $subcompetency_array[$competency_distinct['competency']];
            $ci = 0;
            foreach($competency_value as $sub_competency_key => $sub_competency_value) {

                        $resultcompetency = new ResultCompetency;
                        $resultcompetency->observation_id = $observation_group->observation_id;
                        $resultcompetency->competency = $competency_key;
                        $resultcompetency->sub_competency = $sub_competency_key;
                        // $resultcompetency->competency_id = $subcomp;

                                              if ($sub_competency_key == 'knowledge') {
                                              $resultcompetency->result =  substr($cellvalues,-2,1);
                                              } else if ($sub_competency_key == 'skill')
                                              {
                                               $resultcompetency->result = substr($cellvalues,-1,1);
                                              } else {
                                                $resultcompetency->result = $cellvalues[$ci];
                                                /**
                                                 DISINI BISA DIPAKAI UNTUK CEK INTEGRITAS HASIL LINKAGE
                                                */
                                               //  $reflinkage = RefCompetency::find()->andWhere(['id' => $subcomp])->One();
                                               //  $linkageresult = ResultCompetency::find()->andWhere(['observation_id'=>$id])->andWhere(['competency_id'=>$reflinkage->linkage])->One();
                                               // $resultcompetency->description  =  $subcomp;
                                               $ci++;
                                              }
                                      /*        echo '<br/>subcomp_key : ' . $sub_competency_key;
                                                echo '<br/>value: ' . $resultcompetency->result ;

                                              echo '<br/>';
*/
                        $resultcompetency->save();
                        }
             $competency_column_index++;
         } // foreach($subcompetency_array as $competency_key => $competency_value)
         $nomeja++;
         $indexmeja++;
        }
       }

       Yii::$app->getSession()->setFlash('success', 'linkage di proses');
       return $this->redirect(['file/index']);
    }  ////////////////////END OF PROCESS_LINKAGE































    /**
     * Finds the Observation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Observation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Observation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }




















    public function Process_linkage($id,$aid,$csid)
     {
       $model = File::findOne($id);
       $assessment_id = $aid;
       $competency_set_id = $csid;
       $gol_pegawai_id = 1;
       $inputFileName = \Yii::$app->basePath . '/web/uploads/' . $model->file_location;
       $inputFileType = 'Excel2007';
       $sheetname = 'LINK';
       $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
       $objReader->setLoadSheetsOnly($sheetname);
       $objPHPExcel = $objReader->load($inputFileName);
       $worksheet = $objPHPExcel->getActiveSheet();
       $highestRow = $worksheet->getHighestRow(); // e.g. 10
       $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
       $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
       $jumlahmejadalamsebaris = Yii::$app->params['settings']['jumlahmejadalamsebaris'];
       $jumlahkompetensi = Yii::$app->params['settings']['jumlahkompetensi'];
       $kolomawalkompetensi = Yii::$app->params['settings']['kolomawalkompetensi'];
       $barisawalkompetensi = Yii::$app->params['settings']['barisawalkompetensi'];

       for ($row = $barisawalkompetensi; $row <= $highestRow; ++$row) {
          $rowofdata = new Data();
          $datastring = '';
          $rowofdata->status = 'unprocessed';
          $rowofdata->tipe = 'linkage';
          $rowofdata->file_id = $model->id;
          $rowiterator = $worksheet->getRowIterator($row)->current();
          $cellIterator = $rowiterator->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false);
                  foreach ($cellIterator as $cell) {
                   $datastring = $datastring . ',' .  $cell->getValue();
                  }
          $rowofdata->data = substr($datastring,1);
          $rowofdata->created_at = date('Y-m-d H:i:s');
          $rowofdata->save();

           //echo '<tr>' . PHP_EOL;
           $mejaawal = 0;
           $mejaakhir = 0;
           $nomeja = '';
           $kode_daerah='';
           $kode_asesor='';

                           for ($col = 0; $col <= $kolomawalkompetensi; ++$col) {
                            if ($col == 0)
                             $kode_daerah = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            if ($col == 1)
                             $kode_asesor = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            if ($col == 2)
                             $mejaawal = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            if ($col == 3)
                             $mejaakhir = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            //echo '<td>' . $worksheet->getCellByColumnAndRow($col, $row)->getValue() .' | </td>' . PHP_EOL;
                           }

           $asesor = Asesor::find()
           ->andWhere(['id' => $kode_asesor])
           ->One();
           $nomeja = $mejaawal;
           $indexmeja = 0;

           while ($nomeja <= $mejaakhir)
           {
            $assessment_model = Assessment::find()
            ->andWhere(['id' => $assessment_id])
            ->One();

            $competencies_distinct = RefCompetency::find()
            ->andWhere(['competency_set_id' => $competency_set_id])
                ->andWhere(['uselinkage' => 'true'])
            ->select('competency')
            ->orderBy('order')
            ->distinct()
            ->asArray()
            ->All();


            $competencies = RefCompetency::find()
            ->andWhere(['competency_set_id' => $competency_set_id])
            ->andWhere(['uselinkage' => 'true'])
            ->orderBy('order')
            ->asArray()
            ->All();

            //$subcompetency_array = ArrayHelper::map($competencies, 'id', 'sub_competency','competency');
                            $subcompetency_array = ArrayHelper::map($competencies, 'sub_competency','linkage','competency');

        $observation_group = ObservationGroup::find()
           ->andWhere(['no_meja' => $nomeja])
            ->andWhere(['group_meja' => 'group meja BPS'])
            ->one();

            $competency_column_index = 0;
            $knowledge = '';
            $skill = '';
            $sub_competency_index = 0;

            //foreach($competencies_distinct as $competency_distinct)


            foreach($subcompetency_array as $competency_key => $competency_value)
               {
                $col_competency = $kolomawalkompetensi + 1 + (2 * $indexmeja) + ($competency_column_index * $jumlahmejadalamsebaris * 1);
                $cellvalues = $worksheet->getCellByColumnAndRow($col_competency, $row)->getValue();
                $description_values = $worksheet->getCellByColumnAndRow($col_competency + 1, $row)->getValue();
                $competencies_values = $cellvalues;
    /*
                 echo '<pre>';
                               echo 'competency key: ' . $competency_key;
                                          echo '<br/>value: ' . $cellvalues ;
                               echo '<br/>';
                print_r($competency_value);
                 echo '</pre>';
    */
                $competencies_values_length = strlen($competencies_values);
             //   $subcomps = $subcompetency_array[$competency_distinct['competency']];
                $ci = 0;
                foreach($competency_value as $sub_competency_key => $sub_competency_value) {

                            $resultcompetency = new ResultCompetency;
                            $resultcompetency->observation_id = $observation_group->observation_id;
                            $resultcompetency->competency = $competency_key;
                            $resultcompetency->sub_competency = $sub_competency_key;
                            // $resultcompetency->competency_id = $subcomp;

                                                  if ($sub_competency_key == 'knowledge') {
                                                  $resultcompetency->result =  substr($cellvalues,-2,1);
                                                  } else if ($sub_competency_key == 'skill')
                                                  {
                                                   $resultcompetency->result = substr($cellvalues,-1,1);
                                                  } else {
                                                    $resultcompetency->result = $cellvalues[$ci];
                                                    /**
                                                     DISINI BISA DIPAKAI UNTUK CEK INTEGRITAS HASIL LINKAGE
                                                    */
                                                   //  $reflinkage = RefCompetency::find()->andWhere(['id' => $subcomp])->One();
                                                   //  $linkageresult = ResultCompetency::find()->andWhere(['observation_id'=>$id])->andWhere(['competency_id'=>$reflinkage->linkage])->One();
                                                   // $resultcompetency->description  =  $subcomp;
                                                   $ci++;
                                                  }

                            $resultcompetency->save();
                            }
                 $competency_column_index++;
             } // foreach($subcompetency_array as $competency_key => $competency_value)
             $nomeja++;
             $indexmeja++;
            }
           }
            return;
        }  //////


        public function Process_psikotes($id,$aid,$csid)
        {
         $model = File::findOne($id);
         $assessment_id = $aid;
         $competency_set_id = $csid;
         $gol_pegawai_id = 1;


         $inputFileName = \Yii::$app->basePath . '/web/uploads/' . $model->file_location;

         $inputFileType = 'Excel2007';
         $sheetname = 'PSIKOTES';
         $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
          $objReader->setLoadSheetsOnly($sheetname);
         $objPHPExcel = $objReader->load($inputFileName);
         $worksheet = $objPHPExcel->getActiveSheet();


         $highestRow = $worksheet->getHighestRow(); // e.g. 10
         $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
         $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

         $jumlahmejadalamsebaris = Yii::$app->params['settings']['jumlahmejadalamsebaris'];
         $jumlahsubtest = Yii::$app->params['settings']['jumlahsubtest'];
         $kolomawalsubtest = Yii::$app->params['settings']['kolomawalsubtest'];
         $barisawalsubtest = Yii::$app->params['settings']['barisawalsubtest'];


       //  echo '<table>' . "\n";

         for ($row = $barisawalsubtest; $row <= $highestRow; ++$row) {

          $rowofdata = new Data();
          $datastring = '';
          $rowofdata->status = 'unprocessed';
          $rowofdata->tipe = 'psikotes';
          $rowofdata->file_id = $model->id;
          $rowiterator = $worksheet->getRowIterator($row)->current();
          $cellIterator = $rowiterator->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false);
          foreach ($cellIterator as $cell) {
           $datastring = $datastring . ',' .  $cell->getValue();
          }
          $rowofdata->data = substr($datastring,1);
          $rowofdata->created_at = date('Y-m-d H:i:s');
          $rowofdata->save();

           //echo '<tr>' . PHP_EOL;
           $mejaawal = 0;
           $mejaakhir = 0;
           $nomeja = '';


           for ($col = 0; $col <= $kolomawalsubtest; ++$col) {
                 //  echo '<br/>:';
            if ($col == 0)
            {
            $daerah_asesor_meja = $worksheet->getCellByColumnAndRow($col, $row)->getValue();

             $nomeja =  substr($daerah_asesor_meja, -3, 3);
             $kode_daerah = substr($daerah_asesor_meja, 0,1);
             $kode_asesor = substr($daerah_asesor_meja, 3,1);
            } else if ($col == 1) {
             $nama_peserta = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 2) {
             $nip = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 3) {
             $kodebuku = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 4) {
              $tanggal_tes = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 5) {
               $tanggal_lahir = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 6) {
                $usia = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 7) {
                 $rumpun = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 8) {
                  $pendidikan = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 9) {
                   $jabatan = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 10) {
                    $gol_keg_1 = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            } else if ($col == 11) {
                     $gol_keg_2 = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }
            //echo '<td>' . $worksheet->getCellByColumnAndRow($col, $row)->getValue() .' | </td>' . PHP_EOL;
           }

    $observation_group = ObservationGroup::find()
       ->andWhere(['no_meja' => $nomeja])
        ->andWhere(['group_meja' => 'group meja BPS'])
        ->One();

        $observation =  $observation_group->observation_id;

        $psikotes_result = new PsikotesResult;
        $psikotes_result->kode_daerah = $kode_daerah;
        $psikotes_result->no_meja = $nomeja;
        $psikotes_result->nama_peserta = $nama_peserta;
        $psikotes_result->nip = $nip;
        $psikotes_result->kode_buku = $kodebuku;
        $psikotes_result->tanggal_tes = $tanggal_tes;
        $psikotes_result->tanggal_lahir = $tanggal_lahir;
        $psikotes_result->usia = $usia;
        $psikotes_result->rumpun = $rumpun;
        $psikotes_result->pendidikan = $pendidikan;
        $psikotes_result->jabatan = $jabatan;
        $psikotes_result->golkeg1 = $gol_keg_1;
        $psikotes_result->golkeg2 = $gol_keg_2;
        //$asesor = Asesor::find()->andWhere(['kode_asesor' => $kode_asesor])->One();
       $psikotes_result->asesor_id = $kode_asesor;
        $psikotes_result->observation_id = $observation;
        $psikotes_result->save();
                print_r($psikotes_result->getErrors());

    $assessment = Assessment::findOne($aid);

    $subtests = RefPsikotesSubtest::find()->andWhere(['project_id' => $assessment->project_id])->All();
    $indexsubtest = 0;

    //echo '<pre>';
    //print_r($subtests);
    //echo '</pre>';

    foreach ($subtests as $subtest){
          echo $subtest->subtest_name;
                echo ' = ';
                echo $psikotes_result->id;
          echo ' : ';


           $col_subtest = $kolomawalsubtest + 1 + (3 * $indexsubtest);
           echo $truecount = $worksheet->getCellByColumnAndRow($col_subtest, $row)->getValue();
            echo $falsecount = $worksheet->getCellByColumnAndRow($col_subtest + 1, $row)->getValue();
            echo $blankcount = $worksheet->getCellByColumnAndRow($col_subtest + 2, $row)->getValue();

           $psikotes_subtest_result = new PsikotesSubtestResult;
           $psikotes_subtest_result->psikotes_result_id = $psikotes_result->id;
           $psikotes_subtest_result->psikotes_subtest_name = $subtest->subtest_name;
                  $psikotes_subtest_result->psikotes_project_id = $subtest->project_id;


           if ($subtest->subtest_name != 'kostick') {
                     $psikotes_subtest_result->true_count = $truecount;
                     $psikotes_subtest_result->false_count = $falsecount;
                     $psikotes_subtest_result->blank_count = $blankcount;
                    } else if ($subtest->subtest_name == 'kostick')
                    {
                     $psikotes_subtest_result->data = $truecount;
                    }
           $psikotes_subtest_result->save();
            print_r($psikotes_subtest_result->getErrors());

          $indexsubtest++;
      //     echo $subtest->subtest_name;
           echo '<br/>';

    }

    }
    //Yii::$app->getSession()->setFlash('success', 'psikotes di proses');
    //return $this->redirect(['file/index']);
    return;

        }



}
