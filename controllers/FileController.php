<?php

namespace app\controllers;

use Yii;
use app\models\File;
use app\models\FileSearch;
use app\models\ObservationGroup;
use app\models\Observation;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

use app\models\UploadForm;
use app\models\Data;
use app\models\Pegawai;
use app\models\Asesor;
use app\models\Assessment;
use app\models\RefCompetency;
use app\models\ResultCompetency;
use app\models\Result;
use yii\web\UploadedFile;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;

require(__DIR__ . '/../components/readfilter.php');


/**
 * FileController implements the CRUD actions for File model.
 */
class FileController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionIndex($project_id)
    {

        $searchModel = new FileSearch();
        $searchparams = Yii::$app->request->queryParams;
        $searchparams["FileSearch"]["project_id"] = $_REQUEST['project_id'] ;
        $dataProvider = $searchModel->search($searchparams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }


    /**
     * Displays a single File model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new File model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new File();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing File model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing File model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the File model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return File the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = File::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

public function actionProcess($id)
{

 if (Yii::$app->request->post()) {
  $post = Yii::$app->request->post();
  $choice = $post['choice'];
  $model = File::findOne($id);
  switch ($choice) {

   case '1':
    if ($this->processFile($model, $choice))
    {
     $model->modified_at = new \yii\db\Expression('NOW()');
     $model->save();
     Yii::$app->session->setFlash('success', 'file berhasil upload');
     //print_r($model);
     //echo '<br/>PPPPPPPPPP    ';
     //echo $model->modifed_at;
    }
    break;
   default:
    Yii::$app->session->setFlash('danger', 'belum mendukung untuk asesmen yang dipilih');
  }
 //return $this->redirect(['upload', 'id' => $post['choice'] ]);

 //echo '<br/><br/><br/>';
 //print_r(Yii::$app->request->post());
 }
  return $this->render('uploadchoice');
}


public function processFile($model,$id)
{
 echo 'process';
 $inputFileName = \Yii::$app->basePath . '/web/uploads/' . $model->file_location;

 $inputFileType = 'Excel2007';
 $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
 echo '<br/><br/><br/><br/>';
 echo '<pre>';

 $objPHPExcel = $objReader->load($inputFileName);
 $worksheet = $objPHPExcel->getActiveSheet();

 // Get the highest row and column numbers referenced in the worksheet
 $highestRow = $worksheet->getHighestRow(); // e.g. 10
 $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
 $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

 echo 'highest row : ' . $highestRow;
 echo '<br/>highest column : ' . $highestColumn;
 echo '<br/>highest column index : ' . $highestColumnIndex;


 /*KONFIGURASI UNTUK BPS*/
 // 000000 - digit 5 & 6 adalah KS
 $jumlahmejadalamsebaris = 10;
 $jumlahkompetensi = 5;
 $kolomawalkompetensi = 3;
 $barisawalkompetensi = 3;
 $assessment_id = 1; //BPS
 $competency_set_id = $id; //BPS
 $gol_pegawai_id = 1;
 echo '<table>' . "\n";
 for ($row = $barisawalkompetensi; $row <= $highestRow; ++$row) {
  $rowofdata = new Data();
  $datastring = '';
  $rowofdata->status = 'unprocessed';
  $rowofdata->file_id = $model->id;
  $rowiterator = $worksheet->getRowIterator($row)->current();
  $cellIterator = $rowiterator->getCellIterator();
  $cellIterator->setIterateOnlyExistingCells(false);
  foreach ($cellIterator as $cell) {
   $datastring = $datastring . ',' .  $cell->getValue();
  }
  $rowofdata->data = substr($datastring,1);
  $rowofdata->created_at = date('Y-m-d H:i:s');
  $rowofdata->save();
   /**
   DISINI ANALISA TIAP DATA ROW MENJADI KOMPETENSI _ KOMPETENSI
   */
   echo '<tr>' . PHP_EOL;
   $mejaawal = 0;
   $mejaakhir = 0;
   $nomeja = '';
   $kode_daerah='';
   $kode_asesor='';
   /**
   ISI OBSERVATION GROUP
   */
   for ($col = 0; $col <= $kolomawalkompetensi; ++$col) {
    if ($col == 0)
     $kode_daerah = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
    if ($col == 1)
     $kode_asesor = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
    if ($col == 2)
     $mejaawal = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
    if ($col == 3)
     $mejaakhir = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
    echo '<td>' .
    $worksheet->getCellByColumnAndRow($col, $row)->getValue() .' | </td>' . PHP_EOL;
   }

   $asesor = Asesor::find()
   ->andWhere(['kode_asesor' => $kode_asesor])
   ->One();
   $nomeja = $mejaawal;
   $indexmeja = 0;

   while ($nomeja <= $mejaakhir)
   {

    /**
    DISINI LOOP UNTUK ASPEK ASPEK KOMPETENSI
    1. LOOP untuk KOMPETENSI
    2. LOOP untuk linkage
    */

    $assessment_model = Assessment::find()
    ->andWhere(['id' => $assessment_id])
    ->One();

    $competencies_distinct = RefCompetency::find()
    ->andWhere(['competency_set_id' => $competency_set_id])
    ->andWhere(['linkage' => null])
    ->select('competency')
    ->orderBy('order')
    ->distinct()
    ->asArray()
    ->All();

    $competencies = RefCompetency::find()
    ->andWhere(['competency_set_id' => $competency_set_id])
    ->andWhere(['linkage' => null])
    ->orderBy('order')
    //HARUS DI SORT BERDASARKAN ORDER
    ->asArray()
    ->All();

    $linkages_distinct = RefCompetency::find()
    ->andWhere(['competency_set_id' => $competency_set_id])
    ->andWhere(['not',['linkage' => null]])
    ->select('competency')
    ->orderBy('order')
    ->distinct()
    ->asArray()
    ->All();

    $linkage_all = RefCompetency::find()
    ->andWhere(['competency_set_id' => $competency_set_id])
    ->andWhere(['not',['linkage' => null]])
    ->orderBy('order')
    //HARUS DI SORT BERDASARKAN ORDER
    ->asArray()
    ->All();

    $subcompetency_array = ArrayHelper::map($competencies, 'id', 'sub_competency','competency');
    $col_warteg = (($kolomawalkompetensi) + (2* $jumlahmejadalamsebaris * ($jumlahkompetensi)) + 1);

    $observation = new Observation;
    $observation->assessment_id = $assessment_id;
    $observation->competency_set_id = $competency_set_id;

    $observation->warteg_depth =  $worksheet->getCellByColumnAndRow($col_warteg, $row)->getValue();
    $observation->warteg_detail =  $worksheet->getCellByColumnAndRow($col_warteg + 1, $row)->getValue();
    $observation->warteg_finesse =  $worksheet->getCellByColumnAndRow($col_warteg + 2, $row)->getValue();
    if($observation->validate()) {
     $observation->save();
    } else {
     print_r($observation->errors);
    }

    echo '<br/>Meja = ' . $nomeja;

    $competency_column_index = 0;
    $knowledge = '';
    $skill = '';
    $sub_competency_index = 0;
    foreach($competencies_distinct as $competency_distinct)
    {
     $col_competency = $kolomawalkompetensi + 1 + (2 * $indexmeja) + ($competency_column_index * $jumlahmejadalamsebaris * 2);
     echo '<br/>kompetensi ' . $competency_distinct['competency'] . ' : ';
     $cellvalues = $worksheet->getCellByColumnAndRow($col_competency, $row)->getValue();
     $description_values = $worksheet->getCellByColumnAndRow($col_competency + 1, $row)->getValue();

     $competencies_values = $cellvalues;
     $competencies_values_length = strlen($competencies_values);

     $subcomps = $subcompetency_array[$competency_distinct['competency']];
     $ci = 0;
     foreach ($subcomps as $subcomp => $value) {
      $resultcompetency = new ResultCompetency;
      $resultcompetency->observation_id = $observation->id;
      $resultcompetency->competency_id = $subcomp;
      $resultcompetency->result = ($cellvalues[$ci] > 0 ?$cellvalues[$ci] : '0');
      if(isset($description_values[$ci])) {
       $resultcompetency->description = $description_values[$ci];}
       $resultcompetency->save();
       $ci++;
      }
      echo ' ==> K : ' . $knowledge . ' S : ' . $skill;
      $competency_column_index++;
     }

     $linkage_array = ArrayHelper::map($linkage_all, 'id', 'linkage','competency');
     foreach($linkages_distinct as $linkage_distinct)
     {
      $linkages = $linkage_array[$linkage_distinct['competency']];
      foreach($linkages as $linkage => $val)
      {
       $resultlinkage = new ResultCompetency;
       $resultlinkage->observation_id = $observation->id;
       $resultlinkage->competency_id = $linkage;
       if ($val != '0') {
        $linkedaspect = ResultCompetency::find()
        ->andWhere(['observation_id' => $observation->id])
        ->andWhere(['competency_id' => $val])
        ->One();

        $resultlinkage->result =   ($linkedaspect->result > 0 ? $linkedaspect->result : '0');
       }
       $resultlinkage->save();
      }
     }
     $observationgroup = new ObservationGroup;
     $observationgroup->observation_id = $observation->id;
     $observationgroup->meja_awal = $mejaawal;
     $observationgroup->meja_akhir = $mejaakhir;
     $observationgroup->no_meja = $nomeja;
     if($observationgroup->validate()) {
      $observationgroup->save();
     } else {
      print_r($observationgroup->errors);
     }

     $pegawai = new Pegawai;
     $pegawai->nama_pegawai = $assessment_id . '_' . $observation->id .'_'. $nomeja;
     $pegawai->gol_pegawai_id = $gol_pegawai_id;
     $pegawai->save();

     $result = new Result;
     $result->observation_id = $observation->id;
     $result->kode_daerah = $kode_asesor;
     $result->assessor_id = $asesor->id;

     if($result->validate()) {
      $result->save();
     } else {
      print_r($result->errors);
     }

     $nomeja++;
     $indexmeja++;
     $col_warteg = $col_warteg + 3;
    }
    echo '</tr>' . PHP_EOL;
   }
   echo '</table>' . PHP_EOL;
   echo '</pre>';
   return true;
}


public function actionUpload($project_id)
{
 $objPHPExcel = new \PHPExcel();
    $uploadmodel = new UploadForm();
    $model = new File();
    $sheetname = 'SCAN';

    if (Yii::$app->request->isPost) {
        $uploadmodel->excelFile = UploadedFile::getInstance($uploadmodel, 'excelFile');
        if ($uploadmodel->upload()) {
         $model->project_id = $project_id;
                $model->file_location = $uploadmodel->excelFile->baseName . '.' . $uploadmodel->excelFile->extension;
                $model->created_at = new \yii\db\Expression('NOW()');
                if($model->save()) {
                 Yii::$app->getSession()->setFlash('success', 'file uploaded');
                return $this->redirect(['index', 'project_id' => $_REQUEST['project_id']]);
                } else {
                         Yii::$app->getSession()->setFlash('warning', json_encode($model->getErrors()));
                }

                 }
                }
    return $this->render('upload', ['uploadmodel' => $uploadmodel]);
}


public function actionUploadlinkage($id)
{
 $objPHPExcel = new \PHPExcel();
 $uploadmodel = new UploadForm();
 $model = new File();
 $sheetname = 'LINK';
 if (Yii::$app->request->isPost) {
  $uploadmodel->excelFile = UploadedFile::getInstance($uploadmodel, 'excelFile');
  if ($uploadmodel->upload()) {
   $model->file_location = $uploadmodel->excelFile->baseName . '.' . $uploadmodel->excelFile->extension;
   $model->created_at = new \yii\db\Expression('NOW()');
   $model->save();
   Yii::$app->session->setFlash('success', 'file berhasil upload');
  }
 }
return $this->render('upload', ['uploadmodel' => $uploadmodel]);
}


}
