<?php

namespace app\controllers;

use Yii;
use app\models\Assessment;
use app\models\AssessmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\File;
use app\models\Data;
use app\models\Pegawai;
use app\models\Jabatan;
use app\models\Observation;
use app\models\ObservationGroup;
use app\models\PsikotesResult;
use app\models\RefGolonganPegawai;
use app\models\ResultCompetency;
use app\models\RefProjectConfig;
use app\models\RefCompetency;
use app\models\Result;
use app\models\Asesor;
use app\models\PegawaiExt;
use app\models\RefPsikotesSubtest;
use app\models\PsikotesSubtestResult;
use yii\helpers\ArrayHelper;

//use app\components\ChunkReadFilter;

/**
 * AssessmentController implements the CRUD actions for Assessment model.
 */
class AssessmentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Assessment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AssessmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Assessment model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionPrint($id)
    {
     //echo 'pdf';
     ob_start();
      ob_end_clean();

$model = Assessment::findOne($id);
$gol_pegawai = Pegawai::find()->andWhere(['nama_pegawai' => explode('_',$model->assessment_name)[0]])->One()->golongan_pegawai;

if ($gol_pegawai == 'eselon_2') {
 return $this->render('print_eselon3', [
     'model' => $this->findModel($id),
 ]);
} else if ($gol_pegawai == 'eselon_3') {
 return $this->render('print_eselon3', [
     'model' => $this->findModel($id),
 ]);
}
else if ($gol_pegawai == 'eselon_4') {
 return $this->render('print_eselon3', [
     'model' => $this->findModel($id),
 ]);
} else {
 return $this->render('print_nonstruktural', [
     'model' => $this->findModel($id),
 ]);
}
      /*return $this->render('print', [
          'model' => $this->findModel($id),
      ]);
*/

    }

    /**
     * Creates a new Assessment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Assessment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Assessment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function createPegawaiIfNew($nip)
    {
     $pegawaiexist = Pegawai::find()->andWhere(['nip' => $nip])->One();
     if (isset($pegawaiexist)) {
      return true;
     } else {
      return false;
      //$newpegawai = new Pegawai();
      //$newpegawai->nip;

     }
    }

    public function actionUndoprocess($id, $project_id)
    {

     echo 'id : ' . $id;
     echo '<h2> DELETE ORDER : PSIKOTES SUBTES RESULT -> PSIKOTES RESULT ->RESULT COMPETENCY -> RESULT -> OBSERVATION GROUP -> OBSERVATION -> ASSESSMENT -> DATA</h2>';
     echo '<br/>project_id : ' . $project_id;
     $data = Data::find()->andWhere(['file_id' => $id])->andWhere(['tipe' => 'psikotes'])->All();
     echo 'yang harus dihapus linkage -> observasi -> psikotes. info pegawai disimpan';
$assessment_id_array = [];
$assessment_id_string = '';

   foreach($data as $datum_key => $datum_value)
   {
    echo '<br/>';
    echo $datum_value->id . ' -> ' .  $datum_value->status . ' -> ' . $datum_value->tipe;


 $dataarray = explode(",",$datum_value->data);
  //echo '<br/>';
 echo $namenik = trim($dataarray[1], " ") . '_' . trim($dataarray[2], " ");
 echo '<br/>';
 $found = Assessment::find()->andWhere(['project_id' => $project_id])->andWhere(['assessment_name' => $namenik])->One();
 if (isset($found)) {
  array_push($assessment_id_array, $found->id);
  $assessment_id_string = $assessment_id_string . ',' .  $found->id;
  //echo 'YES';
 } else {
  //echo 'NO';
 }


   }
//DELETE ASSESSMENT -> psikotes_result -> psikotes_subtest_result -> Observation -> observation group ->
   //print_r(array_values($assessment_id_array));
   echo '<br/>';
   echo $assessment_id_string_final = substr($assessment_id_string, 1);
$observation_id_array_2 = [];
$result_competency_array_2 = [];
$result_array_2 = [];
   $psikotes_id_array = PsikotesResult::find()->andWhere(['in', 'assessment_id', $assessment_id_array])->All();
    $observation_id_array = Observation::find()->andWhere(['in', 'assessment_id', $assessment_id_array])->All();
//        $observation_group_array = Observation::find()->andWhere(['in', 'observation_id', $assessment_id_array])->All();


    foreach($observation_id_array as $observation_id) {
        array_push($observation_id_array_2, $observation_id->id);
    }
  echo '<br/>';  echo '<br/>';
  $psikotes_id_array_2 = [];
foreach($psikotes_id_array as $psikotes_id) {
 echo $psikotes_id->id;
   array_push($psikotes_id_array_2, $psikotes_id->id);
}
echo '<br/>';
//print_r($psikotes_id_array_2);
   $psikotes_subtest_id_array = PsikotesSubtestResult::find()->andWhere(['in', 'psikotes_result_id', $psikotes_id_array_2])->All();


   //echo '<br/><br/>Name NIK & project to remove : <br/>';
   //print_r($assessment_id_array);

    $result_competency_array = ResultCompetency::find()->andWhere(['in', 'observation_id', $observation_id_array_2])->All();
            $observation_group_array = ObservationGroup::find()->andWhere(['in', 'observation_id', $observation_id_array_2])->All();
        $result_array = Result::find()->andWhere(['in', 'observation_id', $observation_id_array_2])->All();

    foreach($result_competency_array as $result_competency_id) {
    // echo $psikotes_id->id;
       array_push($result_competency_array_2, $result_competency_id->observation_id);
    }
    foreach($result_array as $result_id) {
    // echo $psikotes_id->id;
       array_push($result_array_2, $result_id->id);
    }

   echo '<br/><br/>assessment id to remove : <br/>';
   print_r($assessment_id_array);

   echo '<br/><br/>psikotes result id to remove : <br/>';
   print_r($psikotes_id_array_2);

      echo '<br/><br/>psikotes SUBTEST id to remove : <br/>';
   //echo sizeof($psikotes_subtest_id_array);

   echo '<br/><br/>Observation id to remove : <br/>';
   print_r($observation_id_array_2);

   echo '<br/><br/>Observation Group id to remove : <br/>';
    print_r($observation_id_array_2);

   echo '<br/><br/>result competency id to remove : <br/>';
   print_r($result_competency_array_2);

   echo '<br/><br/>result id to remove : <br/>';
   print_r($result_array_2);

   echo '<br/><br/>data id to remove : <br/>';
   print_r($assessment_id_array);


     echo '<h2> DELETE ORDER : PSIKOTES SUBTES RESULT -> PSIKOTES RESULT ->RESULT COMPETENCY -> RESULT -> OBSERVATION GROUP -> OBSERVATION -> ASSESSMENT -> DATA</h2>';
//$psikotes_subtest_id_array->deleteAll();
//PsikotesSubtestResult::find()->andWhere(['in', 'psikotes_result_id', $psikotes_id_array_2])->deleteAll();
PsikotesSubtestResult::deleteAll(['in', 'psikotes_result_id', $psikotes_id_array_2]);
PsikotesResult::deleteAll(['in', 'assessment_id', $assessment_id_array]);

ResultCompetency::deleteAll(['in', 'observation_id', $observation_id_array_2]);

    Result::deleteAll(['in', 'observation_id', $observation_id_array_2]);
        ObservationGroup::deleteAll(['in', 'observation_id', $observation_id_array_2]);
        Observation::deleteAll(['in', 'assessment_id', $assessment_id_array]);

        Assessment::deleteAll(['in', 'id', $assessment_id_array]);
        Data::deleteAll(['file_id'=>$id]);

    }


    public function actionProcessall($id, $project_id, $start_row, $max_rows)
    {
echo 'process all';
   //  require_once Yii::app()->basePath . '/components/chunkreadfilter.php';

/*
     if ($this->checkIfProcessed($id)) {
      return false;
     }
*/



     $psikotes_return = $this->Process_psikotes($id,$project_id, $start_row, $max_rows);
     //  $observasi_return = $this->Process_observasi($id,$project_id);
       // $linkage_return = $this->Process_linkage($id,$project_id);

//if ($psikotes_return && $observasi_return && $linkage_return) {
     if (true) {
     Yii::$app->getSession()->setFlash('success', 'observasi di proses');
     echo 'sukses'; } else {
           Yii::$app->getSession()->setFlash('warning', 'Data sudah pernah di proses sebelumnya - TIDAK DIPROSES');
           echo 'Data sudah pernah di proses sebelumnya - TIDAK DIPROSES';
     }
     //return $this->redirect(['file/index']);
    }

    public function Process_linkage($id,$project_id)
     {

      $previous_data = Data::find()->andWhere(['file_id' => $id])->andWhere(['tipe' => 'linkage'])->All();
     if (sizeof($previous_data) > 0) {
       return false;
      }

       $model = File::findOne($id);
       //$assessment_id = $aid;
       //$competency_set_id = $csid;
       //$gol_pegawai_id = 1;
       $inputFileName = \Yii::$app->basePath . '/web/uploads/' . $model->file_location;
       $inputFileType = 'Excel2007';
       $sheetname = 'LINK';
       $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
       $objReader->setLoadSheetsOnly($sheetname);
       $objPHPExcel = $objReader->load($inputFileName);
       $worksheet = $objPHPExcel->getActiveSheet();
       $highestRow = $worksheet->getHighestRow(); // e.g. 10
       $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
       $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
       $jumlahmejadalamsebaris = Yii::$app->params['settings']['jumlahmejadalamsebaris'];
       $jumlahkompetensi = Yii::$app->params['settings']['jumlahkompetensi'];
       $kolomawalkompetensi = Yii::$app->params['settings']['kolomawalkompetensi'];
       $barisawalkompetensi = Yii::$app->params['settings']['barisawalkompetensi'];
//echo '<br/>highestRow : ' . $highestRow;
       for ($row = $barisawalkompetensi; $row <= $highestRow; $row++) {
        echo '<h1>ROW '. $row . '</h1>';
          $rowofdata = new Data();
          $datastring = '';
          $rowofdata->status = 'unprocessed';
          $rowofdata->tipe = 'linkage';
          $rowofdata->file_id = $model->id;

          $rowiterator = $worksheet->getRowIterator($row)->current();
          $cellIterator = $rowiterator->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false);
                  foreach ($cellIterator as $cell) {
                   $datastring = $datastring . ',' .  $cell->getValue();
                  }
          $rowofdata->data = substr($datastring,1);
          $rowofdata->created_at = date('Y-m-d H:i:s');
          $rowofdata->save();

           //echo '<tr>' . PHP_EOL;
           $mejaawal = 0;
           $mejaakhir = 0;
           $nomeja = '';
           $kode_daerah='';
           $kode_asesor='';

                           for ($col = 0; $col <= $kolomawalkompetensi; ++$col) {
                            if ($col == 0)
                             $kode_daerah = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            if ($col == 1)
                             $kode_asesor = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            if ($col == 2)
                             $mejaawal = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            if ($col == 3)
                             $mejaakhir = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            //echo '<td>' . $worksheet->getCellByColumnAndRow($col, $row)->getValue() .' | </td>' . PHP_EOL;
                           }

           $asesor = Asesor::find()
           ->andWhere(['id' => $kode_asesor])
           ->One();
           $nomeja = $mejaawal;
           $indexmeja = 0;

           $assessment_list = Assessment::find()->andWhere(['project_id' => $project_id])->asArray()->All();
           $assessment_list_2 = ArrayHelper::getColumn($assessment_list, 'id');

           while ($nomeja <= $mejaakhir) //$mejaakhir
           {
            echo '<br/><br/>meja : ' . $nomeja;
            $projectconfig = RefProjectConfig::find()->andWhere(['project_id' => $project_id])->One();

            $competencies_distinct = RefCompetency::find()
            ->andWhere(['competency_set' => $projectconfig->competency_set_id])
            ->andWhere(['uselinkage' => 'true'])
            ->select('competency')
            ->orderBy('order')
            ->distinct()
            ->asArray()
            ->All();

            $competencies = RefCompetency::find()
            ->andWhere(['competency_set' => $projectconfig->competency_set_id])
            ->andWhere(['uselinkage' => 'true'])
            ->orderBy('order')
            ->asArray()
            ->All();

            //$subcompetency_array = ArrayHelper::map($competencies, 'id', 'sub_competency','competency');
            $subcompetency_array = ArrayHelper::map($competencies, 'sub_competency','linkage','competency');

            $observation_group = ObservationGroup::find()
           ->andWhere(['no_meja' => $nomeja])
            ->andWhere(['group_meja' => $project_id])
            ->one();

            $competency_column_index = 0;
            $knowledge = '';
            $skill = '';
            $sub_competency_index = 0;
            $countloop = 0;
            foreach($subcompetency_array as $competency_key => $competency_value)
               {
                //echo '<br/> competency value : ' . $competency_key;
                $col_competency = $kolomawalkompetensi + 1 + (1 * $indexmeja) + ($competency_column_index * $jumlahmejadalamsebaris * 1);
                $cellvalues = $worksheet->getCellByColumnAndRow($col_competency, $row)->getValue();
                $description_values = $worksheet->getCellByColumnAndRow($col_competency + 1, $row)->getValue();
                $competencies_values = $cellvalues;

                $competencies_values_length = strlen($competencies_values);
                $ci = 0;

                foreach($competency_value as $sub_competency_key => $sub_competency_value) {
                            echo '<br/>competency : ' . $competency_key . ' => ' . $sub_competency_key;
                            $resultcompetency = new ResultCompetency;
                            $resultcompetency->observation_id = $observation_group->observation_id;
                            $resultcompetency->competency = $competency_key;
                            $resultcompetency->competency_set_id = $projectconfig->competency_set_id;
                            $resultcompetency->sub_competency = $sub_competency_key;
                            if ($sub_competency_key == 'knowledge') {
                                    $resultcompetency->result =  substr($cellvalues,-2,1);
                                    echo 'cell : ' . $worksheet->getCellByColumnAndRow($col_competency, $row)->getCoordinate();
                            } else if ($sub_competency_key == 'skill') {
                                    $resultcompetency->result = substr($cellvalues,-1,1);
                                    echo 'cell : ' . $worksheet->getCellByColumnAndRow($col_competency, $row)->getCoordinate();
                            } else {
                                   $resultcompetency->result = $cellvalues[$ci];
                                    //$resultcompetency->result = 'else';
                                                   //  $reflinkage = RefCompetency::find()->andWhere(['id' => $subcomp])->One();
                                                   //  $linkageresult = ResultCompetency::find()->andWhere(['observation_id'=>$id])->andWhere(['competency_id'=>$reflinkage->linkage])->One();
                                                   // $resultcompetency->description  =  $subcomp;
                               $ci++;
                            }

                               if ($resultcompetency->save()) {
                               //echo '<br/>saved';
                              } else {
                               print_r($resultcompetency->errors);
                         /*     echo '<br/><br/>==error : ' . $resultcompetency->result;
                               echo '<br/>==observation : ' . $observation_group->observation_id;
                                echo '<br/>==Kompetensi : ' . $competency_key;
                                 echo '<br/>==cell values : ' . $cellvalues;
                               echo '<br/>==column : ' . $worksheet->getCellByColumnAndRow($col_competency, $row)->getCoordinate();
                                             echo '<br/>==row : ' . $row;
                                echo '<br/>==no meja : ' . $nomeja;
                                */
                              //echo '<br/><br/><br/>error ' . $competency_key;
                                //print_r($resultcompetency);
                              }

                               $countloop++;
                            }

                 $competency_column_index++;
             } // foreach($subcompetency_array as $competency_key => $competency_value)
              //echo '<br/>count loop : ' . $countloop;

             $nomeja++;
             $indexmeja++;
            }
            echo 'SAMPAI DISINI';
           }

            return true;
        }  ////



    public function Process_observasi($id,$project_id)
    {
     $previous_data = Data::find()->andWhere(['file_id' => $id])->andWhere(['tipe' => 'observasi'])->All();
     if (sizeof($previous_data) > 0) {
      return false;
     }
     //$id => file id
     //project_id => project id
     $resultaddcount = 0;
     $observationaddcount = 0;
     $observationgroupaddcount = 0;
     $resultcompetencyaddcount = 0;
     $model = File::findOne($id);

     $group_meja = $project_id;

     $inputFileName = \Yii::$app->basePath . '/web/uploads/' . $model->file_location;
     $inputFileType = 'Excel2007';
     $sheetname = 'OBS';
     $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
      $objReader->setLoadSheetsOnly($sheetname);
     $objPHPExcel = $objReader->load($inputFileName);
     $worksheet = $objPHPExcel->getActiveSheet();
     // Get the highest row and column numbers referenced in the worksheet
     $highestRow = $worksheet->getHighestRow(); // e.g. 10
     $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
     $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
     $jumlahmejadalamsebaris = Yii::$app->params['settings']['jumlahmejadalamsebaris'];
     $jumlahkompetensi = Yii::$app->params['settings']['jumlahkompetensi'];
     $kolomawalkompetensi = Yii::$app->params['settings']['kolomawalkompetensi'];
     $barisawalkompetensi = Yii::$app->params['settings']['barisawalkompetensi'];

     for ($row = $barisawalkompetensi; $row <= $highestRow; ++$row) {

          $rowofdata = new Data();
          $datastring = '';
          $rowofdata->status = 'unprocessed';
            $rowofdata->tipe = 'observasi';
          $rowofdata->file_id = $model->id;
          $rowiterator = $worksheet->getRowIterator($row)->current();
          $cellIterator = $rowiterator->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false);
            foreach ($cellIterator as $cell) {
             $datastring = $datastring . ',' .  $cell->getValue();
            }
          $rowofdata->data = substr($datastring,1);
          $rowofdata->created_at = date('Y-m-d H:i:s');
          $rowofdata->save();
          $mejaawal = 0;
          $mejaakhir = 0;
          $kode_daerah='';
          $kode_asesor='';
          for ($col = 0; $col <= $kolomawalkompetensi; ++$col) {
           if ($col == 0)
            $kode_daerah = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
           if ($col == 1)
            $kode_asesor = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
           if ($col == 2)
            $mejaawal = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
           if ($col == 3)
            $mejaakhir = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
          }
          $nomeja = $mejaawal;
          $indexmeja = 0;
          $assessment_list = Assessment::find()->andWhere(['project_id' => $project_id])->asArray()->All();
          $assessment_list_2 = ArrayHelper::getColumn($assessment_list, 'id');

               while ($nomeja <= $mejaakhir)
               {
                                $col_warteg = (($kolomawalkompetensi) + (2* $jumlahmejadalamsebaris * ($jumlahkompetensi)) + 1);

                                $psikotes_res_model = PsikotesResult::find()
                                ->andWhere(['no_meja'=>$nomeja])
                                ->andWhere(['in','assessment_id', $assessment_list_2])
                                     ->One();

                                  //   echo $psikotes_res_model->assessment_id;

                                     $projectconfig = RefProjectConfig::find()->andWhere(['project_id' => $project_id])->One();


                $competencies_distinct = RefCompetency::find()
                ->andWhere(['competency_set' => $projectconfig->competency_set_id])
                    ->andWhere(['uselinkage' => 'false'])
                ->select('competency')
                ->orderBy('order')
                ->distinct()
                ->asArray()
                ->All();
                $competencies = RefCompetency::find()
                ->andWhere(['competency_set' => $projectconfig->competency_set_id])
                ->andWhere(['uselinkage' => 'false'])
                ->orderBy('order')
                ->asArray()
                ->All();
                $subcompetency_array = ArrayHelper::map($competencies, 'sub_competency','order','competency');


                $observation = new Observation;
                $observation->assessment_id = $psikotes_res_model->assessment_id;
                //$observation->competency_set = $projectconfig->competency_set_id;
                $observation->warteg_depth =  $worksheet->getCellByColumnAndRow($col_warteg, $row)->getValue();
                $observation->warteg_detail =  $worksheet->getCellByColumnAndRow($col_warteg + 1, $row)->getValue();
                $observation->warteg_finesse =  $worksheet->getCellByColumnAndRow($col_warteg + 2, $row)->getValue();
                if($observation->validate()) {
                 $observation->save();
                 $observationaddcount++;
                } else {
                 print_r($observation->errors);
                }

                $observationgroup = new ObservationGroup;
                $observationgroup->observation_id = $observation->id;
                $observationgroup->meja_awal = $mejaawal;
                $observationgroup->meja_akhir = $mejaakhir;
                $observationgroup->no_meja = $nomeja;
                $observationgroup->group_meja = $group_meja;
                if($observationgroup->validate()) {
                 $observationgroup->save();
                 $observationgroupaddcount++;
                } else {
                 print_r($observationgroup->errors);
                }
/*
                $pegawai = new Pegawai;
                $pegawai->nama_pegawai = $assessment_id . '_' . $observation->id .'_'. $nomeja;
                $golongan_pegawai_model = RefGolonganPegawai::find()->andWhere(['golongan_pegawai' => $gol_pegawai_id])->One();
                $pegawai->golongan_pegawai = (isset($golongan_pegawai_model->golongan_pegawai) > 0)? ($golongan_pegawai_model->golongan_pegawai) : null;
                $pegawai->save();
                */
                $result = new Result;
                $result->observation_id = $observation->id;
                $result->kode_daerah = $kode_daerah;
                $asesor = Asesor::find()->andWhere(['id' => $kode_asesor])->One();
                $result->assessor_id = (isset($asesor->id) > 0)? ($asesor->id) : null;
               //$result->pegawai_id = $pegawai->id;
                if($result->validate()) {
                 $result->save();
                 $resultaddcount++;
                } else {
                 print_r($result->errors);
                }
                $competency_column_index = 0;
                $knowledge = '';
                $skill = '';
                $sub_competency_index = 0;

                foreach($subcompetency_array as $competency_key => $competency_value)
                {

                 $col_competency = $kolomawalkompetensi + 1 + (2 * $indexmeja) + ($competency_column_index * $jumlahmejadalamsebaris * 2);
                 $cellvalues = $worksheet->getCellByColumnAndRow($col_competency, $row)->getValue();
                 $description_values = $worksheet->getCellByColumnAndRow($col_competency + 1, $row)->getValue();
                 $competencies_values = $cellvalues;
                 $competencies_values_length = strlen($competencies_values);
                  $ci = 0;
                 foreach($competency_value as $sub_competency_key => $sub_competency_value) {
                                $resultcompetency = new ResultCompetency;
                                $resultcompetency->observation_id = $observation->id;
                                $resultcompetency->competency_set_id = $projectconfig->competency_set_id;
                                $resultcompetency->competency = $competency_key;
                                $resultcompetency->sub_competency = $sub_competency_key;

                                if ($sub_competency_key == 'knowledge') {
                                 $resultcompetency->result =  substr($cellvalues,-2,1);
                                } else if ($sub_competency_key == 'skill')
                                {
                                 $resultcompetency->result = substr($cellvalues,-1,1);
                                } else {
                                  $resultcompetency->result = $cellvalues[$ci];
                                  $resultcompetency->description  = $description_values[$ci] ;
                                 $ci++;
                                }
                                if($resultcompetency->save()) {

                                } else {
                                      print_r($resultcompetency->errors);
                                }

                                $resultcompetencyaddcount++;
                  }

                  $competency_column_index++;
                 }
                                 // echo '<br/>no meja : ' . $nomeja. '<br/>';
                 $nomeja++;
                 $indexmeja++;
                 $col_warteg = $col_warteg + 3;

                }
       }
       return true;
    }


    /**
    first check if file has been processed before
    */

    public function checkIfProcessed($id) {
     $result = true;

     $previous_data = Data::find()->andWhere(['file_id' => $id])->andWhere(['tipe' => 'psikotes'])->All();
     if (sizeof($previous_data) > 0) {
      $result = true;
     } else {
      $result = false;
     }

     return $result;
    }

    public function Process_psikotes($id, $project_id,$start_row, $max_rows)
    {

     echo 'process _psikotes : ' ;
     $model = File::findOne($id);
     $inputFileName = \Yii::$app->basePath . '/web/uploads/' . $model->file_location;
     $inputFileType = 'Excel2007';
     $sheetname = 'PSIKOTES';
     $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
      $objReader->setLoadSheetsOnly($sheetname);


          $objPHPExcel = $objReader->load($inputFileName);
     $worksheet = $objPHPExcel->getActiveSheet();

     $highestRow = $worksheet->getHighestRow(); // e.g. 10
     $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'

     echo '<br/>'. $highestRow . '<br/>' .$highestColumn . '<br/>';
     $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
     //$highestRowIndex = \PHPExcel_Cell::rowIndexFromString($highestRow); // e.g. 5

     $jumlahmejadalamsebaris = Yii::$app->params['settings']['jumlahmejadalamsebaris'];
     $jumlahsubtest = Yii::$app->params['settings']['jumlahsubtest'];
     $kolomawalsubtest = Yii::$app->params['settings']['kolomawalsubtest'];

     $barisawalsubtest = ($start_row > 0) ? $start_row : (Yii::$app->params['settings']['barisawalsubtest']);
     $max_row_index_to_be_processed = ($highestRow > ( $start_row + $max_rows - 1)) ? ($start_row + $max_rows - 1) : $highestRow;


     require_once(__DIR__ . '/../components/chunkreadfilter.php');

     echo '<hr />';


     $chunkSize = 100;
     $chunkFilter = new \ChunkReadFilter();
       $objReader->setReadFilter($chunkFilter);

       //VALUE HIGHEST ROW DIBAWAH BUAT TESTING AJA


     /**  Loop to read our worksheet in "chunk size" blocks  **/
     /**  $startRow is set to 2 initially because we always read the headings in row #1  **/
echo '<pre>';
     for ($startRow = $barisawalsubtest; $startRow <= $max_row_index_to_be_processed; $startRow += $chunkSize) {
         echo 'Loading WorkSheet using configurable filter for headings row 1-3 and for rows ',$startRow,' to ',($startRow+$chunkSize-1),'<br />';
         /**  Tell the Read Filter, the limits on which rows we want to read this iteration  **/
         $chunkFilter->setRows($startRow,$chunkSize);

         /**  Load only the rows that match our filter from $inputFileName to a PHPExcel Object  **/
         $objPHPExcel = $objReader->load($inputFileName);

         //    Do some processing here

         $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

            //echo sizeof($sheetData);
            //echo $startRow;
            echo '<br /><br />'.$startRow.'==================================== '.  ($startRow + $chunkSize - 1) . '<br/>';


for ($row_loop = $startRow; $row_loop <= ($startRow + $chunkSize - 1); $row_loop++)
{
 $rowofdata = new Data();
 $datastring = '';
 $rowofdata->status = 'unprocessed';
 $rowofdata->tipe = 'psikotes';
 $rowofdata->file_id = $model->id;
 $rowiterator = $worksheet->getRowIterator($row_loop)->current();
 $cellIterator = $rowiterator->getCellIterator();
 $cellIterator->setIterateOnlyExistingCells(false);
      foreach ($cellIterator as $cell) {
       $datastring = $datastring . ',' .  $cell->getValue();
      }
 $rowofdata->data = substr($datastring,1);
 $rowofdata->created_at = date('Y-m-d H:i:s');

  $mejaawal = 0;
  $mejaakhir = 0;
  $nomeja = '';



         $row = $objPHPExcel->getActiveSheet()
             ->getRowIterator($row_loop)->current();


         $cellIterator = $row->getCellIterator();
         $cellIterator->setIterateOnlyExistingCells(false);
         $col = 0;
         foreach ($cellIterator as $cell) {
           //  echo '<br/>ROW: ', $cell->getRow(), PHP_EOL;
            // echo '<br/>COLUMN: ', $cell->getColumn(), PHP_EOL;
            // echo '<br/>COORDINATE: ', $cell->getCoordinate(), PHP_EOL;
            // echo '<br/>RAW VALUE: ', $cell->getValue(), PHP_EOL;
            // echo '<br/>';
            if ($col == 0)
            {
            $daerah_asesor_meja = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();

             $nomeja =  substr($daerah_asesor_meja, -3, 3);
             $kode_daerah = substr($daerah_asesor_meja, 0,1);
             $kode_asesor = substr($daerah_asesor_meja, 3,1);
            } else if ($col == 1) {
             $nama_peserta = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 2) {
             $nip = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 3) {
             $kodebuku = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 4) {
              $tanggal_tes = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 5) {
               $tanggal_lahir = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 6) {
                $usia = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 7) {
                 $rumpun = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 8) {
                  $pendidikan = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 9) {
                   $jabatan = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 10) {
                    $gol_keg_1 = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
            } else if ($col == 11) {
                     $gol_keg_2 = $worksheet->getCellByColumnAndRow($col, $row_loop)->getValue();
                    }
            //echo '<td>' . $worksheet->getCellByColumnAndRow($col, $row)->getValue() .' | </td>' . PHP_EOL;
             $col++;
         }

         if($rowofdata->save())
         {
               $newassignmentmodel = new Assessment();
               $newassignmentmodel->project_id = $project_id;
               $newassignmentmodel->assessment_name = trim($nama_peserta, " ") . '_' . trim($nip, " ");
               if($newassignmentmodel->save())
               {

               } else {
                 print_r($newassignmentmodel->getErrors());
               }
         } else {
           print_r($rowofdata->getErrors());
         }



         if($this->createPegawaiIfNew($nip))
         {
          echo 'pegawai existed<br/>';
          $newpegawai = Pegawai::find()->andWhere(['nip'=>$nip])->One();
         } else {
          $newpegawai = New Pegawai();
          $newpegawai->nip = $nip;
          $newpegawai->usia = $usia;
          $newpegawai->tanggal_lahir = $tanggal_lahir;
          //$newpegawai->pendidikan = $pendidikan;
          $newpegawai->nama_pegawai = $nama_peserta;
          //$golpegawai = RefGolonganPegawai::find()->andWhere(['tipe_jabatan' => strtolower($jabatan)])->andWhere(['rumpun' => strtolower($rumpun)])->One();
         /* if (isset($golpegawai)) {
           $newpegawai->golongan_pegawai = $golpegawai->golongan_pegawai;
          }
*/
          if($newpegawai->save()) {
           $rumpun_ext = new PegawaiExt();
           $rumpun_ext->nip = $nip;
           $rumpun_ext->key = 'rumpun';
           $rumpun_ext->value = $rumpun;

           $pendidikan_ext = new PegawaiExt();
           $pendidikan_ext->nip = $nip;
           $pendidikan_ext->key = 'pendidikan';
           $pendidikan_ext->value = $pendidikan;

           $jabatan_ext = new PegawaiExt();
           $jabatan_ext->nip = $nip;
           $jabatan_ext->key = 'jabatan';
           $jabatan_ext->value = $jabatan;

           if($rumpun_ext->save()) {
            $pendidikan_ext->save();
            $jabatan_ext->save();
           } else {
            print_r($rumpun_ext->getErrors());
            echo '<br/>nip : ';
            echo $nip;
            echo '<br/>rumpun : ';
            print_r($rumpun_ext);
           }





          } else {
                print_r($newpegawai->getErrors());
          }
         }



         $psikotes_result = new PsikotesResult;
         $psikotes_result->kode_daerah = $kode_daerah;
         $psikotes_result->no_meja = $nomeja;
         $psikotes_result->tanggal_tes = $tanggal_tes;
         $psikotes_result->kode_buku = $kodebuku;
         $psikotes_result->asesor_id = $kode_asesor;
          $psikotes_result->assessment_id = $newassignmentmodel->id;
          $psikotes_result->pegawai_id = $newpegawai->id;


        if ($psikotes_result->save()) {

        } else {
                print_r($psikotes_result->getErrors());
               }

               //echo 'proyek id =' . $project_id;
               $projectconfig = RefProjectConfig::find()->andWhere(['project_id' => $project_id])->One();
               $subtests = RefPsikotesSubtest::find()->andWhere(['psikotes_set' => $projectconfig->psikotes_set_id])
                    ->orderBy(['order'=>SORT_ASC])
                    ->All();

    $indexsubtest = 0;

    foreach ($subtests as $subtest){
                    $col_subtest = $kolomawalsubtest + 1 + (3 * $indexsubtest);
            $truecount = $worksheet->getCellByColumnAndRow($col_subtest, $row_loop)->getValue();
             $falsecount = $worksheet->getCellByColumnAndRow($col_subtest + 1, $row_loop)->getValue();
             $blankcount = $worksheet->getCellByColumnAndRow($col_subtest + 2, $row_loop)->getValue();

           $psikotes_subtest_result = new PsikotesSubtestResult;
           $psikotes_subtest_result->psikotes_set = $projectconfig->psikotes_set_id;
           $psikotes_subtest_result->psikotes_subtest_name = $subtest->subtest_name;
                  $psikotes_subtest_result->psikotes_result_id = $psikotes_result->id;


           if (strtolower($subtest->subtest_name) != 'kostick') {
                     $psikotes_subtest_result->true_count = $truecount;
                     $psikotes_subtest_result->false_count = $falsecount;
                     $psikotes_subtest_result->blank_count = $blankcount;
                    } else if (strtolower($subtest->subtest_name) == 'kostick')
                    {
                     $psikotes_subtest_result->data = $truecount;
                    }
           if($psikotes_subtest_result->save()){}else {
            print_r($psikotes_subtest_result->getErrors());}

          $indexsubtest++;
         //  echo '<br/>';

          }


}








     }


echo '</pre>';



     /*
     for ($row = $barisawalsubtest; $row <= $highestRow; ++$row) {



       for ($col = 0; $col <= $kolomawalsubtest; ++$col) {



       }









}
*/
//Yii::$app->getSession()->setFlash('success', 'psikotes di proses');
//return $this->redirect(['file/index']);

if ($max_row_index_to_be_processed == $highestRow) {
     return true;
    } else {
     return $this->redirect(['processresult', 'id'=>$id, 'project_id' => $project_id, 'start_row' => ($max_row_index_to_be_processed + 1), 'max_rows' => $max_rows]);
     //$id, $project_id,$start_row, $max_rows
    }


    }


public function actionProcessresult($id, $project_id, $start_row, $max_rows) {
echo 'max  = ';
}


    /**
     * Deletes an existing Assessment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Assessment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Assessment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Assessment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
