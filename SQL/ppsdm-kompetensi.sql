CREATE TABLE `pegawai` (
`id` int NULL AUTO_INCREMENT,
`nama_pegawai` varchar(255) NULL,
`gol_pegawai_id` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `asesor` (
`id` int NULL AUTO_INCREMENT,
`kode_asesor` varchar(255) NULL,
`gol_asesor_id` int(255) NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `ref_competency` (
`id` int NULL AUTO_INCREMENT,
`competency_set_id` int NULL,
`competency` varchar(255) NULL,
`sub_competency` varchar(255) NULL,
`false` varchar(255) NULL,
`true` varchar(255) NULL,
`linkage` int NULL,
`order` int NULL,
PRIMARY KEY (`id`) ,
UNIQUE INDEX `id kompetensi` (`competency_set_id`, `false`)
);

CREATE TABLE `observation` (
`id` bigint NULL AUTO_INCREMENT,
`assessment_id` int NULL,
`competency_set_id` int(255) NULL,
`warteg_depth` varchar(255) NULL,
`warteg_detail` varchar(255) NULL,
`warteg_finesse` varchar(255) NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `result` (
`id` int NULL AUTO_INCREMENT,
`observation_id` bigint NULL,
`pegawai_id` int NULL,
`assessor_id` int NULL,
`kode_daerah` varchar(255) NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `kuesioner` (
`id` int NULL AUTO_INCREMENT,
`result_id` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `result_competency` (
`id` bigint NULL AUTO_INCREMENT,
`observation_id` bigint NULL,
`competency_id` int(255) NULL,
`result` varchar(255) NULL,
`description` varchar(255) NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `observation_group` (
`id` int NULL AUTO_INCREMENT,
`observation_id` bigint NULL,
`group_meja` varchar(255) NULL,
`meja_awal` int NULL,
`meja_akhir` int NULL,
`no_meja` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `assessment` (
`id` int NULL AUTO_INCREMENT,
`assessment_name` varchar(255) NULL,
`project_id` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `project` (
`id` int NULL AUTO_INCREMENT,
`project_name` varchar(255) NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `file` (
`id` int NULL AUTO_INCREMENT,
`file_location` text NULL,
`created_at` datetime NULL,
`modified_at` datetime NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `data` (
`id` int NULL AUTO_INCREMENT,
`file_id` int NULL,
`data` text NULL,
`status` enum('unprocessed','processed') NULL,
`created_at` datetime NULL,
`modified_at` datetime NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `ref_competency_set` (
`id` int NULL AUTO_INCREMENT,
`competency_set_name` varchar(255) NULL,
`competency_list` varchar(255) NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `ref_gol_pegawai` (
`id` int NULL AUTO_INCREMENT,
`nama_golongan_pegawai` varchar(255) NULL,
`kategori_golongan` varchar(255) NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `asesor_competency` (
`id` bigint NULL AUTO_INCREMENT,
`gol_asesor_id` int(255) NULL,
`competency_set_id` int(255) NULL,
`competency_name` varchar(255) NULL,
`value` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `pegawai_competency` (
`id` bigint NULL AUTO_INCREMENT,
`gol_pegawai_id` int NULL,
`competency_set_id` int NULL,
`competency_name` varchar(255) NULL,
`target` int NULL,
`minimum` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `ref_gol_asesor` (
`id` int NULL AUTO_INCREMENT,
`nama_golongan_asesor` varchar(255) NULL,
`kategori_golongan` varchar(255) NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `ref_uraian` (
`id` int NULL AUTO_INCREMENT,
`competency_id` int NULL,
`kode` varchar(255) NULL,
`uraian` text NULL,
`seed` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `ref_saran` (
`id` int NULL AUTO_INCREMENT,
`competency_set_id` int NULL,
`competency_name` varchar(255) NULL,
`uraian` text NULL,
`seed` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `psikotes_data` (
`id` int NULL AUTO_INCREMENT,
`file_id` int NULL,
`data` text NULL,
`status` enum('unprocessed','processed') NULL,
`created_at` datetime NULL,
`modified_at` datetime NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `psikotes_file` (
`id` int NULL AUTO_INCREMENT,
`file_location` text NULL,
`created_at` datetime NULL,
`modified_at` datetime NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `ref_psikotes_subtest` (
`id` int NULL,
`psikotes_id` int NULL,
`subtest_name` varchar(255) NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `psikotes` (
`id` int NULL AUTO_INCREMENT,
`psikotes_name` varchar(255) NULL,
`project_id` int NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `psikotes_subtest_result` (
`id` bigint NULL AUTO_INCREMENT,
`psikotes_subtest_id` int NULL,
`result` text NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `psikotes_result` (
`id` int NULL AUTO_INCREMENT,
`psikotes_id` int NULL,
`no_meja` varchar(255) NULL,
`nama_peserta` varchar(255) NULL,
`nip` varchar(255) NULL,
`kode_buku` varchar(255) NULL,
`tanggal_tes` varchar(255) NULL,
`tanggal_lahir` varchar(255) NULL,
`usia` varchar(255) NULL,
`rumpun` varchar(255) NULL,
`pendidikan` varchar(255) NULL,
`jabatan` varchar(255) NULL,
`golkeg1` varchar(255) NULL,
`golkeg2` varchar(255) NULL,
PRIMARY KEY (`id`) 
);


ALTER TABLE `kuesioner` ADD CONSTRAINT `fk_kuesioner_assessment_1` FOREIGN KEY (`result_id`) REFERENCES `result` (`id`);
ALTER TABLE `result_competency` ADD CONSTRAINT `fk_result_competency_observation_1` FOREIGN KEY (`observation_id`) REFERENCES `observation` (`id`);
ALTER TABLE `result` ADD CONSTRAINT `fk_assessment_pegawai_1` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawai` (`id`);
ALTER TABLE `result` ADD CONSTRAINT `fk_assessment_asesor_1` FOREIGN KEY (`assessor_id`) REFERENCES `asesor` (`id`);
ALTER TABLE `observation_group` ADD CONSTRAINT `fk_observation_group_observation_1` FOREIGN KEY (`observation_id`) REFERENCES `observation` (`id`);
ALTER TABLE `assessment` ADD CONSTRAINT `fk_configuration_assessment_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);
ALTER TABLE `data` ADD CONSTRAINT `fk_data_file_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`);
ALTER TABLE `observation` ADD CONSTRAINT `fk_observation_ref_competency_set_1` FOREIGN KEY (`competency_set_id`) REFERENCES `ref_competency_set` (`id`);
ALTER TABLE `ref_competency` ADD CONSTRAINT `fk_ref_competency_ref_competency_set_1` FOREIGN KEY (`competency_set_id`) REFERENCES `ref_competency_set` (`id`);
ALTER TABLE `result_competency` ADD CONSTRAINT `fk_result_competency_ref_competency_1` FOREIGN KEY (`competency_id`) REFERENCES `ref_competency` (`id`);
ALTER TABLE `ref_competency` ADD CONSTRAINT `fk_ref_competency_ref_competency_1` FOREIGN KEY (`linkage`) REFERENCES `ref_competency` (`id`);
ALTER TABLE `observation` ADD CONSTRAINT `fk_observation_assessment_1` FOREIGN KEY (`assessment_id`) REFERENCES `assessment` (`id`);
ALTER TABLE `result` ADD CONSTRAINT `fk_result_observation_1` FOREIGN KEY (`observation_id`) REFERENCES `observation` (`id`);
ALTER TABLE `pegawai_competency` ADD CONSTRAINT `fk_golongan_competency_ref_golongan_1` FOREIGN KEY (`gol_pegawai_id`) REFERENCES `ref_gol_pegawai` (`id`);
ALTER TABLE `pegawai` ADD CONSTRAINT `fk_pegawai_ref_golongan_1` FOREIGN KEY (`gol_pegawai_id`) REFERENCES `ref_gol_pegawai` (`id`);
ALTER TABLE `asesor` ADD CONSTRAINT `fk_asesor_ref_gol_asesor_1` FOREIGN KEY (`gol_asesor_id`) REFERENCES `ref_gol_asesor` (`id`);
ALTER TABLE `asesor_competency` ADD CONSTRAINT `fk_assessor_adjustment_ref_gol_asesor_1` FOREIGN KEY (`gol_asesor_id`) REFERENCES `ref_gol_asesor` (`id`);
ALTER TABLE `asesor_competency` ADD CONSTRAINT `fk_asesor_competency_ref_competency_set_1` FOREIGN KEY (`competency_set_id`) REFERENCES `ref_competency_set` (`id`);
ALTER TABLE `pegawai_competency` ADD CONSTRAINT `fk_pegawai_competency_ref_competency_set_1` FOREIGN KEY (`competency_set_id`) REFERENCES `ref_competency_set` (`id`);
ALTER TABLE `ref_uraian` ADD CONSTRAINT `fk_ref_uraian_ref_competency_1` FOREIGN KEY (`competency_id`) REFERENCES `ref_competency` (`id`);
ALTER TABLE `ref_saran` ADD CONSTRAINT `fk_ref_saran_ref_competency_set_1` FOREIGN KEY (`competency_set_id`) REFERENCES `ref_competency_set` (`id`);
ALTER TABLE `psikotes_data` ADD CONSTRAINT `fk_psikotes_data_psikotes_file_1` FOREIGN KEY (`file_id`) REFERENCES `psikotes_file` (`id`);
ALTER TABLE `psikotes` ADD CONSTRAINT `fk_psikotes_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);
ALTER TABLE `ref_psikotes_subtest` ADD CONSTRAINT `fk_ref_psikotes_subtest_psikotes_1` FOREIGN KEY (`psikotes_id`) REFERENCES `psikotes` (`id`);
ALTER TABLE `psikotes_subtest_result` ADD CONSTRAINT `fk_psikotes_result_ref_psikotes_subtest_1` FOREIGN KEY (`psikotes_subtest_id`) REFERENCES `ref_psikotes_subtest` (`id`);
ALTER TABLE `psikotes_result` ADD CONSTRAINT `fk_psikotes_result_psikotes_1` FOREIGN KEY (`psikotes_id`) REFERENCES `psikotes` (`id`);

