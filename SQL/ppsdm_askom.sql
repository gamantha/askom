/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : ppsdm_askom

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-07-27 13:27:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `asesor`
-- ----------------------------
DROP TABLE IF EXISTS `asesor`;
CREATE TABLE `asesor` (
  `id` varchar(255) NOT NULL,
  `golongan_asesor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_asesor_asesor_competency_1` (`golongan_asesor`),
  CONSTRAINT `fk_asesor_asesor_competency_1` FOREIGN KEY (`golongan_asesor`) REFERENCES `ref_golongan_asesor` (`golongan_asesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of asesor
-- ----------------------------
INSERT INTO `asesor` VALUES ('A', null);

-- ----------------------------
-- Table structure for `assessment`
-- ----------------------------
DROP TABLE IF EXISTS `assessment`;
CREATE TABLE `assessment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assessment_name` varchar(255) DEFAULT NULL,
  `project_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_assessment_project_1` (`project_id`),
  CONSTRAINT `fk_assessment_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13584 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of assessment
-- ----------------------------
INSERT INTO `assessment` VALUES ('13084', 'ARWIN RAFIT HIDAYAT_198228022002121003', 'bps');
INSERT INTO `assessment` VALUES ('13085', 'JURLUWIS_196208171988031004', 'bps');
INSERT INTO `assessment` VALUES ('13086', 'ABDUL HARIS_196107301982031001', 'bps');
INSERT INTO `assessment` VALUES ('13087', 'SUBARNO_198303062014061003', 'bps');

-- ----------------------------
-- Table structure for `data`
-- ----------------------------
DROP TABLE IF EXISTS `data`;
CREATE TABLE `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) DEFAULT NULL,
  `data` text,
  `status` enum('unprocessed','processed') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `tipe` enum('linkage','psikotes','observasi') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_data_file_1` (`file_id`),
  CONSTRAINT `fk_data_file_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13584 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data
-- ----------------------------

-- ----------------------------
-- Table structure for `file`
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` varchar(255) DEFAULT NULL,
  `file_location` text,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_file_project_1` (`project_id`),
  CONSTRAINT `fk_file_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of file
-- ----------------------------
INSERT INTO `file` VALUES ('7', 'bps', 'UJI COBA.xlsx', '2016-07-10 18:15:20', null);
INSERT INTO `file` VALUES ('8', 'bps', 'UJI COBA.xlsx', '2016-07-13 11:13:20', null);
INSERT INTO `file` VALUES ('9', 'bps', 'BPS_SEMUA.xlsx', '2016-07-22 16:43:20', null);
INSERT INTO `file` VALUES ('10', 'bps', 'BPS_PSIKOTES_ONLY.xlsx', '2016-07-26 12:11:09', null);

-- ----------------------------
-- Table structure for `institusi`
-- ----------------------------
DROP TABLE IF EXISTS `institusi`;
CREATE TABLE `institusi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_institusi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of institusi
-- ----------------------------

-- ----------------------------
-- Table structure for `jabatan`
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` int(11) DEFAULT NULL,
  `institusi_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jabatan_pegawai_1` (`pegawai_id`),
  KEY `fk_jabatan_institusi_1` (`institusi_id`),
  CONSTRAINT `fk_jabatan_institusi_1` FOREIGN KEY (`institusi_id`) REFERENCES `institusi` (`id`),
  CONSTRAINT `fk_jabatan_pegawai_1` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawai` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jabatan
-- ----------------------------

-- ----------------------------
-- Table structure for `observation`
-- ----------------------------
DROP TABLE IF EXISTS `observation`;
CREATE TABLE `observation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assessment_id` bigint(20) DEFAULT NULL,
  `warteg_depth` varchar(255) DEFAULT NULL,
  `warteg_detail` varchar(255) DEFAULT NULL,
  `warteg_finesse` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_observation_assessment_1` (`assessment_id`),
  CONSTRAINT `fk_observation_assessment_1` FOREIGN KEY (`assessment_id`) REFERENCES `assessment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of observation
-- ----------------------------

-- ----------------------------
-- Table structure for `observation_group`
-- ----------------------------
DROP TABLE IF EXISTS `observation_group`;
CREATE TABLE `observation_group` (
  `observation_id` bigint(20) NOT NULL,
  `group_meja` varchar(255) DEFAULT NULL,
  `meja_awal` int(11) DEFAULT NULL,
  `meja_akhir` int(11) DEFAULT NULL,
  `no_meja` int(11) NOT NULL,
  PRIMARY KEY (`observation_id`,`no_meja`),
  CONSTRAINT `fk_observation_group_observation_1` FOREIGN KEY (`observation_id`) REFERENCES `observation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of observation_group
-- ----------------------------

-- ----------------------------
-- Table structure for `pegawai`
-- ----------------------------
DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(255) DEFAULT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `usia` varchar(255) DEFAULT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9398 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pegawai
-- ----------------------------

-- ----------------------------
-- Table structure for `pegawai_ext`
-- ----------------------------
DROP TABLE IF EXISTS `pegawai_ext`;
CREATE TABLE `pegawai_ext` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nip` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26089 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pegawai_ext
-- ----------------------------

-- ----------------------------
-- Table structure for `project`
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `project_id` varchar(255) NOT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('bps', 'Biro Pusat Statistik');
INSERT INTO `project` VALUES ('lipi', 'LIPI');

-- ----------------------------
-- Table structure for `psikotes_result`
-- ----------------------------
DROP TABLE IF EXISTS `psikotes_result`;
CREATE TABLE `psikotes_result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assessment_id` bigint(20) DEFAULT NULL,
  `no_meja` varchar(255) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `kode_buku` varchar(255) DEFAULT NULL,
  `tanggal_tes` varchar(255) DEFAULT NULL,
  `kode_daerah` varchar(255) DEFAULT NULL,
  `asesor_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_psikotes_result_assessment_1` (`assessment_id`),
  KEY `fk_psikotes_result_pegawai_1` (`pegawai_id`),
  CONSTRAINT `fk_psikotes_result_assessment_1` FOREIGN KEY (`assessment_id`) REFERENCES `assessment` (`id`),
  CONSTRAINT `fk_psikotes_result_pegawai_1` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawai` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9501 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of psikotes_result
-- ----------------------------

-- ----------------------------
-- Table structure for `psikotes_subtest_result`
-- ----------------------------
DROP TABLE IF EXISTS `psikotes_subtest_result`;
CREATE TABLE `psikotes_subtest_result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `psikotes_set` varchar(255) DEFAULT NULL,
  `psikotes_subtest_name` varchar(255) DEFAULT NULL,
  `psikotes_result_id` bigint(20) DEFAULT NULL,
  `data` text,
  `true_count` int(11) DEFAULT NULL,
  `false_count` int(11) DEFAULT NULL,
  `blank_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_psikotes_subtest_result_psikotes_result_1` (`psikotes_result_id`),
  KEY `fk_psikotes_subtest_result_ref_psikotes_subtest_1` (`psikotes_set`,`psikotes_subtest_name`),
  CONSTRAINT `fk_psikotes_subtest_result_psikotes_result_1` FOREIGN KEY (`psikotes_result_id`) REFERENCES `psikotes_result` (`id`),
  CONSTRAINT `fk_psikotes_subtest_result_ref_psikotes_subtest_1` FOREIGN KEY (`psikotes_set`, `psikotes_subtest_name`) REFERENCES `ref_psikotes_subtest` (`psikotes_set`, `subtest_name`)
) ENGINE=InnoDB AUTO_INCREMENT=75980 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of psikotes_subtest_result
-- ----------------------------

-- ----------------------------
-- Table structure for `ref_competency`
-- ----------------------------
DROP TABLE IF EXISTS `ref_competency`;
CREATE TABLE `ref_competency` (
  `competency_set` varchar(255) NOT NULL,
  `competency` varchar(255) NOT NULL,
  `sub_competency` varchar(255) NOT NULL,
  `false` varchar(255) DEFAULT NULL,
  `true` varchar(255) DEFAULT NULL,
  `linkage` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `uselinkage` enum('false','true') DEFAULT NULL,
  PRIMARY KEY (`competency_set`,`competency`,`sub_competency`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_competency
-- ----------------------------
INSERT INTO `ref_competency` VALUES ('', 'manual (no competency set id)', '', '', '', '', null, '');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada kualitas', 'A1-BPK', '', '', 'semangat berprestasi:insightful', '51', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada kualitas', 'A2-BPK', '', '', 'inovasi:changes', '52', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada kualitas', 'A3-BPK', '', '', 'inovasi:added values', '53', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada kualitas', 'knowledge', '', '', '', '54', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada kualitas', 'skill', '', '', '', '55', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada pelayanan', 'A1-BPK', '', '', 'semangat berprestasi:insightful', '56', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada pelayanan', 'A2-BPK', '', '', 'integritas:steady & consistent', '57', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada pelayanan', 'A3-BPK', '', '', 'komunikasi lisan:building-trust', '58', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada pelayanan', 'knowledge', '', '', '', '59', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berorientasi pada pelayanan', 'skill', '', '', '', '60', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berpikir analitis', 'A1-BA', '', '', 'komunikasi lisan:understanding', '32', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berpikir analitis', 'knowledge', '', '', '', '33', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berpikir analitis', 'skill', '', '', '', '34', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berpikir konseptual', 'A1-BK', '', '', 'komunikasi lisan:systematic', '35', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berpikir konseptual', 'knowledge', '', '', '', '36', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'berpikir konseptual', 'skill', '', '', '', '37', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'empati', 'A1-E', '', '', 'integritas:social awareness', '69', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'empati', 'A2-E', '', '', 'kerjasama:prosocial behavior', '70', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'empati', 'knowledge', '', '', '', '71', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'empati', 'skill', '', '', '', '72', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'fleksibilitas berpikir', 'A1-FB', '', '', 'semangat berprestasi:insightful', '28', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'fleksibilitas berpikir', 'A2-FB', '', '', 'inovasi:changes', '29', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'fleksibilitas berpikir', 'knowledge', '', '', '', '30', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'fleksibilitas berpikir', 'skill', '', '', '', '31', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'inisiatif', 'A1-INI', '', '', 'kerjasama:confident', '47', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'inisiatif', 'A2-INI', '', '', 'inovasi:independent', '48', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'inisiatif', 'knowledge', '', '', '', '49', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'inisiatif', 'skill', '', '', '', '50', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'inovasi', 'added values', 'stuck', 'improving', '', '19', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'inovasi', 'changes', 'following', 'promoting', '', '18', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'inovasi', 'independent', 'submissive', 'finishing', '', '17', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'inovasi', 'knowledge', '', '', '', '20', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'inovasi', 'skill', '', '', '', '21', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'integritas', 'firm & consequent', 'fleet/shift', 'persistent', '', '3', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'integritas', 'knowledge', '', '', '', '4', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'integritas', 'self-control', 'defensive', 'balance', '', '1', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'integritas', 'skill', '', '', '', '5', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'integritas', 'social awareness', 'careless', 'ethical', '', '0', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'integritas', 'steady & consistent', 'inconsistent', 'accountable', '', '2', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'interaksi sosial', 'A1-IS', '', '', 'integritas:social awareness', '73', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'interaksi sosial', 'A2-IS', '', '', 'kerjasama:prosocial behavior', '74', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'interaksi sosial', 'A3-IS', '', '', 'integritas:steady & consistent', '75', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'interaksi sosial', 'knowledge', '', '', '', '76', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'interaksi sosial', 'skill', '', '', '', '77', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'kepemimpinan', 'A1-KP', '', '', 'kerjasama:prosocial behavior', '83', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'kepemimpinan', 'A2-KP', '', '', 'integritas:steady & consistent/', '84', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'kepemimpinan', 'A3-KP', '', '', 'komunikasi lisan:building-trust', '85', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'kepemimpinan', 'A4-KP', '', '', 'integritas:firm & consequent', '86', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'kepemimpinan', 'knowledge', '', '', '', '87', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'kepemimpinan', 'skill', '', '', '', '88', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'kerjasama', 'confident', 'passive', 'assertive', '', '12', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'kerjasama', 'knowledge', '', '', '', '15', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'kerjasama', 'prosocial behavior', 'solitaire', 'accomodative', '', '13', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'kerjasama', 'skill', '', '', '', '16', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'kerjasama', 'spirit of unity', 'disharmony', 'solid', '', '14', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'keuletan', 'A1-KEU', '', '', 'semangat berprestasi:irresistent', '43', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'keuletan', 'A2-KEU', '', '', 'semangat berprestasi:effortful', '44', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'keuletan', 'knowledge', '', '', '', '45', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'keuletan', 'skill', '', '', '', '46', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'komitmen terhadap organisasi', 'A1-KTO', '', '', 'integritas:self-control', '38', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'komitmen terhadap organisasi', 'A2-KTO', '', '', 'kerjasama:spirit of unity', '39', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'komitmen terhadap organisasi', 'A3-KTO', '', '', 'semangat berprestasi:resourceful', '40', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'komitmen terhadap organisasi', 'knowledge', '', '', '', '41', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'komitmen terhadap organisasi', 'skill', '', '', '', '42', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'komunikasi lisan', 'building-trust', 'impactless', 'persuasive', '', '25', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'komunikasi lisan', 'knowledge', '', '', '', '26', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'komunikasi lisan', 'open & clear', 'discrete', 'fluent', '', '22', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'komunikasi lisan', 'skill', '', '', '', '27', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'komunikasi lisan', 'systematic', 'random', 'patternize', '', '24', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'komunikasi lisan', 'understanding', 'incomplete', 'complete', '', '23', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'membangun hubungan kerja', 'A1-MHK', '', '', 'kerjasama:prosocial behavior', '78', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'membangun hubungan kerja', 'A2-MHK', '', '', 'integritas:self-control', '79', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'membangun hubungan kerja', 'A3-MHK', '', '', 'komunikasi lisan:building-trust', '80', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'membangun hubungan kerja', 'knowledge', '', '', '', '81', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'membangun hubungan kerja', 'skill', '', '', '', '82', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'negosiasi', 'A1-NEG', '', '', 'kerjasama:prosocial behavior', '89', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'negosiasi', 'A2-NEG', '', '', 'komunikasi lisan:building-trust', '90', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'negosiasi', 'knowledge', '', '', '', '91', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'negosiasi', 'skill', '', '', '', '92', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pencarian informasi', 'A1-PI', '', '', 'kerjasama:confident', '61', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pencarian informasi', 'A2-PI', '', '', 'semangat berprestasi:insightful', '62', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pencarian informasi', 'knowledge', '', '', '', '63', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pencarian informasi', 'skill', '', '', '', '64', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengambilan keputusan', 'A1-PK', '', '', '', '105', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengambilan keputusan', 'A2-PK', '', '', '', '106', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengambilan keputusan', 'A3-PK', '', '', '', '107', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengambilan keputusan', 'A4-PK', '', '', '', '108', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengambilan keputusan', 'knowledge', '', '', '', '109', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengambilan keputusan', 'skill', '', '', '', '110', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengorganisasian', 'A1-P', '', '', '', '99', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengorganisasian', 'A2-P', '', '', '', '100', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengorganisasian', 'A3-P', '', '', '', '101', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengorganisasian', 'A4-P', '', '', '', '102', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengorganisasian', 'knowledge', '', '', '', '103', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'pengorganisasian', 'skill', '', '', '', '104', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'perencanaan', 'A1-PER', '', '', '', '93', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'perencanaan', 'A2-PER', '', '', '', '94', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'perencanaan', 'A3-PER', '', '', '', '95', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'perencanaan', 'A4-PER', '', '', '', '96', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'perencanaan', 'knowledge', '', '', '', '97', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'perencanaan', 'skill', '', '', '', '98', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'semangat berprestasi', 'effortful', 'give up', 'struggle', '', '8', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'semangat berprestasi', 'insightful', 'usual', 'exceed', '', '7', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'semangat berprestasi', 'irresistent', 'wasting', 'focus', '', '6', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'semangat berprestasi', 'knowledge', '', '', '', '10', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'semangat berprestasi', 'resourceful', 'regular', 'optimize', '', '9', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'semangat berprestasi', 'skill', '', '', '', '11', 'false');
INSERT INTO `ref_competency` VALUES ('1', 'tanggap terhadap pengaruh budaya', 'A1-TPB', '', '', 'komunikasi lisan:understanding', '65', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'tanggap terhadap pengaruh budaya', 'A2-TPB', '', '', 'integritas:social awareness', '66', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'tanggap terhadap pengaruh budaya', 'knowledge', '', '', '', '67', 'true');
INSERT INTO `ref_competency` VALUES ('1', 'tanggap terhadap pengaruh budaya', 'skill', '', '', '', '68', 'true');

-- ----------------------------
-- Table structure for `ref_definisi`
-- ----------------------------
DROP TABLE IF EXISTS `ref_definisi`;
CREATE TABLE `ref_definisi` (
  `key` varchar(255) NOT NULL,
  `value` text,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`key`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_definisi
-- ----------------------------
INSERT INTO `ref_definisi` VALUES ('', null, '');
INSERT INTO `ref_definisi` VALUES ('0', 'terbatas/spesifik', 'aspek_potensial_scale');
INSERT INTO `ref_definisi` VALUES ('0', 'kurang sesuai', 'job_fit_scale');
INSERT INTO `ref_definisi` VALUES ('0', 'lambat/slow learner', 'kemampuan_belajar_scale');
INSERT INTO `ref_definisi` VALUES ('3', 'beragam', 'aspek_potensial_scale');
INSERT INTO `ref_definisi` VALUES ('3', 'cukup sesuai', 'job_fit_scale');
INSERT INTO `ref_definisi` VALUES ('3', 'medium/soft learner', 'kemampuan_belajar_scale');
INSERT INTO `ref_definisi` VALUES ('6', 'luas', 'aspek_potensial_scale');
INSERT INTO `ref_definisi` VALUES ('6', 'sangat sesuai', 'job_fit_scale');
INSERT INTO `ref_definisi` VALUES ('6', 'cepat/fast learner', 'kemampuan_belajar_scale');
INSERT INTO `ref_definisi` VALUES ('a', 'Aceh', 'kode_daerah');
INSERT INTO `ref_definisi` VALUES ('beragam', 'Potensi pengembangan diri Yang Bersangkutan mengindikasikan optimalitas pada area yang cukup beragam, artinya kemampuan dan arah minatnya sedikit meluas keluar bidang-bidang tugas serumpun.', 'aspek_potensial');
INSERT INTO `ref_definisi` VALUES ('berorientasi pada kualitas', 'Kesediaan mengelaborasi sasaran kualitas dari tugas-tugas yang diberikan serta perhatian pada keakuratan dan kelengkapan tugas untuk memenuhi standar kualitas terbaik.', 'competency');
INSERT INTO `ref_definisi` VALUES ('berorientasi pada pelayanan', 'Kesediaan menelusuri kebutuhan serta kualitas layanan yang diharapkan untuk memenuhi kebutuhan dan memberikan kepuasan / layanan terbaik kepada pengguna jasa.', 'competency');
INSERT INTO `ref_definisi` VALUES ('berpikir analitis', 'Kemampuan mengidentifikasi dan klasifikasi faktor-faktor terkait / relevan dengan permasalahan untuk membedakan faktor utama dari faktor lain dalam memahami dan merumusKan masalah.', 'competency');
INSERT INTO `ref_definisi` VALUES ('berpikir konseptual', 'Kemampuan mengelaborasi dan menalar fenomena secara kritis dan menyimpulkan hubungan antar faktor yang relevan untuk memperoleh prinsip atau konsep berpikir yang tepat dan lengkap.', 'competency');
INSERT INTO `ref_definisi` VALUES ('cepat/fast learner', 'Yang Bersangkutan memiliki kemampuan belajar dan beradaptasi dengan tantangan baru secara cepat dan efektif yang mendukungnya untuk berkembang hingga jenjang jabatan tertinggi.', 'kemampuan_belajar');
INSERT INTO `ref_definisi` VALUES ('cukup sesuai', 'Yang Bersangkutan telah dapat menampilkan kemampuan dan karakter dominan cukup sesuai dengan tingkat kemahiran yang dipersyaratkan dalam jabatannya saat ini.', 'job_fit');
INSERT INTO `ref_definisi` VALUES ('empati', 'Sensitifitas dan kepedulian terhadap permasalahan dan suasana hati orang lain untuk membangun dan mempertahankan suasana kerja yang kondusif.', 'competency');
INSERT INTO `ref_definisi` VALUES ('fleksibilitas berpikir', 'Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode eklektik untuk menyempurnakan cara berpikir dan dan menemukan pendekatan yang lebih sesuai.', 'competency');
INSERT INTO `ref_definisi` VALUES ('inisiatif', 'Keaktifan dan kemandirian dalam menyelesaikan tugas / masalah, dalam melakukan perbaikan proses kerja, atau pun dalam meningkatkan produktifitas dan kualitas hasil kerja.', 'competency');
INSERT INTO `ref_definisi` VALUES ('inovasi', 'Kemampuan mengimplementasi, mengeksplorasi dan elaborasi gagasan perbaikan untuk memberi nilai tambah terhadap proses kerja atau pun untuk memberi manfaat bagi stakeholder.', 'competency');
INSERT INTO `ref_definisi` VALUES ('integritas', 'Kemampuan menginternalisasi dan mengembangkan etika / nilai-nilai organisasi untuk menjaga kehormatan pribadi serta menegakkan kebenaran dan keterbukaan di dalam organisasi.', 'competency');
INSERT INTO `ref_definisi` VALUES ('interaksi sosial', 'Kemampuan mengembangkan pemikiran dan sikap yang sesuai / dapat diterima oleh lingkungan sosial untuk membangun suasana yang harmonis serta hubungan jangka panjang.', 'competency');
INSERT INTO `ref_definisi` VALUES ('kepemimpinan', 'Membantu, mempengaruhi, mendorong, mengarahkan, serta mendapatkan komitmen kelompok untuk menjalankan fungsi dan memenuhi tanggung jawab kepada organisasi.', 'competency');
INSERT INTO `ref_definisi` VALUES ('kerjasama', 'Kemampuan membangun semangat kebersamaan dan kesediaan membangun sinergi dalam kelompok kerja untuk memperlancar proses dan mempercepat pencapaian tujuan kelompok.', 'competency');
INSERT INTO `ref_definisi` VALUES ('keuletan', 'Semangat dan ketabahan menghadapi tekanan dan kegagalan serta kegigihan dalam memikul tanggung jawab untuk dapat mewujudkan keberhasilan.', 'competency');
INSERT INTO `ref_definisi` VALUES ('komitmen terhadap organisasi', 'Kemampuan menyelaraskan perilaku dan tujuan pribadi dengan tujuan organisasi untuk mengotimalkan hasil atau mempercepat pencapaian tujuan organisasi.', 'competency');
INSERT INTO `ref_definisi` VALUES ('komunikasi lisan', 'Kemampuan menyampaikan dan menangkap pesan menggunakan bahasa yang tepat serta tutur yang jelas dan runtut untuk memperoleh saling pengertian.', 'competency');
INSERT INTO `ref_definisi` VALUES ('kurang sesuai', 'Yang Bersangkutan telah dapat menampilkan kemampuan dan karakter dominan namun kurang sesuai dengan tingkat kemahiran yang dipersyaratkan dalam jabatannya saat ini.', 'job_fit');
INSERT INTO `ref_definisi` VALUES ('lambat/slow learner', 'Yang Bersangkutan memiliki kemampuan belajar dan beradaptasi dengan tantangan yang tergolong lambat dan kurang efektif, yang menjadi penghambatnya untuk berkembang hingga jenjang jabatan tertinggi.', 'kemampuan_belajar');
INSERT INTO `ref_definisi` VALUES ('luas', 'Potensi pengembangan diri Yang Bersangkutan mengindikasikan optimalitas pada area yang luas, artinya kemampuan dan arah minatnya meluas pada bidang-bidang di luar tugas-tugas serumpun.', 'aspek_potensial');
INSERT INTO `ref_definisi` VALUES ('medium/soft learner', 'Yang Bersangkutan memiliki kemampuan belajar dan beradaptasi dengan tantangan baru secara normal dan cukup efektif yang masih memungkinkannya berkembang hingga jenjang jabatan tertinggi.', 'kemampuan_belajar');
INSERT INTO `ref_definisi` VALUES ('membangun hubungan kerja', 'Kemampuan memahami dan mengembangkan manfaat dari hubungan antar lembaga untuk memberi nilai tambah bagi organisasi serta meningkatkan kelancaran operasi.', 'competency');
INSERT INTO `ref_definisi` VALUES ('pencarian informasi', 'Kesediaan mencari kejelasan mengenai prinsip-prinsip, konsep, metode, syarat-syarat, standar yang menunjang keberhasilan atau kesempurnaan hasil kerja.', 'competency');
INSERT INTO `ref_definisi` VALUES ('pengambilan keputusan', 'Kemampuan menentukan sikap / tindakan berdasarkan pertimbangan efektifitas terhadap sasaran, faktualitas, dan analisis manfaat dan risiko, untuk memperoleh hasil terbaik bagi organisasi.', 'competency');
INSERT INTO `ref_definisi` VALUES ('pengorganisasian', 'Kemampuan melakukan pengaturan sumber daya (orang, sarana-prasarana, uang, teknologi, waktu, situasi, dll) untuk memperoleh daya guna (efektifitas dan efisiensi) tertinggi.', 'competency');
INSERT INTO `ref_definisi` VALUES ('perencanaan', 'Kemampuan memahami sasaran dan strategi umum serta cara menyusun langkah-langkah kerja yang efektif untuk mencapai sasaran dan strategi tersebut.', 'competency');
INSERT INTO `ref_definisi` VALUES ('sangat sesuai', 'Yang Bersangkutan telah dapat menampilkan kemampuan dan karakter dominan yang sangat sesuai dengan tingkat kemahiran yang dipersyaratkan dalam jabatannya saat ini.', 'job_fit');
INSERT INTO `ref_definisi` VALUES ('semangat berprestasi', 'Kesiapan menghadapi tantangan yang meningkat serta mempertahankan standar kerja yang tinggi untuk meraih capaian / kinerja terbaik.', 'competency');
INSERT INTO `ref_definisi` VALUES ('tanggap terhadap pengaruh budaya', 'Kesediaan memahami, menghargai, dan menempatkan diri secara layak dalam lingkungan budaya yang beragam untuk menghindari kesalahpahaman dan memperoleh dukungan sosial.', 'competency');
INSERT INTO `ref_definisi` VALUES ('terbatas/spesifik', 'Potensi pengembangan diri Yang Bersangkutan mengindikasikan optimalitas pada area yang terbatas / spesifik, artinya kemampuan dan arah minatnya terbatas pada bidang-bidang tugas serumpun.', 'aspek_potensial');

-- ----------------------------
-- Table structure for `ref_golongan_asesor`
-- ----------------------------
DROP TABLE IF EXISTS `ref_golongan_asesor`;
CREATE TABLE `ref_golongan_asesor` (
  `golongan_asesor` varchar(255) NOT NULL,
  `competency_set_id` varchar(255) NOT NULL,
  `competency_name` varchar(255) NOT NULL,
  `sub_competency` varchar(255) NOT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`golongan_asesor`,`competency_set_id`,`competency_name`,`sub_competency`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_golongan_asesor
-- ----------------------------

-- ----------------------------
-- Table structure for `ref_golongan_pegawai`
-- ----------------------------
DROP TABLE IF EXISTS `ref_golongan_pegawai`;
CREATE TABLE `ref_golongan_pegawai` (
  `golongan_pegawai` varchar(255) NOT NULL,
  `tipe_jabatan` varchar(255) NOT NULL,
  `rumpun` varchar(255) NOT NULL,
  `golkeg1` varchar(255) NOT NULL,
  `golkeg2` varchar(255) NOT NULL,
  PRIMARY KEY (`golongan_pegawai`,`tipe_jabatan`,`rumpun`,`golkeg1`,`golkeg2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_golongan_pegawai
-- ----------------------------
INSERT INTO `ref_golongan_pegawai` VALUES ('eselon_2', 'struktural', 'eselon 2', '', '');
INSERT INTO `ref_golongan_pegawai` VALUES ('eselon_3', 'struktural', 'eselon 3', '', '');
INSERT INTO `ref_golongan_pegawai` VALUES ('eselon_4', 'struktural', 'eselon 4', '', '');
INSERT INTO `ref_golongan_pegawai` VALUES ('ns_gol_1', 'non-struktural', 'ns_gol_1', '1', 'A');
INSERT INTO `ref_golongan_pegawai` VALUES ('ns_gol_2', 'non-struktural', 'ns_gol_2', '2', 'A');
INSERT INTO `ref_golongan_pegawai` VALUES ('ns_gol_3', 'non-struktural', 'ns_gol_3', '3', 'A');
INSERT INTO `ref_golongan_pegawai` VALUES ('ns_gol_4', 'non-struktural', 'ns_gol_4', '4', 'A');

-- ----------------------------
-- Table structure for `ref_golongan_pegawai_adjustment`
-- ----------------------------
DROP TABLE IF EXISTS `ref_golongan_pegawai_adjustment`;
CREATE TABLE `ref_golongan_pegawai_adjustment` (
  `golongan_pegawai` varchar(255) NOT NULL,
  `competency_set_id` varchar(255) NOT NULL,
  `competency` varchar(255) NOT NULL,
  `sub_competency` varchar(255) NOT NULL,
  `target` varchar(255) DEFAULT NULL,
  `minimum` varchar(255) DEFAULT NULL,
  `adjustment` int(11) DEFAULT NULL,
  PRIMARY KEY (`golongan_pegawai`,`competency_set_id`,`competency`,`sub_competency`),
  CONSTRAINT `fk_ref_golongan_pegawai_adjustment_ref_golongan_pegawai_1` FOREIGN KEY (`golongan_pegawai`) REFERENCES `ref_golongan_pegawai` (`golongan_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_golongan_pegawai_adjustment
-- ----------------------------
INSERT INTO `ref_golongan_pegawai_adjustment` VALUES ('eselon_3', '1', 'inovasi', 'NONE', '4', '2', '0');
INSERT INTO `ref_golongan_pegawai_adjustment` VALUES ('eselon_3', '1', 'integritas', 'firm & consequent', '3', '2', '-3');
INSERT INTO `ref_golongan_pegawai_adjustment` VALUES ('eselon_3', '1', 'integritas', 'knowledge', '3', '2', '-1');
INSERT INTO `ref_golongan_pegawai_adjustment` VALUES ('eselon_3', '1', 'integritas', 'NONE', '4', '3', '2');

-- ----------------------------
-- Table structure for `ref_project_config`
-- ----------------------------
DROP TABLE IF EXISTS `ref_project_config`;
CREATE TABLE `ref_project_config` (
  `project_id` varchar(255) DEFAULT NULL,
  `competency_set_id` varchar(255) DEFAULT NULL,
  `psikotes_set_id` varchar(255) DEFAULT NULL,
  KEY `fk_ref_project_config_ref_competency_1` (`competency_set_id`),
  KEY `fk_ref_project_config_project_1` (`project_id`),
  KEY `fk_ref_project_config_ref_psikotes_subtest_1` (`psikotes_set_id`),
  CONSTRAINT `fk_ref_project_config_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`),
  CONSTRAINT `fk_ref_project_config_ref_competency_1` FOREIGN KEY (`competency_set_id`) REFERENCES `ref_competency` (`competency_set`),
  CONSTRAINT `fk_ref_project_config_ref_psikotes_subtest_1` FOREIGN KEY (`psikotes_set_id`) REFERENCES `ref_psikotes_subtest` (`psikotes_set`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_project_config
-- ----------------------------
INSERT INTO `ref_project_config` VALUES ('bps', '1', '1');

-- ----------------------------
-- Table structure for `ref_psikotes_subtest`
-- ----------------------------
DROP TABLE IF EXISTS `ref_psikotes_subtest`;
CREATE TABLE `ref_psikotes_subtest` (
  `psikotes_set` varchar(255) NOT NULL,
  `subtest_name` varchar(255) NOT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`psikotes_set`,`subtest_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_psikotes_subtest
-- ----------------------------
INSERT INTO `ref_psikotes_subtest` VALUES ('1', 'CFIT 1', '1');
INSERT INTO `ref_psikotes_subtest` VALUES ('1', 'CFIT 2', '2');
INSERT INTO `ref_psikotes_subtest` VALUES ('1', 'CFIT 3', '3');
INSERT INTO `ref_psikotes_subtest` VALUES ('1', 'CFIT 4', '4');
INSERT INTO `ref_psikotes_subtest` VALUES ('1', 'KOSTICK', '8');
INSERT INTO `ref_psikotes_subtest` VALUES ('1', 'SUBTEST 5', '5');
INSERT INTO `ref_psikotes_subtest` VALUES ('1', 'SUBTEST 6', '6');
INSERT INTO `ref_psikotes_subtest` VALUES ('1', 'SUBTEST 7', '7');

-- ----------------------------
-- Table structure for `ref_scale`
-- ----------------------------
DROP TABLE IF EXISTS `ref_scale`;
CREATE TABLE `ref_scale` (
  `scale_name` varchar(255) NOT NULL,
  `unscaled` int(11) NOT NULL,
  `scaled` int(11) DEFAULT NULL,
  PRIMARY KEY (`scale_name`,`unscaled`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_scale
-- ----------------------------
INSERT INTO `ref_scale` VALUES ('cfit', '0', '1');
INSERT INTO `ref_scale` VALUES ('cfit', '1', '2');
INSERT INTO `ref_scale` VALUES ('cfit', '3', '3');
INSERT INTO `ref_scale` VALUES ('cfit', '4', '4');
INSERT INTO `ref_scale` VALUES ('cfit', '6', '5');
INSERT INTO `ref_scale` VALUES ('cfit', '7', '6');
INSERT INTO `ref_scale` VALUES ('cfit', '9', '7');
INSERT INTO `ref_scale` VALUES ('cfit', '11', '8');
INSERT INTO `ref_scale` VALUES ('cfit', '12', '9');
INSERT INTO `ref_scale` VALUES ('iq', '0', '38');
INSERT INTO `ref_scale` VALUES ('iq', '1', '40');
INSERT INTO `ref_scale` VALUES ('iq', '2', '43');
INSERT INTO `ref_scale` VALUES ('iq', '3', '45');
INSERT INTO `ref_scale` VALUES ('iq', '4', '47');
INSERT INTO `ref_scale` VALUES ('iq', '5', '48');
INSERT INTO `ref_scale` VALUES ('iq', '6', '52');
INSERT INTO `ref_scale` VALUES ('iq', '7', '55');
INSERT INTO `ref_scale` VALUES ('iq', '8', '57');
INSERT INTO `ref_scale` VALUES ('iq', '9', '60');
INSERT INTO `ref_scale` VALUES ('iq', '10', '63');
INSERT INTO `ref_scale` VALUES ('iq', '11', '67');
INSERT INTO `ref_scale` VALUES ('iq', '12', '70');
INSERT INTO `ref_scale` VALUES ('iq', '13', '72');
INSERT INTO `ref_scale` VALUES ('iq', '14', '75');
INSERT INTO `ref_scale` VALUES ('iq', '15', '78');
INSERT INTO `ref_scale` VALUES ('iq', '16', '81');
INSERT INTO `ref_scale` VALUES ('iq', '17', '85');
INSERT INTO `ref_scale` VALUES ('iq', '18', '88');
INSERT INTO `ref_scale` VALUES ('iq', '19', '91');
INSERT INTO `ref_scale` VALUES ('iq', '20', '94');
INSERT INTO `ref_scale` VALUES ('iq', '21', '96');
INSERT INTO `ref_scale` VALUES ('iq', '22', '100');
INSERT INTO `ref_scale` VALUES ('iq', '23', '103');
INSERT INTO `ref_scale` VALUES ('iq', '24', '106');
INSERT INTO `ref_scale` VALUES ('iq', '25', '109');
INSERT INTO `ref_scale` VALUES ('iq', '26', '113');
INSERT INTO `ref_scale` VALUES ('iq', '27', '116');
INSERT INTO `ref_scale` VALUES ('iq', '28', '117');
INSERT INTO `ref_scale` VALUES ('iq', '29', '119');
INSERT INTO `ref_scale` VALUES ('iq', '30', '124');
INSERT INTO `ref_scale` VALUES ('iq', '31', '128');
INSERT INTO `ref_scale` VALUES ('iq', '32', '130');
INSERT INTO `ref_scale` VALUES ('iq', '33', '130');
INSERT INTO `ref_scale` VALUES ('iq', '34', '130');
INSERT INTO `ref_scale` VALUES ('iq', '35', '130');
INSERT INTO `ref_scale` VALUES ('iq', '36', '130');
INSERT INTO `ref_scale` VALUES ('iq', '37', '130');
INSERT INTO `ref_scale` VALUES ('iq', '38', '130');
INSERT INTO `ref_scale` VALUES ('iq', '39', '130');
INSERT INTO `ref_scale` VALUES ('iq', '40', '130');
INSERT INTO `ref_scale` VALUES ('iq', '41', '130');
INSERT INTO `ref_scale` VALUES ('iq', '42', '130');
INSERT INTO `ref_scale` VALUES ('iq', '43', '130');
INSERT INTO `ref_scale` VALUES ('iq', '44', '130');
INSERT INTO `ref_scale` VALUES ('iq', '45', '130');
INSERT INTO `ref_scale` VALUES ('iq', '46', '130');
INSERT INTO `ref_scale` VALUES ('iq', '47', '130');
INSERT INTO `ref_scale` VALUES ('iq', '48', '130');
INSERT INTO `ref_scale` VALUES ('iq', '49', '130');
INSERT INTO `ref_scale` VALUES ('iq', '50', '130');
INSERT INTO `ref_scale` VALUES ('subtest5', '0', '0');
INSERT INTO `ref_scale` VALUES ('subtest5', '3', '1');
INSERT INTO `ref_scale` VALUES ('subtest5', '7', '2');
INSERT INTO `ref_scale` VALUES ('subtest5', '12', '3');
INSERT INTO `ref_scale` VALUES ('subtest5', '16', '4');
INSERT INTO `ref_scale` VALUES ('subtest5', '21', '5');
INSERT INTO `ref_scale` VALUES ('subtest5', '25', '6');
INSERT INTO `ref_scale` VALUES ('subtest5', '30', '7');
INSERT INTO `ref_scale` VALUES ('subtest5', '34', '8');
INSERT INTO `ref_scale` VALUES ('subtest5', '36', '9');
INSERT INTO `ref_scale` VALUES ('subtest5', '43', '10');
INSERT INTO `ref_scale` VALUES ('subtest5', '47', '11');
INSERT INTO `ref_scale` VALUES ('subtest5', '52', '12');
INSERT INTO `ref_scale` VALUES ('subtest5', '56', '13');
INSERT INTO `ref_scale` VALUES ('subtest5', '61', '14');
INSERT INTO `ref_scale` VALUES ('subtest5', '65', '15');
INSERT INTO `ref_scale` VALUES ('subtest5', '69', '16');
INSERT INTO `ref_scale` VALUES ('subtest5', '74', '17');
INSERT INTO `ref_scale` VALUES ('subtest5', '78', '18');
INSERT INTO `ref_scale` VALUES ('subtest5', '100', '19');
INSERT INTO `ref_scale` VALUES ('subtest5', '130', '20');
INSERT INTO `ref_scale` VALUES ('subtest6', '0', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '1', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '2', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '3', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '4', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '5', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '6', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '7', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '8', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '9', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '10', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '11', '0');
INSERT INTO `ref_scale` VALUES ('subtest6', '12', '1');
INSERT INTO `ref_scale` VALUES ('subtest6', '13', '2');
INSERT INTO `ref_scale` VALUES ('subtest6', '14', '2');
INSERT INTO `ref_scale` VALUES ('subtest6', '15', '3');
INSERT INTO `ref_scale` VALUES ('subtest6', '16', '4');
INSERT INTO `ref_scale` VALUES ('subtest6', '17', '4');
INSERT INTO `ref_scale` VALUES ('subtest6', '18', '5');
INSERT INTO `ref_scale` VALUES ('subtest6', '19', '6');
INSERT INTO `ref_scale` VALUES ('subtest6', '20', '6');
INSERT INTO `ref_scale` VALUES ('subtest6', '21', '7');
INSERT INTO `ref_scale` VALUES ('subtest6', '22', '8');
INSERT INTO `ref_scale` VALUES ('subtest6', '23', '8');
INSERT INTO `ref_scale` VALUES ('subtest6', '24', '9');
INSERT INTO `ref_scale` VALUES ('subtest6', '25', '10');
INSERT INTO `ref_scale` VALUES ('subtest6', '26', '11');
INSERT INTO `ref_scale` VALUES ('subtest6', '27', '11');
INSERT INTO `ref_scale` VALUES ('subtest6', '28', '12');
INSERT INTO `ref_scale` VALUES ('subtest6', '29', '13');
INSERT INTO `ref_scale` VALUES ('subtest6', '30', '13');
INSERT INTO `ref_scale` VALUES ('subtest6', '31', '14');
INSERT INTO `ref_scale` VALUES ('subtest6', '32', '15');
INSERT INTO `ref_scale` VALUES ('subtest6', '33', '15');
INSERT INTO `ref_scale` VALUES ('subtest6', '34', '16');
INSERT INTO `ref_scale` VALUES ('subtest6', '35', '17');
INSERT INTO `ref_scale` VALUES ('subtest6', '36', '17');
INSERT INTO `ref_scale` VALUES ('subtest6', '37', '18');
INSERT INTO `ref_scale` VALUES ('subtest6', '38', '19');
INSERT INTO `ref_scale` VALUES ('subtest6', '39', '19');
INSERT INTO `ref_scale` VALUES ('subtest6', '40', '20');
INSERT INTO `ref_scale` VALUES ('subtest7', '0', '0');
INSERT INTO `ref_scale` VALUES ('subtest7', '1', '3');
INSERT INTO `ref_scale` VALUES ('subtest7', '2', '3');
INSERT INTO `ref_scale` VALUES ('subtest7', '3', '3');
INSERT INTO `ref_scale` VALUES ('subtest7', '4', '3');
INSERT INTO `ref_scale` VALUES ('subtest7', '5', '4');
INSERT INTO `ref_scale` VALUES ('subtest7', '6', '5');
INSERT INTO `ref_scale` VALUES ('subtest7', '7', '6');
INSERT INTO `ref_scale` VALUES ('subtest7', '8', '7');
INSERT INTO `ref_scale` VALUES ('subtest7', '9', '8');
INSERT INTO `ref_scale` VALUES ('subtest7', '10', '9');
INSERT INTO `ref_scale` VALUES ('subtest7', '11', '10');
INSERT INTO `ref_scale` VALUES ('subtest7', '12', '11');
INSERT INTO `ref_scale` VALUES ('subtest7', '13', '12');
INSERT INTO `ref_scale` VALUES ('subtest7', '14', '13');
INSERT INTO `ref_scale` VALUES ('subtest7', '15', '14');
INSERT INTO `ref_scale` VALUES ('subtest7', '16', '15');
INSERT INTO `ref_scale` VALUES ('subtest7', '17', '16');
INSERT INTO `ref_scale` VALUES ('subtest7', '18', '17');
INSERT INTO `ref_scale` VALUES ('subtest7', '19', '17');
INSERT INTO `ref_scale` VALUES ('subtest7', '20', '17');
INSERT INTO `ref_scale` VALUES ('subtest7', '21', '17');
INSERT INTO `ref_scale` VALUES ('subtest7', '22', '17');
INSERT INTO `ref_scale` VALUES ('subtest7', '23', '17');
INSERT INTO `ref_scale` VALUES ('subtest7', '24', '17');
INSERT INTO `ref_scale` VALUES ('subtest7', '25', '17');

-- ----------------------------
-- Table structure for `ref_uraian`
-- ----------------------------
DROP TABLE IF EXISTS `ref_uraian`;
CREATE TABLE `ref_uraian` (
  `competency_set_id` varchar(255) NOT NULL,
  `competency` varchar(255) NOT NULL,
  `sub_competency` varchar(255) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `uraian` text,
  `seed` int(11) NOT NULL,
  PRIMARY KEY (`competency_set_id`,`competency`,`sub_competency`,`kode`,`seed`),
  CONSTRAINT `fk_ref_uraian_ref_competency_1` FOREIGN KEY (`competency_set_id`) REFERENCES `ref_competency` (`competency_set`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_uraian
-- ----------------------------
INSERT INTO `ref_uraian` VALUES ('1', 'integritas', 'knowledge', '11', 'integritas knowledge social 11', '1');
INSERT INTO `ref_uraian` VALUES ('1', 'integritas', 'saran', 'A', 'Manajemen dapat mendorong kepatuhan karyawan dalam menerapkan nilai-nilai integitas khususnya, serta norma-norma sosial yang dibudayakan dalam organisasi, melalui :\r\n• Pelatihan internal “ethical assertiveness” dan “organizational values”;\r\n• Secara terus menerus membudayakan nilai-nilai organisasi untuk menuntun karyawan dalam membangun sistem nilai dalam bekerja;\r\n• Secara konsisten menerapkan “code of conduct” atau “value system” sebagai standar kepatuhan.\r\n• Menerapkan sistem monitoring dan evaluasi terhadap kepatuhan.', '0');
INSERT INTO `ref_uraian` VALUES ('1', 'integritas', 'saran', 'B', 'Atasan dapat memacu kepatuhan bawahan dalam menerapkan nilai-nilai integritas, melalui :\r\n• Melakukan monitoring dan evaluasi secara intensif dan berkelanjutan terhadap penerapan nilai-nilai integritas yang ditunjukkan bawahan;\r\n• Memberikan contoh perilaku serta keteladanan terutama saat menghadapi situasi yang membutuhkan pengambilan sikap yang menunjukkan nilai-nilai integritas;\r\n• Memberi “feed-back” terhadap perilaku menyimpang atau pun yang berpotensi terhadap penyalahgunaan nilai-nilai integritas;\r\n• Apresiatif terhadap sikap dan pilihan tindakan bawahan yang menunjukkan persistensi terhadap nilai-nilai integritas.', '0');
INSERT INTO `ref_uraian` VALUES ('1', 'integritas', 'saran', 'C', 'Karyawan dapat menginternalisasi dan menerapkan nilai-nilai organisasi secara , melalui :\r\n• Menyesuaikan perilaku dengan nilai-nilai organisasi dan menjadikannya sebagai acuan dalam bekerja dan berinteraksi di lingkungan sosial organisasi;\r\n• Menjaga citra diri dan kehormatan pribadi  dalam menjalankan tugas maupun dalam mengembangkan hubungan sosial di lingkungan organisasi.\r\n• Mencoba saling mengingatkan antar sesama karyawan akan perilaku yang berpotensi melanggar etika maupun nilai-nilai integritas. ', '0');
INSERT INTO `ref_uraian` VALUES ('1', 'integritas', 'self-control', '0', 'self control 0', '1');
INSERT INTO `ref_uraian` VALUES ('1', 'integritas', 'self-control', '1A', 'self control 1A', '1');
INSERT INTO `ref_uraian` VALUES ('1', 'integritas', 'social awareness', '1A', 'social awareness 1A', '1');

-- ----------------------------
-- Table structure for `result`
-- ----------------------------
DROP TABLE IF EXISTS `result`;
CREATE TABLE `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `observation_id` bigint(20) DEFAULT NULL,
  `assessor_id` varchar(255) DEFAULT NULL,
  `kode_daerah` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_assessment_asesor_1` (`assessor_id`),
  KEY `fk_result_observation_1` (`observation_id`),
  CONSTRAINT `fk_assessment_asesor_1` FOREIGN KEY (`assessor_id`) REFERENCES `asesor` (`id`),
  CONSTRAINT `fk_result_observation_1` FOREIGN KEY (`observation_id`) REFERENCES `observation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of result
-- ----------------------------

-- ----------------------------
-- Table structure for `result_competency`
-- ----------------------------
DROP TABLE IF EXISTS `result_competency`;
CREATE TABLE `result_competency` (
  `observation_id` bigint(20) NOT NULL,
  `competency_set_id` varchar(255) NOT NULL,
  `competency` varchar(255) NOT NULL,
  `sub_competency` varchar(255) NOT NULL,
  `result` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`observation_id`,`competency_set_id`,`competency`,`sub_competency`),
  KEY `fk_result_competency_ref_competency_1` (`competency_set_id`,`competency`,`sub_competency`),
  CONSTRAINT `fk_result_competency_observation_1` FOREIGN KEY (`observation_id`) REFERENCES `observation` (`id`),
  CONSTRAINT `fk_result_competency_ref_competency_1` FOREIGN KEY (`competency_set_id`, `competency`, `sub_competency`) REFERENCES `ref_competency` (`competency_set`, `competency`, `sub_competency`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of result_competency
-- ----------------------------
