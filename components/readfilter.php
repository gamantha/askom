<?php

/**  Define a Read Filter class implementing PHPExcel_Reader_IReadFilter  */
class ReadFilter implements PHPExcel_Reader_IReadFilter
{
    private $_startRow = 0;
    private $_endRow   = 0;
    private $_columns  = array();

    /**  Get the list of rows and columns to read  */
    public function __construct($startRow, $endRow, $columns) {
        $this->_startRow = $startRow;
        $this->_endRow   = $endRow;
        $this->_columns  = $columns;
    }

    public function readCell($column, $row, $worksheetName = '') {
        //  Only read the rows and columns that were configured
        if ($row >= $this->_startRow && $row <= $this->_endRow) {
            if (in_array($column,$this->_columns)) {
                //  echo 'true<br/>';
                return true;
            }
        }
        //echo 'false<br/>';
        return false;
    }


    public function xrange($start, $end, $limit = 1000) {
    $l = array();
    while ($start !== $end && count($l) < $limit) {
        $l[] = $start;
        $start ++;
    }
    $l[] = $end;
    return $l;
}



}


?>