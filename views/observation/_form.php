<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Observation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="observation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'assessment_id')->textInput() ?>

    <?= $form->field($model, 'competency_set_id')->textInput() ?>

    <?= $form->field($model, 'warteg_depth')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warteg_detail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warteg_finesse')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
