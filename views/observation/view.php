<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\RefCompetency;
use app\models\RefUraian;
use app\models\ResultCompetency;
use app\models\PegawaiCompetency;
use app\models\AsesorCompetency;
use app\models\PsikotesResult;
use app\models\PsikotesSubtestResult;
use app\models\Pegawai;
use app\models\RefGolAsesor;
use app\models\RefSaran;
use app\models\RefScale;
use app\models\Result;
use app\models\ObservationGroup;
use yii\db\Expression;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Observation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Observations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="observation-view">

    <h1>No Observasi : <?= Html::encode($this->title) ?></h1>
<p>
<?php

/*echo '<h1>Assesor ID :  ';
$result = Result::find()->andWhere(['observation_id' => $model->id])->One();
echo $result->assessor_id;
echo ' (';


$gol_asesor = RefGolAsesor::find()->andWhere(['id' => $result->assessor->gol_asesor_id])->One();
echo $gol_asesor->nama_golongan_asesor;
echo  ' - ';
echo $gol_asesor->kategori_golongan;

echo ') </h1>';
*/
$obgroup = ObservationGroup::find()
->andWhere(['observation_id' => $model->id])
->One();

echo '<h1>No Meja :  ';
echo $obgroup->no_meja;
echo '</h1>';

echo '<h1>User ID :  ';
   echo $uid = $model->assessment_id . '_' . $model->id . '_' . $obgroup->no_meja;

echo '</h1>';
echo '<h1>Kode :  ';
$resu = Result::find()
->andWhere(['observation_id' => $model->id])
->One();
   echo $assessor_id = $resu->assessor_id;
 echo '<br/>gol asesor : ' . $resu->assessor->golongan_asesor;
echo '</h1>';

echo '<h1>Golongan pegawai :  ';
$pegwai = Pegawai::find()->andWhere(['nama_pegawai' => $uid])->One();
echo $gol_pegawai_id = $pegwai->golongan_pegawai;
//echo $pegwai->golPegawai->kategori_golongan;

   //echo $model->assessment_id . '_' . $model->id . '_' . $obgroup->no_meja;

echo '</h1>';

 ?>
</p>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
<?php


$competency_set_id = '1';

$attributes_array = [
 //'id',
 'assessment_id',
 'competency_set_id',
 'warteg_depth',
 'warteg_detail',
 'warteg_finesse',
];

$competencies_all = RefCompetency::find()
->andWhere(['competency_set_id' => $competency_set_id])
->andWhere(['linkage' => null])
->orderBy('order')
->asArray()
->All();

$linkages_all = RefCompetency::find()
->andWhere(['competency_set_id' => $competency_set_id])
->andWhere(['not',['linkage' => null]])
->orderBy('order')
//HARUS DI SORT BERDASARKAN ORDER
->asArray()
->All();

$competencies_linkages_all = RefCompetency::find()
->select(['competency'])
->andWhere(['competency_set_id' => $competency_set_id])
->orderBy('order')
->distinct()
->asArray()
->All();

$subcompetencies_linkages_all = RefCompetency::find()
->andWhere(['competency_set_id' => $competency_set_id])
->orderBy('order')
->asArray()
->All();


$query1 = (new \yii\db\Query())
    ->select(['order', 'competency','sub_competency'])
    ->where(['competency_set_id' => $competency_set_id])
    ->from('ref_competency')
    ->orderBy('order')
    ->all();
    //->limit(10);
/*
$query2 = (new \yii\db\Query())
    ->select('id, type, name')
    ->from('result_competency')
    ->limit(10);

$query1->union($query2);
*/




$params =  ArrayHelper::map($competencies_linkages_all, 'sub_competency','order','competency');

$competency_list =  ArrayHelper::getColumn($competencies_linkages_all, 'competency');
$subcompetency_list =  ArrayHelper::getColumn($subcompetencies_linkages_all, 'sub_competency');

//$params = ['1','2','3','4','5'];
/*
echo '<pre>';
//echo implode(',', $competency_list);
print_r($query1);
echo '</pre>';
*/

//$test = 'integritas,semangat berprestasi,kerjasama,inovasi,komunikasi lisan';
/*
$results_all = ResultCompetency::find()
->andWhere(['observation_id' => $model->id])
    //->orderBy([new \yii\db\Expression('FIELD (competency_id, ' . implode(',', $params) . ')')])
//    ->orderBy([new \yii\db\Expression('FIELD (competency, ' . implode(',', $competency_list) . ')')])
->orderBy([new \yii\db\Expression('FIELD (competency, " ' . implode(",",$competency_list).' ")')])
//HARUS DI SORT BERDASARKAN ORDER
->asArray()
->All();


echo '<pre>';
print_r($results_all);
echo '</pre>';
*/

foreach ($query1 as $key => $value)
{
//print_r($value);
echo $value['competency'];
echo ' : ';
echo $value['sub_competency'];
$subcomp_val = ResultCompetency::find()
               ->andWhere(['observation_id' => $model->id])
                ->andWhere(['competency' => $value['competency']])
                 ->andWhere(['sub_competency' => $value['sub_competency']])
                 ->One();
                 echo ' : ';
                 echo $kodeuraian = $subcomp_val->result . $subcomp_val->description;
                 echo ' : ';
                 $subkompetensi = $value['sub_competency'];

                 if(($value['sub_competency'] == 'knowledge') || ($value['sub_competency'] == 'skill'))
                 {

                 // $knowledge_result = 'knowldge&&skill uraian';
                  $knowledge_result = ResultCompetency::find()
                  ->andWhere(['observation_id' => $model->id])
                  ->andWhere(['competency' => $value['competency']])
                  ->andWhere(['sub_competency' => 'knowledge'])
                  ->One();

                  //$skill_result = 'knowldge&&skill uraian';
                  $skill_result = ResultCompetency::find()
                  ->andWhere(['observation_id' => $model->id])
                  ->andWhere(['competency' => $value['competency']])
                  ->andWhere(['sub_competency' => 'skill'])
                  ->One();

                  $kodeuraian = $knowledge_result->result . $skill_result->result;
                  $subkompetensi = 'knowledge&skill';
                 }


                 $uraian = RefUraian::find()
                 ->andWhere(['competency_set_id' => $competency_set_id])
                 ->andWhere(['competency' => $value['competency']])
                 ->andWhere(['sub_competency' => $subkompetensi])
                 ->andWhere(['kode' => $kodeuraian])
                 ->One();

                 echo isset($uraian->uraian) ? $uraian->uraian : ' - ';

                 echo '<br/>';
}

/*

foreach ($results_all as $result_key => $result_value)
{

//print_r($result);
print_r($result_value);
echo '<br/>';

$sub_competency_label = RefCompetency::find()->andWhere(['id' => $result['competency_id']])
                         //->andWhere(['competency_set_id' => $competency_set_id])
                         ->One();
$uraian =
 $attributes =  [
        'label' => $sub_competency_label->competency . ' => ' . $sub_competency_label->sub_competency,
         'value' => $result['result'] . $result['description'] . ' =>',
 ];

 array_push($attributes_array, $attributes);


}
*/
/*
echo '<pre>';

$sigma_array = [];
$isgma_array_2 =[];
$uraian_lki_array = [];
$uraian_kelebihan = [];
$uraian_kekurangan = [];
$gap_array=[];
$ks_array = [];
$LKIs = ResultCompetency::find()
->andWhere(['observation_id' => $model->id])
    ->orderBy([new \yii\db\Expression('FIELD (competency_id, ' . implode(',', $params) . ')')])
    ->All();

foreach ($LKIs as $lki)
{

 //echo $lki->competency->competency . ' : ' .  $lki->result ;
$sigma_array[$lki->competency->competency] = (isset($sigma_array[$lki->competency->competency]) ? $sigma_array[$lki->competency->competency] : 0)+ $lki->result;
$sigma_array_2[$lki->competency->competency] = (isset($sigma_array_2[$lki->competency->competency]) ? $sigma_array_2[$lki->competency->competency] : ' ') . $lki->result;
$kodelki =  $lki->result .  $lki->description;

$uraianquery_all = RefUraian::find()->andWhere(['competency_id' => $lki->competency->id])
         ->andWhere(['kode' => $kodelki])
            //->andWhere(['seed' => '1'])
         ->One();

if (sizeof($uraianquery_all) > 0) {
 $optionsize = sizeof($uraianquery_all);
 $uraianretval = ($model->id % $optionsize) + 1;
} else {
 $uraianretval = 0;
}


$uraianquery = RefUraian::find()
->andWhere(['competency_id' => $lki->competency->id])
         ->andWhere(['kode' => $kodelki])
            ->andWhere(['seed' => $uraianretval])
         ->One();
$querytoadd = isset($uraianquery->uraian) ? $uraianquery->uraian : '';

if ($lki->competency->sub_competency != 'knowledge') {
 if ($lki->competency->sub_competency != 'skill') {
  if ($lki->result > 0)
  {
   $uraian_kelebihan[$lki->competency->competency][$lki->competency->sub_competency] = $querytoadd;
  } else {
    $uraian_kekurangan[$lki->competency->competency][$lki->competency->sub_competency] = $querytoadd;
  }
 }
}

$uraian_lki_array[$lki->competency->competency]  = isset($uraian_lki_array[$lki->competency->competency]) ? $uraian_lki_array[$lki->competency->competency] : '';
$uraian_lki_array[$lki->competency->competency] = $uraian_lki_array[$lki->competency->competency] . $querytoadd;

 if ($lki->competency->sub_competency == 'knowledge') {
  $ks_array[$lki->competency->competency]['knowledge']['id'] = $lki->competency->id;
   $ks_array[$lki->competency->competency]['knowledge']['result'] =  $lki->result;

 }
 if ($lki->competency->sub_competency == 'skill') {
  $ks_array[$lki->competency->competency]['skill']['id'] = $lki->competency->id;
   $ks_array[$lki->competency->competency]['skill']['result'] =  $lki->result;
 }

}


foreach($sigma_array as $sigmakey => $sigmavalue)
{

echo '<h3>' . $sigmakey . ' : ' .  $sigma_array_2[$sigmakey]. ' ( '. $sigmavalue .')</h3>';

$adjustment = AsesorCompetency::find()
->andWhere(['gol_asesor_id' => $gol_asesor->id])
->andWhere(['competency_name' => $sigmakey])
->andWhere(['competency_set_id' => $competency_set_id])->One();

$pecomp = PegawaiCompetency::find()
->andWhere(['gol_pegawai_id' => $gol_pegawai_id])
->andWhere(['competency_name' => $sigmakey])
->andWhere(['competency_set_id' => $competency_set_id])->One();


echo '<br/>minimum : ';
echo $minimum = isset($pecomp->minimum) ? $pecomp->minimum : 0;
//echo '<br/>nilai LKI : ';
//echo
echo '<br/>besaran adjustment menurut golongan asesor : ';
echo $adjustment_value = isset($adjustment->value) ? $adjustment->value : 0;
echo '<br/>nilai LKI stelah di adjust: ';
echo $adjusted_lki = (($sigmavalue + $adjustment_value) > $minimum) ? ($sigmavalue + $adjustment_value) : $minimum;
echo '<br/>target : ';
echo $target = isset($pecomp->target) ? $pecomp->target : 0;
echo '<br/>GAP : ';
echo $gap_array[$sigmakey] = (($target - $adjusted_lki) > 0) ? ($target - $adjusted_lki) : 0;
//$target - $adjusted_lki;

echo '<br/>';
if(isset($ks_array[$sigmakey]['knowledge']['result'] ))
{
$kodeks = $ks_array[$sigmakey]['knowledge']['result'] . $ks_array[$sigmakey]['skill']['result'];
echo 'KS : ' . $kodeks ;
} else {
 echo 'GA ADA';
 print_r($ks_array);
}

echo '<br/>';

echo '<br/>';

}

echo '</pre>';
 echo '<br/>';

echo '<pre>';
echo 'GAP TERBESAR : <br/>';
arsort($gap_array);
print_r($gap_array);
 echo '</pre>';
  echo '<br/>';




$query = ResultCompetency::find()
->andWhere(['observation_id' => $model->id])
    ->orderBy([new \yii\db\Expression('FIELD (competency_id, ' . implode(',', $params) . ')')]);

$dataProvider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 1000,
    ],
    'sort' => [
        'defaultOrder' => [

        ]
    ],
]);
*/
/*
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        ['label' => 'kompetensi',
        'value'=>function($data){
         return $data->competency->competency;
        },
        ],
        ['label' => 'sub kompetensi',
        'value'=>function($data){
    return $data->competency->sub_competency;
        },
        ],
        ['label' => 'result',
        'value'=>function($data){
    return $data->result;
        },
        ],

        ['label' => 'kode uraian',
        'value'=>function($data){
    return $data->description;
        },
        ],


        ['class' => 'yii\grid\ActionColumn',
                  'template'=>'{isi}',
                    'buttons'=>[
                      'isi' => function ($url, $model) {
if ($model->competency->linkage == '0') {
                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, [
                                'title' => Yii::t('yii', 'Isi'),
                        ]);
                       }

                      }
                  ]
                    ],

    ],
]);
*/
    ?>

    <h2>PSIKOTES</h2>
    <br/><br/>
    <?php

$psikotesresult = PsikotesResult::find()->andWhere(['observation_id' => $model->id])->One();
$subtestresults = PsikotesSubtestResult::find()
->andWhere(['psikotes_project_id' => $model->assessment->project_id])
->andWhere(['psikotes_result_id'=>$psikotesresult->id])->All();
//echo $model->assessment->project_id;

$totalcfitbenar = 0;
$subtes_loop_index = 0;
foreach($subtestresults as $subtestresult) {
 if ($subtes_loop_index < 4) {
 $totalcfitbenar =  $totalcfitbenar + $subtestresult->true_count;
}
$subtes_loop_index++;
}
echo 'Total CFIT benar : ' . $totalcfitbenar;
echo '<br/> IQ : ';

$scalediq = RefScale::find()->andWhere(['scale_name' => 'iq'])->andWhere(['unscaled' => $totalcfitbenar])->One();
$closestraw = $totalcfitbenar;
while (!isset($scalediq)) {
$scalediq = RefScale::find()->andWhere(['scale_name' => 'iq'])->andWhere(['unscaled' => $closestraw])->One();
 $closestraw--;
}

echo $scalediq->scaled;



$subtest5_result = PsikotesSubtestResult::find()
->andWhere(['psikotes_project_id' => $model->assessment->project_id])
->andWhere(['psikotes_result_id'=>$psikotesresult->id])
->andWhere(['psikotes_subtest_name'=>'subtest5'])
->One();

echo '<br/> subtest 5 total benar : ' . $subtest5_result->true_count;

$scaledsubtest5 = RefScale::find()->andWhere(['scale_name' => 'subtest5'])->andWhere(['unscaled' => $subtest5_result->true_count])->One();
$closestraw = $subtest5_result->true_count;
while (!isset($scaledsubtest5)) {
 $scaledsubtest5 = RefScale::find()->andWhere(['scale_name' => 'subtest5'])->andWhere(['unscaled' => $closestraw])->One();
 $closestraw--;
}

echo '<br/> subtest 5 scaled score : ' . $scaledsubtest5->scaled;










$subtest6_result = PsikotesSubtestResult::find()
->andWhere(['psikotes_project_id' => $model->assessment->project_id])
->andWhere(['psikotes_result_id'=>$psikotesresult->id])
->andWhere(['psikotes_subtest_name'=>'subtest6'])
->One();

echo '<br/> subtest 6 total benar : ' . $subtest6_result->true_count;

$scaledsubtest6 = RefScale::find()->andWhere(['scale_name' => 'subtest6'])->andWhere(['unscaled' => $subtest6_result->true_count])->One();
$closestraw = $subtest6_result->true_count;
while (!isset($scaledsubtest6)) {
 $scaledsubtest6 = RefScale::find()->andWhere(['scale_name' => 'subtest6'])->andWhere(['unscaled' => $closestraw])->One();
 $closestraw--;
}

echo '<br/> subtest 6 scaled score : ' . $scaledsubtest6->scaled;




$subtest7_result = PsikotesSubtestResult::find()
->andWhere(['psikotes_project_id' => $model->assessment->project_id])
->andWhere(['psikotes_result_id'=>$psikotesresult->id])
->andWhere(['psikotes_subtest_name'=>'subtest7'])
->One();

echo '<br/> subtest 7 total benar : ' . $subtest7_result->true_count;

$scaledsubtest7 = RefScale::find()->andWhere(['scale_name' => 'subtest7'])->andWhere(['unscaled' => $subtest7_result->true_count])->One();
$closestraw = $subtest7_result->true_count;
while (!isset($scaledsubtest6)) {
 $scaledsubtest7 = RefScale::find()->andWhere(['scale_name' => 'subtest7'])->andWhere(['unscaled' => $closestraw])->One();
 $closestraw--;
}

echo '<br/> subtest 7 scaled score : ' . $scaledsubtest7->scaled;




     ?>

</div>
