<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\ObservationGroup;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ObservationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Observations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="observation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Observation'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
         //   ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'assessment_id',
            [
             'label' => 'Assessment',
             'value' => function($data){
               Return $data->assessment->assessment_name;
             },
            ],
           /* [
             'label' => 'ID pegawai',
             'value' => function($data){
              $ObGroup = ObservationGroup::find()->andWhere(['observation_id' => $data->id])->One();

              return $data->assessment_id . '_' . $data->id . '_' . $ObGroup->no_meja;
             },
            ],
            */
            [
             'label' => 'No Meja',
             'value' => function($data){
              $ObGroup = ObservationGroup::find()->andWhere(['observation_id' => $data->id])->One();
              return $ObGroup->no_meja;
              //return $data->id;
             },
            ],

        //    'competency_set_id',
        //    'warteg_depth',
          //  'warteg_detail',
            // 'warteg_finesse',

            ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{view} {print}',
                          'buttons'=>[
                            'process_askom' => function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-glass"></span>', $url, [
                                      'title' => Yii::t('yii', 'Process askom'),
                              ]);
                            },
                            'process_linkage' => function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-heart"></span>', $url, [
                                      'title' => Yii::t('yii', 'Process linkage'),
                              ]);

                            },

                            'process_psikotes' => function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-asterisk"></span>', $url, [
                                      'title' => Yii::t('yii', 'Process psikotes'),
                              ]);

                            },
                            'print' => function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-glass">print</span>', 'http://127.0.0.1:8090/askom/web/pdf/hasil_asesmen.php', [
                                      'title' => Yii::t('yii', 'print'),
                              ]);
                            },

                        ]
                          ],

        ],
    ]); ?>

</div>
