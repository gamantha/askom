<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Assessments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Assessment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'assessment_name',
            'project_id',

            //['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{print}',
                          'buttons'=>[
                            'view' => function($url, $model) {
                             return Html::a('<span class="glyphicon glyphicon-glass">view</span>', $url . '?project_id=' . $_REQUEST['project_id'], [
                                     'title' => Yii::t('yii', 'view'),
                             ]);
                            },
                            'print' => function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-glass">print</span>', $url . '?project_id=' . $_REQUEST['project_id'], [
                                      'title' => Yii::t('yii', 'print'),
                              ]);
                            },

                        ]
                          ],

        ],
    ]); ?>
</div>
