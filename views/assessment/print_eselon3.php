<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Observation;
use app\models\PsikotesResult;
use app\models\Result;
use app\models\Pegawai;
use app\models\Project;
use app\models\ResultCompetency;
use app\models\RefScale;
use app\models\RefUraian;
use app\models\RefDefinisi;
use app\models\RefCompetency;
use app\models\RefProjectConfig;
use app\models\RefGolonganPegawaiAdjustment;
use app\models\PsikotesSubtestResult;




/* @var $this yii\web\View */
/* @var $model app\models\Assessment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Assessments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'assessment_name',
            'project_id',
            [
             'label' => 'Nama',
             'value' => explode('_',$model->assessment_name)[0],
            ],
            [
             'label' => 'Golongan',
             'value' => Pegawai::find()->andWhere(['nama_pegawai' => explode('_',$model->assessment_name)[0]])->One()->golongan_pegawai,
            ],
        ],
    ]) ?>

    <?php

    $observation = Observation::find()->andWhere(['assessment_id' => $model->id])->One();
        $psikotes = PsikotesResult::find()->andWhere(['assessment_id' => $model->id])->One();
                $psikotes_result = PsikotesSubtestResult::find()->andWhere(['psikotes_result_id' => $psikotes->id])->All();
                $result = Result::find()->andWhere(['observation_id' => $observation->id])->One();
                    $wilayah = RefDefinisi::find()->andWhere(['type' => 'kode_daerah'])->andWhere(['key' => $psikotes->kode_daerah])->One()->value;
$project_name = Project::findOne($_REQUEST['project_id'])->project_name;
    echo '<pre>';
    echo '<h2>PSIKOTES ESELON 3</h2>';
    echo '<br/>psikotes id : ' . $psikotes->id;
    echo '<br/>no meja : ' . $psikotes->no_meja;
        echo '<br/>pegawai id : ' . $psikotes->pegawai_id;
       echo '<br/>kode buku : ' . $psikotes->kode_buku;
       echo '<br/>kode daerah : ' . $psikotes->kode_daerah;
       echo '<br/>asesor id : ' . $psikotes->asesor_id;


    echo '<h2>OBSERVATION</h2>';
    echo '<br/>observation id : ' . $observation->id;
        echo '<br/>warteg depth : ' . $observation->warteg_depth;
            echo '<br/>warteg detail : ' . $observation->warteg_detail;
                echo '<br/>warteg finesse : ' . $observation->warteg_finesse;

    echo '</pre>';
        echo '<h1>OBSERVATION RESULT</h1>';
        $project_id = $model->project_id;
        $project_config = RefProjectConfig::find()->andWhere(['project_id' => $project_id])->One();
                $competency_list = RefCompetency::find()->andWhere(['competency_set' => $project_config->competency_set_id])->orderBy(['order' => SORT_ASC])->All();

        $distinct_competency_list = RefCompetency::find()->select(['competency'])->distinct()->andWhere(['competency_set' => $project_config->competency_set_id])->orderBy(['order' => SORT_ASC])->All();
   $competency_count_index = 0;
   $competency_list_array = [];
   $tl_array = [];
   $knowledge_array = [];
   $skill_array = [];
   $sigma_array = [];
        foreach($distinct_competency_list as $distinct_competency_list_key => $distinct_competency_list_value) {
              $uraian_array = [];
              $ks_array =[];
              array_push($competency_list_array, $distinct_competency_list_value->competency);
         $a = 0;
         $sigma = 0;
         echo '<br/>';
         echo '<h2>' . $distinct_competency_list_value->competency . '</h2>';
                 $competency_list = RefCompetency::find()->andWhere(['competency_set' => $project_config->competency_set_id])
                 ->andWhere(['competency' => $distinct_competency_list_value->competency])
                 ->orderBy(['order' => SORT_ASC])->All();


                 foreach($competency_list as $competency_key => $competency_value)
                 {
                  echo '<br/>';

                                  $result_competency = ResultCompetency::find()->andWhere(['observation_id' => $observation->id])->andWhere(['competency_set_id' => $project_config->competency_set_id])
                                  ->andWhere(['competency' => $distinct_competency_list_value->competency])
                                  ->andWhere(['sub_competency' => $competency_value->sub_competency])->One();
                 echo $competency_value->competency . ' : ' . $competency_value->sub_competency . ' : ' .  $result_competency->result . $result_competency->description;
                  $uraian_array[$competency_value->sub_competency] = $result_competency->result . $result_competency->description;

                 if (($competency_value->sub_competency == 'knowledge') ||  ($competency_value->sub_competency == 'skill')) {
                   $ks_array[$competency_value->sub_competency] = $result_competency->result;
                   if ($competency_value->sub_competency == 'knowledge') {
                    $knowledge_array[$distinct_competency_list_value->competency] = $result_competency->result;
                   } else {
                    $skill_array[$distinct_competency_list_value->competency] = $result_competency->result;
                   }
                 } else {
                  $a = $a + $result_competency->result;
                 }
                    $sigma = $sigma + $result_competency->result;

                 }
                  echo '<br/>A = ' . $a;
                  echo '<br/><pre>Kode uraian : ';
                  print_r($uraian_array);
                  $uraian_final ='';
                  $uraian_ks_final = '';
                  foreach($uraian_array as $uraian_array_key => $uraian_array_value) {
              //     echo $uraian_array_key;
                   $uraian = RefUraian::find()
                   ->andWhere(['competency_set_id' => $project_config->competency_set_id])
                   ->andWhere(['competency' => $distinct_competency_list_value->competency])
                     ->andWhere(['sub_competency' => $uraian_array_key])
                     ->andWhere(['kode' => $uraian_array_value])
                     ->andWhere(['seed' => '1'])   //DISINI FUNGSI UNTUK RANDOM
                   ->One();

                   if (isset($uraian)) {
                      $uraian_final = $uraian_final . ' ' . $uraian->uraian;
                   }

                  }

                   echo 'Uraian A : ' . $uraian_final;
                   $uraianks = RefUraian::find()
                   ->andWhere(['competency_set_id' => $project_config->competency_set_id])
                   ->andWhere(['competency' => $distinct_competency_list_value->competency])
                     ->andWhere(['sub_competency' => 'knowledge'])
                     ->andWhere(['kode' => $ks_array['knowledge'] . $ks_array['skill']])
                     ->andWhere(['seed' => '1'])   //DISINI FUNGSI UNTUK RANDOM
                   ->One();

                   if (isset($uraianks)) {
                      $uraian_ks_final = $uraianks->uraian;
                   }

                   echo '<br/>Uraian K-S : ' . $uraian_ks_final;

                  echo '</pre>';
                 echo '<br/>Sigma = ' . $sigma;
                 $sigma_array[$distinct_competency_list_value->competency] = $sigma;
                 echo '<br/>';
                 $refgolonganpegawai = RefGolonganPegawaiAdjustment::find()
                 ->andWhere(['competency_set_id' => $project_config->competency_set_id])
                 ->andWhere(['golongan_pegawai'=> Pegawai::find()->andWhere(['nama_pegawai' => explode('_',$model->assessment_name)[0]])->One()->golongan_pegawai])
                 ->andWhere(['competency' => $distinct_competency_list_value->competency])
                   ->andWhere(['sub_competency' => 'NONE'])
                   ->One();
                   $tl_array[$distinct_competency_list_value->competency] = (isset($refgolonganpegawai->target) ? $refgolonganpegawai->target : 0);
                 echo '<br/>Target sigma = ' .  $tl_array[$distinct_competency_list_value->competency]; ;

                 $gap = (isset($refgolonganpegawai->target) ? $refgolonganpegawai->target - $sigma : 0 - $sigma);
                   echo '<br/>GAP = ' .  $gap;
                echo '<br/>Minimal = ' . (isset($refgolonganpegawai->minimum) ? $refgolonganpegawai->minimum : 0) ;
                echo '<br/>Adjustment = ' . (isset($refgolonganpegawai->adjustment) ? $refgolonganpegawai->adjustment : 0) ;
                echo '<br/>asesor adjustment = ' . $a;
                $competency_count_index++;



        }


    echo '<h1>PSIKOTES RESULT</h1>';
    $cfit_array = ['CFIT 1', 'CFIT 2', 'CFIT 3', 'CFIT 4'];
    $truecount_array=[];
    $total_cfit = 0;
    foreach ($psikotes_result as $psikotes_subtest_result_key => $psikotes_subtest_result_value) {
     echo '<br/><br/>' . $psikotes_subtest_result_value->id . ' : ' . $psikotes_subtest_result_value->psikotes_subtest_name;
     if (in_array($psikotes_subtest_result_value->psikotes_subtest_name, $cfit_array)) {
       $total_cfit = $total_cfit + $psikotes_subtest_result_value->true_count;
     }
          $truecount_array[$psikotes_subtest_result_value->psikotes_subtest_name] = $psikotes_subtest_result_value->true_count;
          echo '<br/>True : ' . $psikotes_subtest_result_value->true_count;
          echo '<br/>False : ' . $psikotes_subtest_result_value->false_count;
          echo '<br/>Blank : ' . $psikotes_subtest_result_value->blank_count;

    }

 echo '<pre>';
 $cfit_iq = RefScale::find()->andWhere(['scale_name' =>  'iq'])->andWhere(['unscaled' => $total_cfit])->One();
 $subtest5_scaled = RefScale::find()->andWhere(['scale_name' =>  'subtest5'])->andWhere(['<=','unscaled',$truecount_array['SUBTEST 5']])->orderBy(['unscaled'=>SORT_DESC])->One();
  $subtest6_scaled = RefScale::find()->andWhere(['scale_name' =>  'subtest6'])->andWhere(['unscaled' => $truecount_array['SUBTEST 6']])->One();
   $subtest7_scaled = RefScale::find()->andWhere(['scale_name' =>  'subtest7'])->andWhere(['unscaled' => $truecount_array['SUBTEST 7']])->One();
               echo '<br/>Total CFIT : ' . $total_cfit;
                             echo '<br/>IQ (MAX 130): ' . $cfit_iq->scaled;
               echo '<br/>SUBTEST 5 : ' . $truecount_array['SUBTEST 5'];
               echo '<br/>SUBTEST 5 scaled : ' . $subtest5_scaled->scaled;
               echo '<br/>SUBTEST 6 : ' . $truecount_array['SUBTEST 6'];
               echo '<br/>SUBTEST 6 scaled : ' . $subtest6_scaled->scaled;
               echo '<br/>SUBTEST 7 : ' . $truecount_array['SUBTEST 7'];
               echo '<br/>SUBTEST 7 scaled : ' . $subtest7_scaled->scaled;
  echo '</pre>';

     ?>

</div>



<?php


$nama           = explode('_',$model->assessment_name)[0];
$nip            = explode('_',$model->assessment_name)[1];
//$struk          = "NON - STRUKTURAL";
//$eselon         = "5 - NON STRUKTURAL";
$struk = Pegawai::find()->andWhere(['nama_pegawai' => explode('_',$model->assessment_name)[0]])->One()->golonganPegawai->tipe_jabatan;
$eselon = Pegawai::find()->andWhere(['nama_pegawai' => explode('_',$model->assessment_name)[0]])->One()->golonganPegawai->rumpun;





$jabatan        = "-- NO DATA --";
$satker         = "-- NO DATA --";
$golongan       = "-- NO DATA --";
$pendidikan     = "-- NO DATA --";
$tglsekarang    = date("d M Y");


//$ttl            = "05 SEPTEMBER 1988";
$ttl = Pegawai::find()->andWhere(['nama_pegawai' => explode('_',$model->assessment_name)[0]])->One()->tanggal_lahir;
//$tgltes         = "16 September 2015";
$tgltes = $psikotes->tanggal_tes;


$ttd            = "Drs. Budiman Sanusi, MPsi.";
$himpsi         = "0111891963";


$tl1            = $tl_array['fleksibilitas berpikir'];
$tl2            = $tl_array['inovasi'];
$tl3            = $tl_array['berpikir analitis'];
$tl4            = $tl_array['berpikir konseptual'];
$tl5            = $tl_array['integritas'];
$tl6            = $tl_array['keuletan'];
$tl7            = $tl_array['inisiatif'];
$tl8            = $tl_array['semangat berprestasi'];
$tl9            = $tl_array['komitmen terhadap organisasi'];
$tl10           = $tl_array['kerjasama'];;
$tl11           = $tl_array['kepemimpinan'];;
$tl12           = $tl_array['berorientasi pada pelayanan'];;
$tl13           = $tl_array['membangun hubungan kerja'];;
$tl14           = $tl_array['pencarian informasi'];;
$tl15           = $tl_array['komunikasi lisan'];;
$tl16           = $tl_array['pengambilan keputusan'];
$tl17           = $tl_array['pengorganisasian'];;
$tl18           = $tl_array['perencanaan'];;
$tl19           = $tl_array['berorientasi pada kualitas'];;
$tl20           = $tl_array['tanggap terhadap pengaruh budaya'];;
$tl21           = $tl_array['empati'];
$tl22           = $tl_array['interaksi sosial'];


// Dummy Variable Actual Level

$al1            = $sigma_array['fleksibilitas berpikir'];
$al2            = $sigma_array['inovasi'];
$al3            = $sigma_array['berpikir analitis'];
$al4            = $sigma_array['berpikir konseptual'];
$al5            = $sigma_array['integritas'];
$al6            = $sigma_array['keuletan'];
$al7            = $sigma_array['inisiatif'];
$al8            = $sigma_array['semangat berprestasi'];
$al9            = $sigma_array['komitmen terhadap organisasi'];
$al10           = $sigma_array['kerjasama'];
$al11           = $sigma_array['kepemimpinan'];
$al12           = $sigma_array['berorientasi pada pelayanan'];
$al13           = $sigma_array['membangun hubungan kerja'];
$al14           = $sigma_array['pencarian informasi'];
$al15           = $sigma_array['komunikasi lisan'];
$al16           = $sigma_array['pengambilan keputusan'];
$al17           = $sigma_array['pengorganisasian'];
$al18           = $sigma_array['perencanaan'];
$al19           = $sigma_array['berorientasi pada kualitas'];
$al20           = $sigma_array['tanggap terhadap pengaruh budaya'];
$al21           = $sigma_array['empati'];
$al22           = $sigma_array['interaksi sosial'];




// Dummy Variable GAPS
$gaps1            = $al1-$tl1;
$gaps2            = $al2-$tl2;
$gaps3            = $al3-$tl3;
$gaps4            = $al4-$tl4;
$gaps5            = $al5-$tl5;
$gaps6            = $al6-$tl6;
$gaps7            = $al7-$tl7;
$gaps8            = $al8-$tl8;
$gaps9            = $al9-$tl9;
$gaps10           = $al10-$tl10;
$gaps11           = $al11-$tl11;
$gaps12           = $al12-$tl12;
$gaps13           = $al13-$tl13;
$gaps14           = $al14-$tl14;
$gaps15           = $al15-$tl15;
$gaps16           = $al16-$tl16;
$gaps17           = $al17-$tl17;
$gaps18           = $al18-$tl18;
$gaps19           = $al19-$tl19;
$gaps20           = $al20-$tl20;
$gaps21           = $al21-$tl21;
$gaps22           = $al22-$tl22;

// Dummy Variable TOTAL
$tltot            = $tl1+$tl2+$tl3+$tl4+$tl5+$tl6+$tl7+$tl8+$tl9+$tl10+$tl11+$tl12+$tl13+$tl14+$tl15+$tl16+$tl17+$tl19+$tl20+$tl21+$tl22;
$altot            = $al1+$al2+$al3+$al4+$al5+$al6+$al7+$al8+$al9+$al10+$al11+$al12+$al13+$al14+$al15+$al16+$al17+$al18+$al19+$al20+$al21+$al22;
$gapstot          = $gaps1+$gaps2+$gaps3+$gaps4+$gaps5+$gaps6+$gaps7+$gaps8+$gaps9+$gaps10+$gaps11+$gaps12+$gaps13+$gaps14+$gaps15+$gaps16+$gaps17+$gaps18+$gaps19+$gaps20+$gaps21+$gaps22;
$jobfit = $altot/$tltot;

$cfit1_scaled = RefScale::find()->andWhere(['scale_name' =>  'cfit'])->andWhere(['<=','unscaled',$truecount_array['CFIT 1']])->orderBy(['unscaled'=>SORT_DESC])->One()->scaled;
$cfit2_scaled = RefScale::find()->andWhere(['scale_name' =>  'cfit'])->andWhere(['<=','unscaled',$truecount_array['CFIT 2']])->orderBy(['unscaled'=>SORT_DESC])->One()->scaled;
$cfit3_scaled =  RefScale::find()->andWhere(['scale_name' =>  'cfit'])->andWhere(['<=','unscaled',$truecount_array['CFIT 3']])->orderBy(['unscaled'=>SORT_DESC])->One()->scaled;
$cfit4_scaled = RefScale::find()->andWhere(['scale_name' =>  'cfit'])->andWhere(['<=','unscaled',$truecount_array['CFIT 4']])->orderBy(['unscaled'=>SORT_DESC])->One()->scaled;

 $analitik =  ($cfit2_scaled
 + $cfit4_scaled
  + $cfit3_scaled +
 $subtest5_scaled->scaled + $subtest6_scaled->scaled)/5;

//0; //2-6
$logika = ($cfit1_scaled + $cfit2_scaled + $cfit3_scaled) / 3;
$konsep = $cfit4_scaled; // 4
$akurasi = $subtest5_scaled->scaled; // 5
$speed = $subtest7_scaled->scaled; // 7
$task_orientation = 0;
$people_orientation = 0;
$flexibility = 0;
$knowledge_total = 0;
$skill_total = 0;
$threshold = 0;
//$potensi_pengembangan_diri = avg($analitik + $logika + $konsep + $akurasi + $speed + $task_orientation + $people_orientation + $flexibility);
$potensi_pengembangan_diri = round(($analitik + $logika + $konsep + $akurasi + $speed + $task_orientation + $people_orientation + $flexibility)/8 , 1);
foreach($knowledge_array as $ks_key => $ks_value) {
 $knowledge_total = $knowledge_total + $ks_value;
}
foreach($skill_array as $ks_key => $ks_value) {
 $skill_total = $knowledge_total + $ks_value;
}

$threshold = $skill_total + $knowledge_total;
$ability = 0;
$warteg = 0;
$kemampuan_belajar = round(($threshold/32 + $ability/32 + $warteg/32), 1);
//$kemampuan_belajar = 44;


$job_fit_key = RefDefinisi::find()->andWhere(['type' => 'job_fit_scale'])->andWhere(['<=','key',$jobfit])->orderBy(['key'=>SORT_DESC])->One()->value;
$keterangan_job_fit = RefDefinisi::find()->andWhere(['type' =>  'job_fit'])->andWhere(['<=','key',$job_fit_key])->One();


$potensi_pengembangan_diri_key = RefDefinisi::find()->andWhere(['type' => 'aspek_potensial_scale'])->andWhere(['<=','key',$potensi_pengembangan_diri])->orderBy(['key'=>SORT_DESC])->One()->value;
$keterangan_potensi_pengembangan_diri = RefDefinisi::find()->andWhere(['type' =>  'aspek_potensial'])->andWhere(['<=','key',$potensi_pengembangan_diri_key])->One();

$kemampuan_belajar_key = RefDefinisi::find()->andWhere(['type' => 'kemampuan_belajar_scale'])->andWhere(['<=','key',$kemampuan_belajar])->orderBy(['key'=>SORT_DESC])->One()->value;
$keterangan_kemampuan_belajar = RefDefinisi::find()->andWhere(['type' =>  'kemampuan_belajar'])->andWhere(['<=','key',$kemampuan_belajar_key])->One();



// DEFINISI KOMPETENSI
$kompetensi_1 = 'fleksibilitas berpikir';
$kompetensi_2 = 'inovasi';
$kompetensi_3 = 'berpikir analitis';
$kompetensi_4 = 'berpikir konseptual';
$kompetensi_5 = 'integritas';
$kompetensi_6 = 'keuletan';
$kompetensi_7 = 'inisiatif';
$kompetensi_8 = 'semangat berprestasi';
$kompetensi_9 = 'komitmen terhadap organisasi';
$kompetensi_10 = 'kerjasama';
$kompetensi_11 = 'kepemimpinan';
$kompetensi_12 = 'berorientasi pada pelayanan';
$kompetensi_13 = 'membangun hubungan kerja';
$kompetensi_14 = 'pencarian informasi';
$kompetensi_15 = 'komunikasi lisan';
$kompetensi_16 = 'pengambilan keputusan';
$kompetensi_17 = 'pengorganisasian';
$kompetensi_18 = 'perencanaan';
$kompetensi_19 = 'berorientasi pada kualitas';
$kompetensi_20 = 'tanggap terhadap pengaruh budaya';
$kompetensi_21 = 'empati';
$kompetensi_22 = 'interaksi sosial';


/*foreach ($competency_list_array as $competency_list_item) {
 $definisi_1 = $competency_list_item;
}
*/
$definisi_1 = RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'fleksibilitas berpikir'])->One()->value;
$definisi_2 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'inovasi'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'inovasi'])->One()->value : '';
$definisi_3 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'berpikir analitis'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'berpikir analitis'])->One()->value : '';
$definisi_4 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'berpikir konseptual'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'berpikir konseptual'])->One()->value : '';
$definisi_5 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'integritas'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'integritas'])->One()->value : '';
$definisi_6 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'keuletan'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'keuletan'])->One()->value : '';
$definisi_7 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'inisiatif'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'inisiatif'])->One()->value : '';
$definisi_8 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'semangat berprestasi'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'semangat berprestasi'])->One()->value : '';
$definisi_9 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'komitmen terhadap organisasi'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'komitmen terhadap organisasi'])->One()->value : '';
$definisi_10 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'kerjasama'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'kerjasama'])->One()->value : '';
$definisi_11 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'kepemimpinan'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'kepemimpinan'])->One()->value : '';
$definisi_12 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'berorientasi pada pelayanan'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'berorientasi pada pelayanan'])->One()->value : '';
$definisi_13 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'membangun hubungan kerja'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'membangun hubungan kerja'])->One()->value : '';
$definisi_14 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'pencarian informasi'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'pencarian informasi'])->One()->value : 'defisini 14';
$definisi_15 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'komunikasi lisan'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'komunikasi lisan'])->One()->value : '';
$definisi_16 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'pengambilan keputusan'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'pengambilan keputusan'])->One()->value : '';
$definisi_17 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'pengorganisasian'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'pengorganisasian'])->One()->value : '';
$definisi_18 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'perencanaan'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'perencanaan'])->One()->value : '';
$definisi_19 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'berorientasi pada kualitas'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'berorientasi pada kualitas'])->One()->value : '';
$definisi_20 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'tanggap terhadap pengaruh budaya'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'tanggap terhadap pengaruh budaya'])->One()->value : '';
$definisi_21 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'empati'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'empati'])->One()->value : '';
$definisi_22 = isset (RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'interaksi sosial'])->One()->value) ? RefDefinisi::find()->andWhere(['type' => 'competency'])->andWhere(['key' => 'interaksi sosial'])->One()->value : '';



$penjelasan_1 = 'Reza Septian Perdana, ST telah dapat menampilkan kemampuan dan karakter dominan cukup sesuai dengan tingkat kemahiran yang dipersyaratkan dalam jabatannya saat ini.';
$penjelasan_2 = '';
$penjelasan_3 = '';
$penjelasan_4 = '';
$rekomendasi = '';

 ?>

<?php
echo '<pre>';
print_r($tl_array);
echo '</pre>';
include("pchart/class/pData.class.php");
include("pchart/class/pDraw.class.php");
include("pchart/class/pImage.class.php");
include("pchart/class/pScatter.class.php");


$myData = new \pData();


//$aspek_potensi_pengembangan_diri = 3;
//$aspek_kemampuan_belajar = 2;
$min_area_x = 40;
$min_area_y = 40;
$max_area_x = 370;
$max_area_y = 370;

$x1 = 3;
$x2 = 6;

$y1 = 3;
$y2 = 6;

$imagesize_x = 400;
$imagesize_y = 400;

$myData->addPoints($potensi_pengembangan_diri,"Potensi Pengembangan Diri");
$myData->addPoints($kemampuan_belajar,"Kemampuan Belajar");

$myData->setAxisName(0,"Aspek Potensi Pengembangan Diri (X)");
$myData->setAxisXY(0,AXIS_X);
$myData->setAxisPosition(0,AXIS_POSITION_BOTTOM);

$myData->setAxisName(1,"Aspek Kemampuan Belajar (Y)");
$myData->setAxisXY(1,AXIS_Y);
$myData->setAxisPosition(1,AXIS_POSITION_LEFT);

$myData->setAbscissa("Potensi Pengembangan Diri");


$myData->setSerieOnAxis("Kemampuan Belajar",1);
 $myData->setSerieOnAxis("Potensi Pengembangan Diri",0);

$myData->setScatterSerie("Potensi Pengembangan Diri","Kemampuan Belajar",0);
$myData->setScatterSerieDescription(0,"This year");
$myData->setScatterSerieColor(0,array("R"=>0,"G"=>0,"B"=>0));
$myPicture = new \pImage($imagesize_x,$imagesize_y,$myData);

$myPicture->Antialias = FALSE;

$myPicture->drawRectangle(0,0,399,399,array("R"=>0,"G"=>0,"B"=>0));

$myPicture->setFontProperties(array("FontName"=>"pchart/fonts/pf_arma_five.ttf","FontSize"=>6));

$myPicture->setGraphArea($min_area_x,$min_area_y,$max_area_x,$max_area_y);






$myScatter = new \pScatter($myPicture,$myData);



$AxisBoundaries = array(0=>array("Min"=>0,"Max"=>9),1=>array("Min"=>0,"Max"=>9));
$scaleSettings = array("Mode"=>SCALE_MODE_MANUAL,"ManualScale"=>$AxisBoundaries,"XMargin"=>0,"YMargin"=>0,"Floating"=>FALSE,"GridR"=>200,"GridG"=>200,"GridB"=>200,"DrawSubTicks"=>TRUE,"CycleBackground"=>TRUE);

$TextSettings = array("R"=>255,"G"=>0,"B"=>0,"Angle"=>00, "FontSize"=>20);
$myPicture->drawText(100,115,"6", $TextSettings);
$myPicture->drawText(200,115,"8", $TextSettings);
$myPicture->drawText(300,115,"9", $TextSettings);
$myPicture->drawText(100,215,"3", $TextSettings);
$myPicture->drawText(200,215,"5", $TextSettings);
$myPicture->drawText(300,215,"7", $TextSettings);
$myPicture->drawText(100,315,"1", $TextSettings);
$myPicture->drawText(200,315,"2", $TextSettings);
$myPicture->drawText(300,315,"4", $TextSettings);

$myScatter->drawScatterScale($scaleSettings);




//$myPicture->drawScale(array("XMargin"=>10,"YMargin"=>10,"Floating"=>TRUE,"DrawSubTicks"=>TRUE));
//$myPicture->drawXThreshold(2,array("Alpha"=>70,"Ticks"=>2,"R"=>0,"G"=>0,"B"=>255));

$myPicture->Antialias = TRUE;
$myScatter->drawScatterPlotChart();


$myScatter->drawScatterThreshold($x1,array("AxisID"=>0,"R"=>0,"G"=>0,"B"=>0,"Ticks"=>10));
$myScatter->drawScatterThreshold($x2,array("AxisID"=>0,"R"=>0,"G"=>0,"B"=>0,"Ticks"=>10));


$myScatter->drawScatterThreshold($y1,array("AxisID"=>1,"R"=>0,"G"=>0,"B"=>0,"Ticks"=>10));
$myScatter->drawScatterThreshold($y2,array("AxisID"=>1,"R"=>0,"G"=>0,"B"=>0,"Ticks"=>10));




/*
$myScatter->drawScatterThresholdArea(0,3,array("AxisID"=>0, "AreaName"=>"Error zone"));
$myScatter->drawScatterThresholdArea(3,6,array("AxisID"=>0, "AreaName"=>"Error zone"));

$myScatter->drawScatterThresholdArea(2,3,array("AxisID"=>1, "AreaName"=>"Error zone"));
$myScatter->drawScatterThresholdArea(2,3,array("AxisID"=>1, "AreaName"=>"Error zone"));
*/

//$myPicture->writeLabel("Probe 1",0);
$myPicture->render($model->id . ".png");
//$pic = $myPicture->autoOutput("pchart/pictures/example.example.drawScatterBestFit.png");


// Example of Image from data stream ('PHP rules')

//$imgdata = base64_decode($pic);

// The '@' character is used to indicate that follows an image data stream and not an image file name
//$pdf->Image('@'.$imgdata);

      ob_end_clean();


// Dummy Variable Graph Bar blue
// Dummy Variable Graph Bar blue
if ($tl1 > 0 ){ $g1a = "#5AD2FA";} else { $g1a = "#E7E6E8";}
if ($tl1 > 1 ){ $g1b = "#5AD2FA";} else { $g1b = "#E7E6E8";}
if ($tl1 > 2 ){ $g1c = "#5AD2FA";} else { $g1c = "#E7E6E8";}
if ($tl1 > 3 ){ $g1d = "#5AD2FA";} else { $g1d = "#E7E6E8";}
if ($tl1 > 4 ){ $g1e = "#5AD2FA";} else { $g1e = "#E7E6E8";}
if ($tl1 > 5 ){ $g1f = "#5AD2FA";} else { $g1f = "#E7E6E8";}
if ($tl2 > 0 ){ $g2a = "#5AD2FA";} else { $g2a = "#E7E6E8";}
if ($tl2 > 1 ){ $g2b = "#5AD2FA";} else { $g2b = "#E7E6E8";}
if ($tl2 > 2 ){ $g2c = "#5AD2FA";} else { $g2c = "#E7E6E8";}
if ($tl2 > 3 ){ $g2d = "#5AD2FA";} else { $g2d = "#E7E6E8";}
if ($tl2 > 4 ){ $g2e = "#5AD2FA";} else { $g2e = "#E7E6E8";}
if ($tl2 > 5 ){ $g2f = "#5AD2FA";} else { $g2f = "#E7E6E8";}
if ($tl3 > 0 ){ $g3a = "#5AD2FA";} else { $g3a = "#E7E6E8";}
if ($tl3 > 1 ){ $g3b = "#5AD2FA";} else { $g3b = "#E7E6E8";}
if ($tl3 > 2 ){ $g3c = "#5AD2FA";} else { $g3c = "#E7E6E8";}
if ($tl3 > 3 ){ $g3d = "#5AD2FA";} else { $g3d = "#E7E6E8";}
if ($tl3 > 4 ){ $g3e = "#5AD2FA";} else { $g3e = "#E7E6E8";}
if ($tl3 > 5 ){ $g3f = "#5AD2FA";} else { $g3f = "#E7E6E8";}
if ($tl4 > 0 ){ $g4a = "#5AD2FA";} else { $g4a = "#E7E6E8";}
if ($tl4 > 1 ){ $g4b = "#5AD2FA";} else { $g4b = "#E7E6E8";}
if ($tl4 > 2 ){ $g4c = "#5AD2FA";} else { $g4c = "#E7E6E8";}
if ($tl4 > 3 ){ $g4d = "#5AD2FA";} else { $g4d = "#E7E6E8";}
if ($tl4 > 4 ){ $g4e = "#5AD2FA";} else { $g4e = "#E7E6E8";}
if ($tl4 > 5 ){ $g4f = "#5AD2FA";} else { $g4f = "#E7E6E8";}
// Dummy Variable Graph Bar red
if ($tl5 > 0 ){ $g5a = "#FC568D";} else { $g5a = "#E7E6E8";}
if ($tl5 > 1 ){ $g5b = "#FC568D";} else { $g5b = "#E7E6E8";}
if ($tl5 > 2 ){ $g5c = "#FC568D";} else { $g5c = "#E7E6E8";}
if ($tl5 > 3 ){ $g5d = "#FC568D";} else { $g5d = "#E7E6E8";}
if ($tl5 > 4 ){ $g5e = "#FC568D";} else { $g5e = "#E7E6E8";}
if ($tl5 > 5 ){ $g5f = "#FC568D";} else { $g5f = "#E7E6E8";}
if ($tl6 > 0 ){ $g6a = "#FC568D";} else { $g6a = "#E7E6E8";}
if ($tl6 > 1 ){ $g6b = "#FC568D";} else { $g6b = "#E7E6E8";}
if ($tl6 > 2 ){ $g6c = "#FC568D";} else { $g6c = "#E7E6E8";}
if ($tl6 > 3 ){ $g6d = "#FC568D";} else { $g6d = "#E7E6E8";}
if ($tl6 > 4 ){ $g6e = "#FC568D";} else { $g6e = "#E7E6E8";}
if ($tl6 > 5 ){ $g6f = "#FC568D";} else { $g6f = "#E7E6E8";}
if ($tl7 > 0 ){ $g7a = "#FC568D";} else { $g7a = "#E7E6E8";}
if ($tl7 > 1 ){ $g7b = "#FC568D";} else { $g7b = "#E7E6E8";}
if ($tl7 > 2 ){ $g7c = "#FC568D";} else { $g7c = "#E7E6E8";}
if ($tl7 > 3 ){ $g7d = "#FC568D";} else { $g7d = "#E7E6E8";}
if ($tl7 > 4 ){ $g7e = "#FC568D";} else { $g7e = "#E7E6E8";}
if ($tl7 > 5 ){ $g7f = "#FC568D";} else { $g7f = "#E7E6E8";}
if ($tl8 > 0 ){ $g8a = "#FC568D";} else { $g8a = "#E7E6E8";}
if ($tl8 > 1 ){ $g8b = "#FC568D";} else { $g8b = "#E7E6E8";}
if ($tl8 > 2 ){ $g8c = "#FC568D";} else { $g8c = "#E7E6E8";}
if ($tl8 > 3 ){ $g8d = "#FC568D";} else { $g8d = "#E7E6E8";}
if ($tl8 > 4 ){ $g8e = "#FC568D";} else { $g8e = "#E7E6E8";}
if ($tl8 > 5 ){ $g8f = "#FC568D";} else { $g8f = "#E7E6E8";}
if ($tl9 > 0 ){ $g9a = "#FC568D";} else { $g9a = "#E7E6E8";}
if ($tl9 > 1 ){ $g9b = "#FC568D";} else { $g9b = "#E7E6E8";}
if ($tl9 > 2 ){ $g9c = "#FC568D";} else { $g9c = "#E7E6E8";}
if ($tl9 > 3 ){ $g9d = "#FC568D";} else { $g9d = "#E7E6E8";}
if ($tl9 > 4 ){ $g9e = "#FC568D";} else { $g9e = "#E7E6E8";}
if ($tl9 > 5 ){ $g9f = "#FC568D";} else { $g9f = "#E7E6E8";}
// Dummy Variable Graph Bar GREEN
if ($tl10 > 0 ){ $g10a = "#3CFA7E";} else { $g10a = "#E7E6E8";}
if ($tl10 > 1 ){ $g10b = "#3CFA7E";} else { $g10b = "#E7E6E8";}
if ($tl10 > 2 ){ $g10c = "#3CFA7E";} else { $g10c = "#E7E6E8";}
if ($tl10 > 3 ){ $g10d = "#3CFA7E";} else { $g10d = "#E7E6E8";}
if ($tl10 > 4 ){ $g10e = "#3CFA7E";} else { $g10e = "#E7E6E8";}
if ($tl10 > 5 ){ $g10f = "#3CFA7E";} else { $g10f = "#E7E6E8";}
if ($tl11 > 0 ){ $g11a = "#3CFA7E";} else { $g11a = "#E7E6E8";}
if ($tl11 > 1 ){ $g11b = "#3CFA7E";} else { $g11b = "#E7E6E8";}
if ($tl11 > 2 ){ $g11c = "#3CFA7E";} else { $g11c = "#E7E6E8";}
if ($tl11 > 3 ){ $g11d = "#3CFA7E";} else { $g11d = "#E7E6E8";}
if ($tl11 > 4 ){ $g11e = "#3CFA7E";} else { $g11e = "#E7E6E8";}
if ($tl11 > 5 ){ $g11f = "#3CFA7E";} else { $g11f = "#E7E6E8";}
// Dummy Variable Graph Bar ORANGE
if ($tl12 > 0 ){ $g12a = "#FFB640";} else { $g12a = "#E7E6E8";}
if ($tl12 > 1 ){ $g12b = "#FFB640";} else { $g12b = "#E7E6E8";}
if ($tl12 > 2 ){ $g12c = "#FFB640";} else { $g12c = "#E7E6E8";}
if ($tl12 > 3 ){ $g12d = "#FFB640";} else { $g12d = "#E7E6E8";}
if ($tl12 > 4 ){ $g12e = "#FFB640";} else { $g12e = "#E7E6E8";}
if ($tl12 > 5 ){ $g12f = "#FFB640";} else { $g12f = "#E7E6E8";}
if ($tl13 > 0 ){ $g13a = "#FFB640";} else { $g13a = "#E7E6E8";}
if ($tl13 > 1 ){ $g13b = "#FFB640";} else { $g13b = "#E7E6E8";}
if ($tl13 > 2 ){ $g13c = "#FFB640";} else { $g13c = "#E7E6E8";}
if ($tl13 > 3 ){ $g13d = "#FFB640";} else { $g13d = "#E7E6E8";}
if ($tl13 > 4 ){ $g13e = "#FFB640";} else { $g13e = "#E7E6E8";}
if ($tl13 > 5 ){ $g13f = "#FFB640";} else { $g13f = "#E7E6E8";}
if ($tl14 > 0 ){ $g14a = "#FFB640";} else { $g14a = "#E7E6E8";}
if ($tl14 > 1 ){ $g14b = "#FFB640";} else { $g14b = "#E7E6E8";}
if ($tl14 > 2 ){ $g14c = "#FFB640";} else { $g14c = "#E7E6E8";}
if ($tl14 > 3 ){ $g14d = "#FFB640";} else { $g14d = "#E7E6E8";}
if ($tl14 > 4 ){ $g14e = "#FFB640";} else { $g14e = "#E7E6E8";}
if ($tl14 > 5 ){ $g14f = "#FFB640";} else { $g14f = "#E7E6E8";}
if ($tl15 > 0 ){ $g15a = "#FFB640";} else { $g15a = "#E7E6E8";}
if ($tl15 > 1 ){ $g15b = "#FFB640";} else { $g15b = "#E7E6E8";}
if ($tl15 > 2 ){ $g15c = "#FFB640";} else { $g15c = "#E7E6E8";}
if ($tl15 > 3 ){ $g15d = "#FFB640";} else { $g15d = "#E7E6E8";}
if ($tl15 > 4 ){ $g15e = "#FFB640";} else { $g15e = "#E7E6E8";}
if ($tl15 > 5 ){ $g15f = "#FFB640";} else { $g15f = "#E7E6E8";}
if ($tl16 > 0 ){ $g16a = "#FFB640";} else { $g16a = "#E7E6E8";}
if ($tl16 > 1 ){ $g16b = "#FFB640";} else { $g16b = "#E7E6E8";}
if ($tl16 > 2 ){ $g16c = "#FFB640";} else { $g16c = "#E7E6E8";}
if ($tl16 > 3 ){ $g16d = "#FFB640";} else { $g16d = "#E7E6E8";}
if ($tl16 > 4 ){ $g16e = "#FFB640";} else { $g16e = "#E7E6E8";}
if ($tl16 > 5 ){ $g16f = "#FFB640";} else { $g16f = "#E7E6E8";}
if ($tl17 > 0 ){ $g17a = "#FFB640";} else { $g17a = "#E7E6E8";}
if ($tl17 > 1 ){ $g17b = "#FFB640";} else { $g17b = "#E7E6E8";}
if ($tl17 > 2 ){ $g17c = "#FFB640";} else { $g17c = "#E7E6E8";}
if ($tl17 > 3 ){ $g17d = "#FFB640";} else { $g17d = "#E7E6E8";}
if ($tl17 > 4 ){ $g17e = "#FFB640";} else { $g17e = "#E7E6E8";}
if ($tl17 > 5 ){ $g17f = "#FFB640";} else { $g17f = "#E7E6E8";}
if ($tl18 > 0 ){ $g18a = "#FFB640";} else { $g18a = "#E7E6E8";}
if ($tl18 > 1 ){ $g18b = "#FFB640";} else { $g18b = "#E7E6E8";}
if ($tl18 > 2 ){ $g18c = "#FFB640";} else { $g18c = "#E7E6E8";}
if ($tl18 > 3 ){ $g18d = "#FFB640";} else { $g18d = "#E7E6E8";}
if ($tl18 > 4 ){ $g18e = "#FFB640";} else { $g18e = "#E7E6E8";}
if ($tl18 > 5 ){ $g18f = "#FFB640";} else { $g18f = "#E7E6E8";}
if ($tl19 > 0 ){ $g19a = "#FFB640";} else { $g19a = "#E7E6E8";}
if ($tl19 > 1 ){ $g19b = "#FFB640";} else { $g19b = "#E7E6E8";}
if ($tl19 > 2 ){ $g19c = "#FFB640";} else { $g19c = "#E7E6E8";}
if ($tl19 > 3 ){ $g19d = "#FFB640";} else { $g19d = "#E7E6E8";}
if ($tl19 > 4 ){ $g19e = "#FFB640";} else { $g19e = "#E7E6E8";}
if ($tl19 > 5 ){ $g19f = "#FFB640";} else { $g19f = "#E7E6E8";}
// Dummy Variable Graph Bar VIOLET
if ($tl20 > 0 ){ $g20a = "#BB6AFC";} else { $g20a = "#E7E6E8";}
if ($tl20 > 1 ){ $g20b = "#BB6AFC";} else { $g20b = "#E7E6E8";}
if ($tl20 > 2 ){ $g20c = "#BB6AFC";} else { $g20c = "#E7E6E8";}
if ($tl20 > 3 ){ $g20d = "#BB6AFC";} else { $g20d = "#E7E6E8";}
if ($tl20 > 4 ){ $g20e = "#BB6AFC";} else { $g20e = "#E7E6E8";}
if ($tl20 > 5 ){ $g20f = "#BB6AFC";} else { $g20f = "#E7E6E8";}
if ($tl21 > 0 ){ $g21a = "#BB6AFC";} else { $g21a = "#E7E6E8";}
if ($tl21 > 1 ){ $g21b = "#BB6AFC";} else { $g21b = "#E7E6E8";}
if ($tl21 > 2 ){ $g21c = "#BB6AFC";} else { $g21c = "#E7E6E8";}
if ($tl21 > 3 ){ $g21d = "#BB6AFC";} else { $g21d = "#E7E6E8";}
if ($tl21 > 4 ){ $g21e = "#BB6AFC";} else { $g21e = "#E7E6E8";}
if ($tl21 > 5 ){ $g21f = "#BB6AFC";} else { $g21f = "#E7E6E8";}
if ($tl22 > 0 ){ $g22a = "#BB6AFC";} else { $g22a = "#E7E6E8";}
if ($tl22 > 1 ){ $g22b = "#BB6AFC";} else { $g22b = "#E7E6E8";}
if ($tl22 > 2 ){ $g22c = "#BB6AFC";} else { $g22c = "#E7E6E8";}
if ($tl22 > 3 ){ $g22d = "#BB6AFC";} else { $g22d = "#E7E6E8";}
if ($tl22 > 4 ){ $g22e = "#BB6AFC";} else { $g22e = "#E7E6E8";}
if ($tl22 > 5 ){ $g22f = "#BB6AFC";} else { $g22f = "#E7E6E8";}
// Red Dot Dot 1
if ($al1 == 1) {$g1aa = '<img src="red-dot-icon.png" width="12px">';} else { $g1aa = "";}
if ($al1 == 2) {$g1ab = '<img src="red-dot-icon.png" width="12px">';} else { $g1ab = "";}
if ($al1 == 3) {$g1ac = '<img src="red-dot-icon.png" width="12px">';} else { $g1ac = "";}
if ($al1 == 4) {$g1ad = '<img src="red-dot-icon.png" width="12px">';} else { $g1ad = "";}
if ($al1 == 5) {$g1ae = '<img src="red-dot-icon.png" width="12px">';} else { $g1ae = "";}
if ($al1 == 6) {$g1af = '<img src="red-dot-icon.png" width="12px">';} else { $g1af = "";}
// Red Dot Dot 2
if ($al2 == 1) {$g2aa = '<img src="red-dot-icon.png" width="12px">';} else { $g2aa = "";}
if ($al2 == 2) {$g2ab = '<img src="red-dot-icon.png" width="12px">';} else { $g2ab = "";}
if ($al2 == 3) {$g2ac = '<img src="red-dot-icon.png" width="12px">';} else { $g2ac = "";}
if ($al2 == 4) {$g2ad = '<img src="red-dot-icon.png" width="12px">';} else { $g2ad = "";}
if ($al2 == 5) {$g2ae = '<img src="red-dot-icon.png" width="12px">';} else { $g2ae = "";}
if ($al2 == 6) {$g2af = '<img src="red-dot-icon.png" width="12px">';} else { $g2af = "";}
// Red Dot Dot 3
if ($al3 == 1) {$g3aa = '<img src="red-dot-icon.png" width="12px">';} else { $g3aa = "";}
if ($al3 == 2) {$g3ab = '<img src="red-dot-icon.png" width="12px">';} else { $g3ab = "";}
if ($al3 == 3) {$g3ac = '<img src="red-dot-icon.png" width="12px">';} else { $g3ac = "";}
if ($al3 == 4) {$g3ad = '<img src="red-dot-icon.png" width="12px">';} else { $g3ad = "";}
if ($al3 == 5) {$g3ae = '<img src="red-dot-icon.png" width="12px">';} else { $g3ae = "";}
if ($al3 == 6) {$g3af = '<img src="red-dot-icon.png" width="12px">';} else { $g3af = "";}
// Red Dot Dot 4
if ($al4 == 1) {$g4aa = '<img src="red-dot-icon.png" width="12px">';} else { $g4aa = "";}
if ($al4 == 2) {$g4ab = '<img src="red-dot-icon.png" width="12px">';} else { $g4ab = "";}
if ($al4 == 3) {$g4ac = '<img src="red-dot-icon.png" width="12px">';} else { $g4ac = "";}
if ($al4 == 4) {$g4ad = '<img src="red-dot-icon.png" width="12px">';} else { $g4ad = "";}
if ($al4 == 5) {$g4ae = '<img src="red-dot-icon.png" width="12px">';} else { $g4ae = "";}
if ($al4 == 6) {$g4af = '<img src="red-dot-icon.png" width="12px">';} else { $g4af = "";}
// Red Dot Dot 5
if ($al5 == 1) {$g5aa = '<img src="red-dot-icon.png" width="12px">';} else { $g5aa = "";}
if ($al5 == 2) {$g5ab = '<img src="red-dot-icon.png" width="12px">';} else { $g5ab = "";}
if ($al5 == 3) {$g5ac = '<img src="red-dot-icon.png" width="12px">';} else { $g5ac = "";}
if ($al5 == 4) {$g5ad = '<img src="red-dot-icon.png" width="12px">';} else { $g5ad = "";}
if ($al5 == 5) {$g5ae = '<img src="red-dot-icon.png" width="12px">';} else { $g5ae = "";}
if ($al5 == 6) {$g5af = '<img src="red-dot-icon.png" width="12px">';} else { $g5af = "";}
// Red Dot Dot 6
if ($al6 == 1) {$g6aa = '<img src="red-dot-icon.png" width="12px">';} else { $g6aa = "";}
if ($al6 == 2) {$g6ab = '<img src="red-dot-icon.png" width="12px">';} else { $g6ab = "";}
if ($al6 == 3) {$g6ac = '<img src="red-dot-icon.png" width="12px">';} else { $g6ac = "";}
if ($al6 == 4) {$g6ad = '<img src="red-dot-icon.png" width="12px">';} else { $g6ad = "";}
if ($al6 == 5) {$g6ae = '<img src="red-dot-icon.png" width="12px">';} else { $g6ae = "";}
if ($al6 == 6) {$g6af = '<img src="red-dot-icon.png" width="12px">';} else { $g6af = "";}
// Red Dot Dot 7
if ($al7 == 1) {$g7aa = '<img src="red-dot-icon.png" width="12px">';} else { $g7aa = "";}
if ($al7 == 2) {$g7ab = '<img src="red-dot-icon.png" width="12px">';} else { $g7ab = "";}
if ($al7 == 3) {$g7ac = '<img src="red-dot-icon.png" width="12px">';} else { $g7ac = "";}
if ($al7 == 4) {$g7ad = '<img src="red-dot-icon.png" width="12px">';} else { $g7ad = "";}
if ($al7 == 5) {$g7ae = '<img src="red-dot-icon.png" width="12px">';} else { $g7ae = "";}
if ($al7 == 6) {$g7af = '<img src="red-dot-icon.png" width="12px">';} else { $g7af = "";}
// Red Dot Dot 8
if ($al8 == 1) {$g8aa = '<img src="red-dot-icon.png" width="12px">';} else { $g8aa = "";}
if ($al8 == 2) {$g8ab = '<img src="red-dot-icon.png" width="12px">';} else { $g8ab = "";}
if ($al8 == 3) {$g8ac = '<img src="red-dot-icon.png" width="12px">';} else { $g8ac = "";}
if ($al8 == 4) {$g8ad = '<img src="red-dot-icon.png" width="12px">';} else { $g8ad = "";}
if ($al8 == 5) {$g8ae = '<img src="red-dot-icon.png" width="12px">';} else { $g8ae = "";}
if ($al8 == 6) {$g8af = '<img src="red-dot-icon.png" width="12px">';} else { $g8af = "";}
// Red Dot Dot 9
if ($al9 == 1) {$g9aa = '<img src="red-dot-icon.png" width="12px">';} else { $g9aa = "";}
if ($al9 == 2) {$g9ab = '<img src="red-dot-icon.png" width="12px">';} else { $g9ab = "";}
if ($al9 == 3) {$g9ac = '<img src="red-dot-icon.png" width="12px">';} else { $g9ac = "";}
if ($al9 == 4) {$g9ad = '<img src="red-dot-icon.png" width="12px">';} else { $g9ad = "";}
if ($al9 == 5) {$g9ae = '<img src="red-dot-icon.png" width="12px">';} else { $g9ae = "";}
if ($al9 == 6) {$g9af = '<img src="red-dot-icon.png" width="12px">';} else { $g9af = "";}
// Red Dot Dot 10
if ($al10 == 1) {$g10aa = '<img src="red-dot-icon.png" width="12px">';} else { $g10aa = "";}
if ($al10 == 2) {$g10ab = '<img src="red-dot-icon.png" width="12px">';} else { $g10ab = "";}
if ($al10 == 3) {$g10ac = '<img src="red-dot-icon.png" width="12px">';} else { $g10ac = "";}
if ($al10 == 4) {$g10ad = '<img src="red-dot-icon.png" width="12px">';} else { $g10ad = "";}
if ($al10 == 5) {$g10ae = '<img src="red-dot-icon.png" width="12px">';} else { $g10ae = "";}
if ($al10 == 6) {$g10af = '<img src="red-dot-icon.png" width="12px">';} else { $g10af = "";}
// Red Dot Dot 11
if ($al11 == 1) {$g11aa = '<img src="red-dot-icon.png" width="12px">';} else { $g11aa = "";}
if ($al11 == 2) {$g11ab = '<img src="red-dot-icon.png" width="12px">';} else { $g11ab = "";}
if ($al11 == 3) {$g11ac = '<img src="red-dot-icon.png" width="12px">';} else { $g11ac = "";}
if ($al11 == 4) {$g11ad = '<img src="red-dot-icon.png" width="12px">';} else { $g11ad = "";}
if ($al11 == 5) {$g11ae = '<img src="red-dot-icon.png" width="12px">';} else { $g11ae = "";}
if ($al11 == 6) {$g11af = '<img src="red-dot-icon.png" width="12px">';} else { $g11af = "";}
// Red Dot Dot 12
if ($al12 == 1) {$g12aa = '<img src="red-dot-icon.png" width="12px">';} else { $g12aa = "";}
if ($al12 == 2) {$g12ab = '<img src="red-dot-icon.png" width="12px">';} else { $g12ab = "";}
if ($al12 == 3) {$g12ac = '<img src="red-dot-icon.png" width="12px">';} else { $g12ac = "";}
if ($al12 == 4) {$g12ad = '<img src="red-dot-icon.png" width="12px">';} else { $g12ad = "";}
if ($al12 == 5) {$g12ae = '<img src="red-dot-icon.png" width="12px">';} else { $g12ae = "";}
if ($al12 == 6) {$g12af = '<img src="red-dot-icon.png" width="12px">';} else { $g12af = "";}
// Red Dot Dot 13
if ($al13 == 1) {$g13aa = '<img src="red-dot-icon.png" width="12px">';} else { $g13aa = "";}
if ($al13 == 2) {$g13ab = '<img src="red-dot-icon.png" width="12px">';} else { $g13ab = "";}
if ($al13 == 3) {$g13ac = '<img src="red-dot-icon.png" width="12px">';} else { $g13ac = "";}
if ($al13 == 4) {$g13ad = '<img src="red-dot-icon.png" width="12px">';} else { $g13ad = "";}
if ($al13 == 5) {$g13ae = '<img src="red-dot-icon.png" width="12px">';} else { $g13ae = "";}
if ($al13 == 6) {$g13af = '<img src="red-dot-icon.png" width="12px">';} else { $g13af = "";}
// Red Dot Dot 14
if ($al14 == 1) {$g14aa = '<img src="red-dot-icon.png" width="12px">';} else { $g14aa = "";}
if ($al14 == 2) {$g14ab = '<img src="red-dot-icon.png" width="12px">';} else { $g14ab = "";}
if ($al14 == 3) {$g14ac = '<img src="red-dot-icon.png" width="12px">';} else { $g14ac = "";}
if ($al14 == 4) {$g14ad = '<img src="red-dot-icon.png" width="12px">';} else { $g14ad = "";}
if ($al14 == 5) {$g14ae = '<img src="red-dot-icon.png" width="12px">';} else { $g14ae = "";}
if ($al14 == 6) {$g14af = '<img src="red-dot-icon.png" width="12px">';} else { $g14af = "";}
// Red Dot Dot 15
if ($al15 == 1) {$g15aa = '<img src="red-dot-icon.png" width="12px">';} else { $g15aa = "";}
if ($al15 == 2) {$g15ab = '<img src="red-dot-icon.png" width="12px">';} else { $g15ab = "";}
if ($al15 == 3) {$g15ac = '<img src="red-dot-icon.png" width="12px">';} else { $g15ac = "";}
if ($al15 == 4) {$g15ad = '<img src="red-dot-icon.png" width="12px">';} else { $g15ad = "";}
if ($al15 == 5) {$g15ae = '<img src="red-dot-icon.png" width="12px">';} else { $g15ae = "";}
if ($al15 == 6) {$g15af = '<img src="red-dot-icon.png" width="12px">';} else { $g15af = "";}
// Red Dot Dot 16
if ($al16 == 1) {$g16aa = '<img src="red-dot-icon.png" width="12px">';} else { $g16aa = "";}
if ($al16 == 2) {$g16ab = '<img src="red-dot-icon.png" width="12px">';} else { $g16ab = "";}
if ($al16 == 3) {$g16ac = '<img src="red-dot-icon.png" width="12px">';} else { $g16ac = "";}
if ($al16 == 4) {$g16ad = '<img src="red-dot-icon.png" width="12px">';} else { $g16ad = "";}
if ($al16 == 5) {$g16ae = '<img src="red-dot-icon.png" width="12px">';} else { $g16ae = "";}
if ($al16 == 6) {$g16af = '<img src="red-dot-icon.png" width="12px">';} else { $g16af = "";}
// Red Dot Dot 17
if ($al17 == 1) {$g17aa = '<img src="red-dot-icon.png" width="12px">';} else { $g17aa = "";}
if ($al17 == 2) {$g17ab = '<img src="red-dot-icon.png" width="12px">';} else { $g17ab = "";}
if ($al17 == 3) {$g17ac = '<img src="red-dot-icon.png" width="12px">';} else { $g17ac = "";}
if ($al17 == 4) {$g17ad = '<img src="red-dot-icon.png" width="12px">';} else { $g17ad = "";}
if ($al17 == 5) {$g17ae = '<img src="red-dot-icon.png" width="12px">';} else { $g17ae = "";}
if ($al17 == 6) {$g17af = '<img src="red-dot-icon.png" width="12px">';} else { $g17af = "";}
// Red Dot Dot 18
if ($al18 == 1) {$g18aa = '<img src="red-dot-icon.png" width="12px">';} else { $g18aa = "";}
if ($al18 == 2) {$g18ab = '<img src="red-dot-icon.png" width="12px">';} else { $g18ab = "";}
if ($al18 == 3) {$g18ac = '<img src="red-dot-icon.png" width="12px">';} else { $g18ac = "";}
if ($al18 == 4) {$g18ad = '<img src="red-dot-icon.png" width="12px">';} else { $g18ad = "";}
if ($al18 == 5) {$g18ae = '<img src="red-dot-icon.png" width="12px">';} else { $g18ae = "";}
if ($al18 == 6) {$g18af = '<img src="red-dot-icon.png" width="12px">';} else { $g18af = "";}
// Red Dot Dot 19
if ($al19 == 1) {$g19aa = '<img src="red-dot-icon.png" width="12px">';} else { $g19aa = "";}
if ($al19 == 2) {$g19ab = '<img src="red-dot-icon.png" width="12px">';} else { $g19ab = "";}
if ($al19 == 3) {$g19ac = '<img src="red-dot-icon.png" width="12px">';} else { $g19ac = "";}
if ($al19 == 4) {$g19ad = '<img src="red-dot-icon.png" width="12px">';} else { $g19ad = "";}
if ($al19 == 5) {$g19ae = '<img src="red-dot-icon.png" width="12px">';} else { $g19ae = "";}
if ($al19 == 6) {$g19af = '<img src="red-dot-icon.png" width="12px">';} else { $g19af = "";}
// Red Dot Dot 20
if ($al20 == 1) {$g20aa = '<img src="red-dot-icon.png" width="12px">';} else { $g20aa = "";}
if ($al20 == 2) {$g20ab = '<img src="red-dot-icon.png" width="12px">';} else { $g20ab = "";}
if ($al20 == 3) {$g20ac = '<img src="red-dot-icon.png" width="12px">';} else { $g20ac = "";}
if ($al20 == 4) {$g20ad = '<img src="red-dot-icon.png" width="12px">';} else { $g20ad = "";}
if ($al20 == 5) {$g20ae = '<img src="red-dot-icon.png" width="12px">';} else { $g20ae = "";}
if ($al20 == 6) {$g20af = '<img src="red-dot-icon.png" width="12px">';} else { $g20af = "";}
// Red Dot Dot 21
if ($al21 == 1) {$g21aa = '<img src="red-dot-icon.png" width="12px">';} else { $g21aa = "";}
if ($al21 == 2) {$g21ab = '<img src="red-dot-icon.png" width="12px">';} else { $g21ab = "";}
if ($al21 == 3) {$g21ac = '<img src="red-dot-icon.png" width="12px">';} else { $g21ac = "";}
if ($al21 == 4) {$g21ad = '<img src="red-dot-icon.png" width="12px">';} else { $g21ad = "";}
if ($al21 == 5) {$g21ae = '<img src="red-dot-icon.png" width="12px">';} else { $g21ae = "";}
if ($al21 == 6) {$g21af = '<img src="red-dot-icon.png" width="12px">';} else { $g21af = "";}
// Red Dot Dot 22
if ($al22 == 1) {$g22aa = '<img src="red-dot-icon.png" width="12px">';} else { $g22aa = "";}
if ($al22 == 2) {$g22ab = '<img src="red-dot-icon.png" width="12px">';} else { $g22ab = "";}
if ($al22 == 3) {$g22ac = '<img src="red-dot-icon.png" width="12px">';} else { $g22ac = "";}
if ($al22 == 4) {$g22ad = '<img src="red-dot-icon.png" width="12px">';} else { $g22ad = "";}
if ($al22 == 5) {$g22ae = '<img src="red-dot-icon.png" width="12px">';} else { $g22ae = "";}
if ($al22 == 6) {$g22af = '<img src="red-dot-icon.png" width="12px">';} else { $g22af = "";}
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information

// set header and footer fonts

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);


$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
   require_once(dirname(__FILE__).'/lang/eng.php');
   $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
// add a page
$pdf->AddPage();


$pdf->SetFont('helvetica', '', 6);

// -----------------------------------------------------------------------------

$tbl = <<<EOD
<div align="center"><img src="bps.jpg" width="150px"><BR>
<H3>$project_name</H3><BR><BR><BR><BR>
<H1><B>LAPORAN <BR>
INDIVIDUAL</B></H1><BR>

<H3>
HASIL ASESMEN KOMPETENSI PEJABAT<BR>
$project_name<BR>
REPUBLIK INDONESIA
</H3><BR><BR><BR>

<H1><B>PROVINSI $wilayah</B></H1><BR><BR>

<H3><B><U>$struk</U></B></H3><BR><BR>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
<BR><BR><BR>

<H2><B>Nama Peserta :<BR></B>
<table>
<tr>
<td width="20%"></td>
<td width="60%">

<table cellspacing="0" cellpadding="1" border="1" align="center">
   <tr>
       <td colspan="2" align="center" height="15px"><B>$nama</B></td>
   </tr>
</table>
</td>
<td width="20%"></td>
</tr>
</table>
<BR>
<H2><B>NIP :<BR></B>
<table>
<tr>
<td width="20%"></td>
<td width="60%">

<table cellspacing="0" cellpadding="1" border="1"  align="center">
   <tr>
       <td colspan="2" align="center" height="15px"><B>$nip</B></td>
   </tr>
</table>
</td>
<td width="20%"></td>
</tr>
</table>

</H2>
<BR><BR><BR><BR><BR>
<H3>
Disampaikan Oleh :
<BR><BR>
<img src="ppsdm.jpg" width="150px">
<BR>

PT PRIMA PERSONA SUMBER DAYA MANDIRI
</H3>

</div>


EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// add a page
$pdf->AddPage();


$pdf->SetFont('helvetica', '', 6);

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
$img_file = 'pchart/examples/pictures/ese3.png';
$pdf->Image($img_file, -10, 49, 225, 205 , '', '', '', false, 500, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();

// -----------------------------------------------------------------------------

$tbl = <<<EOD

<table>
<tr>
<td width="15%" align="left"><p><img src="bps.jpg"></p></td>
<td width="70%" align="center"><H3><B><p>HASIL ASESMEN KOMPETENSI PEJABAT $project_name (BPS)</p></B></H3></td>
<td width="15%" align="right"><p><img src="ppsdm.jpg"></p></td>
</tr>
<table>


<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td colspan="2" align="center" height="15px"><B>IDENTITAS PESERTA</B></td>
   </tr>
   <tr>

      <td>
      <table>
           <tr>
               <td>Eselon</td>
               <td>:</td>
               <td>$eselon</td>
           </tr>
           <tr>
               <td>Wilayah</td>
               <td>:</td>
               <td>$wilayah</td>
           </tr><tr>
               <td>NIP</td>
               <td>:</td>
               <td>$nip</td>
           </tr><tr>
               <td>Nama</td>
               <td>:</td>
               <td>$nama</td>
           </tr><tr>
               <td>Tgl Lahir</td>
               <td>:</td>
               <td>$ttl</td>
           </tr>
      </table>
      </td>

      <td>
           <table>
           <tr>
               <td>Jabatan</td>
               <td>:</td>
               <td>$jabatan</td>
           </tr>
           <tr>
               <td>Sekter</td>
               <td>:</td>
               <td>$satker</td>
           </tr><tr>
               <td>Golongan</td>
               <td>:</td>
               <td>$golongan</td>
           </tr><tr>
               <td>Pendidikan</td>
               <td>:</td>
               <td>$pendidikan</td>
           </tr><tr>
               <td>Tgl Tes</td>
               <td>:</td>
               <td>$tgltes</td>
           </tr>
           </table>
      </td>
   </tr>

</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->SetFont('helvetica', '', 5);

$tbl = <<<EOD
</BR>
<table>
<tr>
<td width="80%" >
<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#9E9E9E" colspan="2" rowspan="2" align="center" width="10%"><B>CLUSTER</B></td>
       <td colspan="2" rowspan="2" align="center" width="19%"><B>COMPETENCY</B></td>
       <td colspan="2" rowspan="2" align="center" width="51%"><B>DEFINISI OPERASIONAL</B></td>
       <td colspan="2" align="center" width="15%"><B>PROFICIENCY</B></td>
       <td colspan="2" rowspan="2" align="center" width="5%"><B>GAPS</B></td>
   </tr>
   <tr>
      <td bgcolor="#9E9E9E" align="center"><B>TARGET LEVEL</B></td>
      <td bgcolor="#E6E6E6" align="center"><B>ACTUAL LEVEL</B></td>
   </tr>

</table>

</td>
<td width="1%"></td>
<td width="19%">

</td>
</tr>
</table>

<table>
<tr>
<td width="80%" ></td>
<td width="1%"></td>
<td width="19%">
<table >
<tr>
       0
       <td align="center">1</td>
       <td align="center">2</td>
       <td align="center">3</td>
       <td align="center">4</td>
       <td align="center">5</td>
       <td align="center">6</td>
   </tr>
</table>
</td>
</tr>
</table>


<table>
<tr>
<td width="80%" >

<table cellspacing="0" cellpadding="1" border="1">

   <tr>
       <td bgcolor="#0AACFC" rowspan="4" align="center" width="10%"><B>Kemampuan Berpikir</B></td>
       <td bgcolor="#CAEAFA" align="center" width="3%"><B>1</B></td>
       <td bgcolor="#CAEAFA" align="left" width="16%"><B>$kompetensi_1</B></td>
       <td bgcolor="#CAEAFA" align="left" width="51%">$definisi_1</td>
       <td bgcolor="#0AACFC" align="center" width="7.5%"><B>$tl1</B></td>
       <td bgcolor="#5AD2FA" align="center" width="7.5%"><B>$al1</B></td>
       <td bgcolor="#CAEAFA" align="center" width="5%"><B>$gaps1</B></td>
   </tr>
   <tr>
       <td  bgcolor="#CAEAFA" align="center" width="3%"><B>2</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="16%"><B>$kompetensi_2</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="51%">$definisi_2</td>
       <td  bgcolor="#0AACFC" align="center" width="7.5%"><B>$tl2</B></td>
       <td  bgcolor="#5AD2FA" align="center" width="7.5%"><B>$al2</B></td>
       <td  bgcolor="#CAEAFA" align="center" width="5%"><B>$gaps2</B></td>
   </tr>
   <tr>
       <td  bgcolor="#CAEAFA" align="center" width="3%"><B>3</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="16%"><B>$kompetensi_3</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="51%">$definisi_3</td>
       <td  bgcolor="#0AACFC" align="center" width="7.5%"><B>$tl3</B></td>
       <td  bgcolor="#5AD2FA" align="center" width="7.5%"><B>$al3</B></td>
       <td  bgcolor="#CAEAFA" align="center" width="5%"><B>$gaps3</B></td>
   </tr>
   <tr>
       <td  bgcolor="#CAEAFA" align="center" width="3%"><B>4</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="16%"><B>$kompetensi_4</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="51%">$definisi_4</td>
       <td  bgcolor="#0AACFC" align="center" width="7.5%"><B>$tl4</B></td>
       <td  bgcolor="#5AD2FA" align="center" width="7.5%"><B>$al4</B></td>
       <td  bgcolor="#CAEAFA" align="center" width="5%"><B>$gaps4</B></td>
   </tr>
</table>

<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#F70A59" rowspan="5" align="center" width="10%"><B>Pengelolaan Diri</B></td>
       <td bgcolor="#FCBDF8" align="center" width="3%"><B>5</B></td>
       <td bgcolor="#FCBDF8" align="left" width="16%"><B>$kompetensi_5</B></td>
       <td bgcolor="#FCBDF8" align="left" width="51%">$definisi_5</td>
       <td bgcolor="#F70A59" align="center" width="7.5%"><B>$tl5</B></td>
       <td bgcolor="#FC568D" align="center" width="7.5%"><B>$al5</B></td>
       <td bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps5</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FCBDF8" align="center" width="3%"><B>6</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="16%"><B>$kompetensi_6</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="51%">$definisi_6</td>
       <td  bgcolor="#F70A59" align="center" width="7.5%"><B>$tl6</B></td>
       <td  bgcolor="#FC568D" align="center" width="7.5%"><B>$al6</B></td>
       <td  bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps6</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FCBDF8" align="center" width="3%"><B>7</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="16%"><B>$kompetensi_7</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="51%">$definisi_7</td>
       <td  bgcolor="#F70A59" align="center" width="7.5%"><B>$tl7</B></td>
       <td  bgcolor="#FC568D" align="center" width="7.5%"><B>$al7</B></td>
       <td  bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps7</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FCBDF8" align="center" width="3%"><B>8</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="16%"><B>$kompetensi_8</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="51%">$definisi_8</td>
       <td  bgcolor="#F70A59" align="center" width="7.5%"><B>$tl8</B></td>
       <td  bgcolor="#FC568D" align="center" width="7.5%"><B>$al8</B></td>
       <td  bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps8</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FCBDF8" align="center" width="3%"><B>9</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="16%"><B>$kompetensi_9</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="51%">$definisi_9</td>
       <td  bgcolor="#F70A59" align="center" width="7.5%"><B>$tl9</B></td>
       <td  bgcolor="#FC568D" align="center" width="7.5%"><B>$al9</B></td>
       <td  bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps9</B></td>
   </tr>
</table>

<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#02DB4E" rowspan="5" align="center" width="10%"><B>Pengelolaan Orang Lain</B></td>
       <td bgcolor="#B9FCA7" align="center" width="3%"><B>10</B></td>
       <td bgcolor="#B9FCA7" align="left" width="16%"><B>$kompetensi_10</B></td>
       <td bgcolor="#B9FCA7" align="left" width="51%">$definisi_10</td>
       <td bgcolor="#02DB4E" align="center" width="7.5%"><B>$tl10</B></td>
       <td bgcolor="#3CFA7E" align="center" width="7.5%"><B>$al10</B></td>
       <td bgcolor="#B9FCA7" align="center" width="5%"><B>$gaps10</B></td>
   </tr>
   <tr>
    <td bgcolor="#B9FCA7" align="center" width="3%"><B>11</B></td>
    <td bgcolor="#B9FCA7" align="left" width="16%"><B>$kompetensi_11</B></td>
    <td bgcolor="#B9FCA7" align="left" width="51%">$definisi_11</td>
    <td bgcolor="#02DB4E" align="center" width="7.5%"><B>$tl11</B></td>
    <td bgcolor="#3CFA7E" align="center" width="7.5%"><B>$al11</B></td>
    <td bgcolor="#B9FCA7" align="center" width="5%"><B>$gaps11</B></td>
   </tr>
</table>

<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#FC7303" rowspan="8" align="center" width="10%"><B>Pengelolaan Tugas</B></td>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>12</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>$kompetensi_12</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">$definisi_12</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl12</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al12</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps12</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>13</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>$kompetensi_13</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">$definisi_13</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl13</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al13</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps13</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>14</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>$kompetensi_14</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">$definisi_14</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl14</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al14</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps14</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>15</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>$kompetensi_15</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">$definisi_15</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl15</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al15</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps15</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>16</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>$kompetensi_16</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">$definisi_16</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl16</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al16</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps16</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>17</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>$kompetensi_17</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">$definisi_17</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl17</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al17</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps17</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>18</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>$kompetensi_18</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">$definisi_18</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl18</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al18</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps18</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>19</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>$kompetensi_19</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">$definisi_19</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl19</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al19</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps19</B></td>
   </tr>

</table>
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td bgcolor="#9717FF" rowspan="4" align="center" width="10%"><B>Pengelolaan Sosial Budaya</B></td>
        <td bgcolor="#E0BDFC" align="center" width="3%"><B>20</B></td>
        <td bgcolor="#E0BDFC" align="left" width="16%"><B>$kompetensi_20</B></td>
        <td bgcolor="#E0BDFC" align="left" width="51%">$definisi_20</td>
        <td bgcolor="#9717FF" align="center" width="7.5%"><B>$tl20</B></td>
        <td bgcolor="#BB6AFC" align="center" width="7.5%"><B>$al20</B></td>
        <td bgcolor="#E0BDFC" align="center" width="5%"><B>$gaps20</B></td>
    </tr>
    <tr>
        <td  bgcolor="#E0BDFC" align="center" width="3%"><B>21</B></td>
        <td  bgcolor="#E0BDFC" align="left" width="16%"><B>$kompetensi_21</B></td>
        <td  bgcolor="#E0BDFC" align="left" width="51%">$definisi_21</td>
        <td  bgcolor="#9717FF" align="center" width="7.5%"><B>$tl21</B></td>
        <td  bgcolor="#BB6AFC" align="center" width="7.5%"><B>$al21</B></td>
        <td  bgcolor="#E0BDFC" align="center" width="5%"><B>$gaps21</B></td>
    </tr>
    <tr>
        <td  bgcolor="#E0BDFC" align="center" width="3%"><B>22</B></td>
        <td  bgcolor="#E0BDFC" align="left" width="16%"><B>$kompetensi_22</B></td>
        <td  bgcolor="#E0BDFC" align="left" width="51%">$definisi_22</td>
        <td  bgcolor="#9717FF" align="center" width="7.5%"><B>$tl22</B></td>
        <td  bgcolor="#BB6AFC" align="center" width="7.5%"><B>$al22</B></td>
        <td  bgcolor="#E0BDFC" align="center" width="5%"><B>$gaps22</B></td>
    </tr>

</table>



</td>
<td width="1%"></td>
<td width="19%">

<table cellspacing="2" cellpadding="0" border="0">
<tr>
    <td bgcolor="$g1a" height="15.5px">$g1aa</td>
    <td bgcolor="$g1b">$g1ab</td>
    <td bgcolor="$g1c">$g1ac</td>
    <td bgcolor="$g1d">$g1ad</td>
    <td bgcolor="$g1e">$g1ae</td>
    <td bgcolor="$g1f">$g1af</td>
</tr>

<tr>
    <td bgcolor="$g2a" height="15.5px">$g2aa</td>
    <td bgcolor="$g2b">$g2ab</td>
    <td bgcolor="$g2c">$g2ac</td>
    <td bgcolor="$g2d">$g2ad</td>
    <td bgcolor="$g2e">$g2ae</td>
    <td bgcolor="$g2f">$g2af</td>
</tr>

<tr>
    <td bgcolor="$g3a" height="15.5px">$g3aa</td>
    <td bgcolor="$g3b">$g3ab</td>
    <td bgcolor="$g3c">$g3ac</td>
    <td bgcolor="$g3d">$g3ad</td>
    <td bgcolor="$g3e">$g3ae</td>
    <td bgcolor="$g3f">$g3af</td>
</tr>

<tr>
    <td bgcolor="$g4a" height="15.5px">$g4aa</td>
    <td bgcolor="$g4b">$g4ab</td>
    <td bgcolor="$g4c">$g4ac</td>
    <td bgcolor="$g4d">$g4ad</td>
    <td bgcolor="$g4e">$g4ae</td>
    <td bgcolor="$g4f">$g4af</td>
</tr>

<tr>
    <td bgcolor="$g5a" height="15.5px">$g5aa</td>
    <td bgcolor="$g5b">$g5ab</td>
    <td bgcolor="$g5c">$g5ac</td>
    <td bgcolor="$g5d">$g5ad</td>
    <td bgcolor="$g5e">$g5ae</td>
    <td bgcolor="$g5f">$g5af</td>
</tr>

<tr>
    <td bgcolor="$g6a" height="15.5px">$g6aa</td>
    <td bgcolor="$g6b">$g6ab</td>
    <td bgcolor="$g6c">$g6ac</td>
    <td bgcolor="$g6d">$g6ad</td>
    <td bgcolor="$g6e">$g6ae</td>
    <td bgcolor="$g6f">$g6af</td>
</tr>

<tr>
    <td bgcolor="$g7a" height="15.5px">$g7aa</td>
    <td bgcolor="$g7b">$g7ab</td>
    <td bgcolor="$g7c">$g7ac</td>
    <td bgcolor="$g7d">$g7ad</td>
    <td bgcolor="$g7e">$g7ae</td>
    <td bgcolor="$g7f">$g7af</td>
</tr>

<tr>
    <td bgcolor="$g8a" height="15.5px">$g8aa</td>
    <td bgcolor="$g8b">$g8ab</td>
    <td bgcolor="$g8c">$g8ac</td>
    <td bgcolor="$g8d">$g8ad</td>
    <td bgcolor="$g8e">$g8ae</td>
    <td bgcolor="$g8f">$g8af</td>
</tr>

<tr>
    <td bgcolor="$g9a" height="15.5px">$g9aa</td>
    <td bgcolor="$g9b">$g9ab</td>
    <td bgcolor="$g9c">$g9ac</td>
    <td bgcolor="$g9d">$g9ad</td>
    <td bgcolor="$g9e">$g9ae</td>
    <td bgcolor="$g9f">$g9af</td>
</tr>

<tr>
    <td bgcolor="$g10a" height="15.5px">$g10aa</td>
    <td bgcolor="$g10b">$g10ab</td>
    <td bgcolor="$g10c">$g10ac</td>
    <td bgcolor="$g10d">$g10ad</td>
    <td bgcolor="$g10e">$g10ae</td>
    <td bgcolor="$g10f">$g10af</td>
</tr>

<tr>
    <td bgcolor="$g11a" height="15.5px">$g11aa</td>
    <td bgcolor="$g11b">$g11ab</td>
    <td bgcolor="$g11c">$g11ac</td>
    <td bgcolor="$g11d">$g11ad</td>
    <td bgcolor="$g11e">$g11ae</td>
    <td bgcolor="$g11f">$g11af</td>
</tr>

<tr>
    <td bgcolor="$g12a" height="15.5px">$g12aa</td>
    <td bgcolor="$g12b">$g12ab</td>
    <td bgcolor="$g12c">$g12ac</td>
    <td bgcolor="$g12d">$g12ad</td>
    <td bgcolor="$g12e">$g12ae</td>
    <td bgcolor="$g12f">$g12af</td>
</tr>

<tr>
    <td bgcolor="$g13a" height="15.5px">$g13aa</td>
    <td bgcolor="$g13b">$g13ab</td>
    <td bgcolor="$g13c">$g13ac</td>
    <td bgcolor="$g13d">$g13ad</td>
    <td bgcolor="$g13e">$g13ae</td>
    <td bgcolor="$g13f">$g13af</td>
</tr>

<tr>
    <td bgcolor="$g14a" height="15.5px">$g14aa</td>
    <td bgcolor="$g14b">$g14ab</td>
    <td bgcolor="$g14c">$g14ac</td>
    <td bgcolor="$g14d">$g14ad</td>
    <td bgcolor="$g14e">$g14ae</td>
    <td bgcolor="$g14f">$g14af</td>
</tr>

<tr>
    <td bgcolor="$g15a" height="15.5px">$g15aa</td>
    <td bgcolor="$g15b">$g15ab</td>
    <td bgcolor="$g15c">$g15ac</td>
    <td bgcolor="$g15d">$g15ad</td>
    <td bgcolor="$g15e">$g15ae</td>
    <td bgcolor="$g15f">$g15af</td>
</tr>

<tr>
    <td bgcolor="$g16a" height="15.5px">$g16aa</td>
    <td bgcolor="$g16b">$g16ab</td>
    <td bgcolor="$g16c">$g16ac</td>
    <td bgcolor="$g16d">$g16ad</td>
    <td bgcolor="$g16e">$g16ae</td>
    <td bgcolor="$g16f">$g16af</td>
</tr>
   <tr>
       <td bgcolor="$g17a" height="15.5px">$g17aa</td>
       <td bgcolor="$g17b">$g17ab</td>
       <td bgcolor="$g17c">$g17ac</td>
       <td bgcolor="$g17d">$g17ad</td>
       <td bgcolor="$g17e">$g17ae</td>
       <td bgcolor="$g17f">$g17af</td>
   </tr>

   <tr>
       <td bgcolor="$g18a" height="15.5px">$g18aa</td>
       <td bgcolor="$g18b">$g18ab</td>
       <td bgcolor="$g18c">$g18ac</td>
       <td bgcolor="$g18d">$g18ad</td>
       <td bgcolor="$g18e">$g18ae</td>
       <td bgcolor="$g18f">$g18af</td>
   </tr>

   <tr>
       <td bgcolor="$g19a" height="15.5px">$g19aa</td>
       <td bgcolor="$g19b">$g19ab</td>
       <td bgcolor="$g19c">$g19ac</td>
       <td bgcolor="$g19d">$g19ad</td>
       <td bgcolor="$g19e">$g19ae</td>
       <td bgcolor="$g19f">$g19af</td>
   </tr>

   <tr>
       <td bgcolor="$g20a" height="15.5px">$g20aa</td>
       <td bgcolor="$g20b">$g20ab</td>
       <td bgcolor="$g20c">$g20ac</td>
       <td bgcolor="$g20d">$g20ad</td>
       <td bgcolor="$g20e">$g20ae</td>
       <td bgcolor="$g20f">$g20af</td>
   </tr>

   <tr>
       <td bgcolor="$g21a" height="15.5px">$g21aa</td>
       <td bgcolor="$g21b">$g21ab</td>
       <td bgcolor="$g21c">$g21ac</td>
       <td bgcolor="$g21d">$g21ad</td>
       <td bgcolor="$g21e">$g21ae</td>
       <td bgcolor="$g21f">$g21af</td>
   </tr>

   <tr>
       <td bgcolor="$g22a" height="15.5px">$g22aa</td>
       <td bgcolor="$g22b">$g22ab</td>
       <td bgcolor="$g22c">$g22ac</td>
       <td bgcolor="$g22d">$g22ad</td>
       <td bgcolor="$g22e">$g22ae</td>
       <td bgcolor="$g22f">$g22af</td>
   </tr>
</table>

</td>
</tr>
</table>
<table>
<tr>
<td width="80%" ></td>
<td width="1%"></td>
<td width="19%">
<table>
<tr>
       0
       <td align="center">1</td>
       <td align="center">2</td>
       <td align="center">3</td>
       <td align="center">4</td>
       <td align="center">5</td>
       <td align="center">6</td>
   </tr>
</table>
</td>
</tr>
</table>


<table cellspacing="0" cellpadding="1" border="1" width="80%">
   <tr>
       <td align="center" width="80%"><h3><B>TOTAL</B></h3></td>
       <td bgcolor="#9E9E9E" align="center" width="7.5%"><B>$tltot</B></td>
       <td bgcolor="#E6E6E6" align="center" width="7.5%"><B>$altot</B></td>
       <td align="center" width="5%"><B>$gapstot</B></td>
   </tr>

</table>

EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->SetFont('helvetica', '', 6);

// -----------------------------------------------------------------------------

$tbl = <<<EOD

<table cellspacing="0" cellpadding="0" border="0">
   <tr>
       <td colspan="3" align="center" height="15px"><B></B></td>
   </tr>
   <tr>
      <td width="49%">

      <table border="1">
           <tr>
               <td bgcolor="#5AD2FA" colspan="2" align="center" width="65%"><B>INDEX</B></td>
               <td bgcolor="#5AD2FA" align="center" width="35%"><B>KETERANGAN</B></td>
           </tr>
           <tr>
               <td width="50%">JOB FIT</td>
               <td align="center" width="15%">$jobfit</td>
               <td width="35%">$keterangan_job_fit->key</td>
           </tr>
           <tr>
               <td>POTENSI PENGEMBANGAN DIRI</td>
               <td align="center">$potensi_pengembangan_diri</td>
               <td>$keterangan_potensi_pengembangan_diri->key</td>
           </tr>
           <tr>
               <td>KEMAMPUAN BELAJAR</td>
               <td align="center">$kemampuan_belajar</td>
               <td>$keterangan_kemampuan_belajar->key</td>
           </tr>
      </table>

      <br><br>

      <table border="1">
           <tr>
               <td bgcolor="#5AD2FA" align="right"><B>PENJELASAN</B></td>

           </tr>
           <tr>
               <td>
               <p>$keterangan_job_fit->value</p>
               </td>

           </tr>
           <tr>
               <td>
               <p>$keterangan_potensi_pengembangan_diri->value</p>
               </td>

           </tr>
           <tr>
               <td>
               <p>$keterangan_kemampuan_belajar->value</p>
               </td>

           </tr>
           <tr>
               <td>
               <p>$penjelasan_4</p>
               </td>

           </tr>
           <tr>
               <td bgcolor="#5AD2FA" align="right"><B>REKOMENDASI</B></td>

           </tr>
           <tr>
               <td>
               <BR><p>$rekomendasi</p><BR>
               </td>

           </tr>
      </table>

      </td>

      <td width="2%"></td>

      <td width="49%">

           <table border="1">
           <tr>
                <td><img src="$model->id.png" width="290px"/></td>
           </tr>
          </table>
      </td>

   </tr>

</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// add a page
$pdf->AddPage();


$pdf->SetFont('helvetica', '', 6);

// -----------------------------------------------------------------------------

$tbl = <<<EOD

<table>
<tr>
<td width="15%" align="left"><p><img src="bps.jpg"></p></td>
<td width="70%" align="center"><H3><B><p>HASIL ASESMEN KOMPETENSI PEJABAT BADAN PUSAT STATISTIK (BPS)</p></B></H3></td>
<td width="15%" align="right"><p><img src="ppsdm.jpg"></p></td>
</tr>
<table>


<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td colspan="2" align="center" height="15px"><B>IDENTITAS PESERTA</B></td>
   </tr>
   <tr>

      <td>

      <table>
           <tr>
               <td>Eselon</td>
               <td>:</td>
               <td>$eselon</td>
           </tr>
           <tr>
               <td>Wilayah</td>
               <td>:</td>
               <td>$wilayah</td>
           </tr><tr>
               <td>NIP</td>
               <td>:</td>
               <td>$nip</td>
           </tr><tr>
               <td>Nama</td>
               <td>:</td>
               <td>$nama</td>
           </tr><tr>
               <td>Tgl Lahir</td>
               <td>:</td>
               <td>$ttl</td>
           </tr>
      </table>
      </td>

      <td>
           <table>
           <tr>
               <td>Jabatan</td>
               <td>:</td>
               <td>$jabatan</td>
           </tr>
           <tr>
               <td>Sakter</td>
               <td>:</td>
               <td>$satker</td>
           </tr><tr>
               <td>Golongan</td>
               <td>:</td>
               <td>$golongan</td>
           </tr><tr>
               <td>Pendidikan</td>
               <td>:</td>
               <td>$pendidikan</td>
           </tr><tr>
               <td>Tgl Tes</td>
               <td>:</td>
               <td>$tgltes</td>
           </tr>
           </table>
      </td>
   </tr>

</table>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
<p><img src=""></p>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
<div align="right">
Jakarta, $tglsekarang<BR><BR><BR><BR><BR><BR><BR>
<U>($ttd)</U><BR>
No. HIMPSI: $himpsi
</div>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');









// -----------------------------------------------------------------------------

//Close and output PDF document

$pdf->Output($project_id . '_' . $model->assessment_name . '.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
