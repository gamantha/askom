<?php
//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
* Creates an example PDF TEST document using TCPDF
* @package com.tecnick.tcpdf
* @abstract TCPDF - Example: HTML tables and table headers
* @author Nicola Asuni
* @since 2009-03-20
*/

// Include the main TCPDF library (search for installation path).
ob_start();
//require_once('../components/pdf/tcpdf_include.php');
// install Composer autoloader
//require_once('../vendor/pdf/tcpdf_include.php');
//require_once('../vendor/pdf/tcpdf/tcpdf.php');
//require('../vendor/autoload.php');
//require('../vendor/yiisoft/yii2/Yii.php');
// include Yii class file
//require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

//use yiiTCPDF;

// Dummy Variable Data Diri
$nama           = "Reza Septian Pradana, SST";
$nip            = "12323123123123123";
$struk          = "NON - STRUKTURAL";
$eselon         = "5 - NON STRUKTURAL";
$wilayah        = "PROV. ACEH";
$ttl            = "05 SEPTEMBER 1988";
$jabatan        = "Staistisi Pelaksana Lanjutan";
$satker         = "BPS Kab. Aceh Jaya";
$golongan       = "III/A";
$pendidikan     = "D IV STIS Ekonomi";
$tgltes         = "16 September 2015";
$tglsekarang    = "20 November 2015";
$ttd            = "Drs. Budiman Sanusi, MPsi.";
$himpsi         = "0111891963";

// Dummy Variable Target Level
$tl1            = "3";
$tl2            = "2";
$tl3            = "3";
$tl4            = "5";
$tl5            = "4";
$tl6            = "2";
$tl7            = "2";
$tl8            = "2";
$tl9            = "5";
$tl10           = "3";
$tl11           = "2";
$tl12           = "2";
$tl13           = "3";
$tl14           = "3";
$tl15           = "2";
$tl16           = "4";

// Dummy Variable Actual Level
$al1            = "2";
$al2            = "3";
$al3            = "2";
$al4            = "2";
$al5            = "3";
$al6            = "4";
$al7            = "3";
$al8            = "2";
$al9            = "4";
$al10           = "2";
$al11           = "2";
$al12           = "2";
$al13           = "3";
$al14           = "3";
$al15           = "3";
$al16           = "3";

// Dummy Variable GAPS
$gaps1            = $al1-$tl1;
$gaps2            = $al2-$tl2;
$gaps3            = $al3-$tl3;
$gaps4            = $al4-$tl4;
$gaps5            = $al5-$tl5;
$gaps6            = $al6-$tl6;
$gaps7            = $al7-$tl7;
$gaps8            = $al8-$tl8;
$gaps9            = $al9-$tl9;
$gaps10           = $al10-$tl10;
$gaps11           = $al11-$tl11;
$gaps12           = $al12-$tl12;
$gaps13           = $al13-$tl13;
$gaps14           = $al14-$tl14;
$gaps15           = $al15-$tl15;
$gaps16           = $al16-$tl16;

// Dummy Variable TOTAL
$tltot            = $tl1+$tl2+$tl3+$tl4+$tl5+$tl6+$tl7+$tl8+$tl9+$tl10+$tl11+$tl12+$tl13+$tl14+$tl15+$tl16;
$altot            = $al1+$al2+$al3+$al4+$al5+$al6+$al7+$al8+$al9+$al10+$al11+$al12+$al13+$al14+$al15+$al16;
$gapstot          = $gaps1+$gaps2+$gaps3+$gaps4+$gaps5+$gaps6+$gaps7+$gaps8+$gaps9+$gaps10+$gaps11+$gaps12+$gaps13+$gaps14+$gaps15+$gaps16;

// Dummy Variable Graph Bar blue
if ($tl1 > 0 ){ $g1a = "#5AD2FA";} else { $g1a = "#E7E6E8";}
if ($tl1 > 1 ){ $g1b = "#5AD2FA";} else { $g1b = "#E7E6E8";}
if ($tl1 > 2 ){ $g1c = "#5AD2FA";} else { $g1c = "#E7E6E8";}
if ($tl1 > 3 ){ $g1d = "#5AD2FA";} else { $g1d = "#E7E6E8";}
if ($tl1 > 4 ){ $g1e = "#5AD2FA";} else { $g1e = "#E7E6E8";}
if ($tl1 > 5 ){ $g1f = "#5AD2FA";} else { $g1f = "#E7E6E8";}
if ($tl2 > 0 ){ $g2a = "#5AD2FA";} else { $g2a = "#E7E6E8";}
if ($tl2 > 1 ){ $g2b = "#5AD2FA";} else { $g2b = "#E7E6E8";}
if ($tl2 > 2 ){ $g2c = "#5AD2FA";} else { $g2c = "#E7E6E8";}
if ($tl2 > 3 ){ $g2d = "#5AD2FA";} else { $g2d = "#E7E6E8";}
if ($tl2 > 4 ){ $g2e = "#5AD2FA";} else { $g2e = "#E7E6E8";}
if ($tl2 > 5 ){ $g2f = "#5AD2FA";} else { $g2f = "#E7E6E8";}
if ($tl3 > 0 ){ $g3a = "#5AD2FA";} else { $g3a = "#E7E6E8";}
if ($tl3 > 1 ){ $g3b = "#5AD2FA";} else { $g3b = "#E7E6E8";}
if ($tl3 > 2 ){ $g3c = "#5AD2FA";} else { $g3c = "#E7E6E8";}
if ($tl3 > 3 ){ $g3d = "#5AD2FA";} else { $g3d = "#E7E6E8";}
if ($tl3 > 4 ){ $g3e = "#5AD2FA";} else { $g3e = "#E7E6E8";}
if ($tl3 > 5 ){ $g3f = "#5AD2FA";} else { $g3f = "#E7E6E8";}
if ($tl4 > 0 ){ $g4a = "#5AD2FA";} else { $g4a = "#E7E6E8";}
if ($tl4 > 1 ){ $g4b = "#5AD2FA";} else { $g4b = "#E7E6E8";}
if ($tl4 > 2 ){ $g4c = "#5AD2FA";} else { $g4c = "#E7E6E8";}
if ($tl4 > 3 ){ $g4d = "#5AD2FA";} else { $g4d = "#E7E6E8";}
if ($tl4 > 4 ){ $g4e = "#5AD2FA";} else { $g4e = "#E7E6E8";}
if ($tl4 > 5 ){ $g4f = "#5AD2FA";} else { $g4f = "#E7E6E8";}
// Dummy Variable Graph Bar blue
if ($tl5 > 0 ){ $g5a = "#FC568D";} else { $g5a = "#E7E6E8";}
if ($tl5 > 1 ){ $g5b = "#FC568D";} else { $g5b = "#E7E6E8";}
if ($tl5 > 2 ){ $g5c = "#FC568D";} else { $g5c = "#E7E6E8";}
if ($tl5 > 3 ){ $g5d = "#FC568D";} else { $g5d = "#E7E6E8";}
if ($tl5 > 4 ){ $g5e = "#FC568D";} else { $g5e = "#E7E6E8";}
if ($tl5 > 5 ){ $g5f = "#FC568D";} else { $g5f = "#E7E6E8";}
if ($tl6 > 0 ){ $g6a = "#FC568D";} else { $g6a = "#E7E6E8";}
if ($tl6 > 1 ){ $g6b = "#FC568D";} else { $g6b = "#E7E6E8";}
if ($tl6 > 2 ){ $g6c = "#FC568D";} else { $g6c = "#E7E6E8";}
if ($tl6 > 3 ){ $g6d = "#FC568D";} else { $g6d = "#E7E6E8";}
if ($tl6 > 4 ){ $g6e = "#FC568D";} else { $g6e = "#E7E6E8";}
if ($tl6 > 5 ){ $g6f = "#FC568D";} else { $g6f = "#E7E6E8";}
if ($tl7 > 0 ){ $g7a = "#FC568D";} else { $g7a = "#E7E6E8";}
if ($tl7 > 1 ){ $g7b = "#FC568D";} else { $g7b = "#E7E6E8";}
if ($tl7 > 2 ){ $g7c = "#FC568D";} else { $g7c = "#E7E6E8";}
if ($tl7 > 3 ){ $g7d = "#FC568D";} else { $g7d = "#E7E6E8";}
if ($tl7 > 4 ){ $g7e = "#FC568D";} else { $g7e = "#E7E6E8";}
if ($tl7 > 5 ){ $g7f = "#FC568D";} else { $g7f = "#E7E6E8";}
if ($tl8 > 0 ){ $g8a = "#FC568D";} else { $g8a = "#E7E6E8";}
if ($tl8 > 1 ){ $g8b = "#FC568D";} else { $g8b = "#E7E6E8";}
if ($tl8 > 2 ){ $g8c = "#FC568D";} else { $g8c = "#E7E6E8";}
if ($tl8 > 3 ){ $g8d = "#FC568D";} else { $g8d = "#E7E6E8";}
if ($tl8 > 4 ){ $g8e = "#FC568D";} else { $g8e = "#E7E6E8";}
if ($tl8 > 5 ){ $g8f = "#FC568D";} else { $g8f = "#E7E6E8";}
if ($tl9 > 0 ){ $g9a = "#FC568D";} else { $g9a = "#E7E6E8";}
if ($tl9 > 1 ){ $g9b = "#FC568D";} else { $g9b = "#E7E6E8";}
if ($tl9 > 2 ){ $g9c = "#FC568D";} else { $g9c = "#E7E6E8";}
if ($tl9 > 3 ){ $g9d = "#FC568D";} else { $g9d = "#E7E6E8";}
if ($tl9 > 4 ){ $g9e = "#FC568D";} else { $g9e = "#E7E6E8";}
if ($tl9 > 5 ){ $g9f = "#FC568D";} else { $g9f = "#E7E6E8";}
// Dummy Variable Graph Bar GREEN
if ($tl10 > 0 ){ $g10a = "#3CFA7E";} else { $g10a = "#E7E6E8";}
if ($tl10 > 1 ){ $g10b = "#3CFA7E";} else { $g10b = "#E7E6E8";}
if ($tl10 > 2 ){ $g10c = "#3CFA7E";} else { $g10c = "#E7E6E8";}
if ($tl10 > 3 ){ $g10d = "#3CFA7E";} else { $g10d = "#E7E6E8";}
if ($tl10 > 4 ){ $g10e = "#3CFA7E";} else { $g10e = "#E7E6E8";}
if ($tl10 > 5 ){ $g10f = "#3CFA7E";} else { $g10f = "#E7E6E8";}
// Dummy Variable Graph Bar ORANGE
if ($tl11 > 0 ){ $g11a = "#FFB640";} else { $g11a = "#E7E6E8";}
if ($tl11 > 1 ){ $g11b = "#FFB640";} else { $g11b = "#E7E6E8";}
if ($tl11 > 2 ){ $g11c = "#FFB640";} else { $g11c = "#E7E6E8";}
if ($tl11 > 3 ){ $g11d = "#FFB640";} else { $g11d = "#E7E6E8";}
if ($tl11 > 4 ){ $g11e = "#FFB640";} else { $g11e = "#E7E6E8";}
if ($tl11 > 5 ){ $g11f = "#FFB640";} else { $g11f = "#E7E6E8";}
if ($tl12 > 0 ){ $g12a = "#FFB640";} else { $g12a = "#E7E6E8";}
if ($tl12 > 1 ){ $g12b = "#FFB640";} else { $g12b = "#E7E6E8";}
if ($tl12 > 2 ){ $g12c = "#FFB640";} else { $g12c = "#E7E6E8";}
if ($tl12 > 3 ){ $g12d = "#FFB640";} else { $g12d = "#E7E6E8";}
if ($tl12 > 4 ){ $g12e = "#FFB640";} else { $g12e = "#E7E6E8";}
if ($tl12 > 5 ){ $g12f = "#FFB640";} else { $g12f = "#E7E6E8";}
if ($tl13 > 0 ){ $g13a = "#FFB640";} else { $g13a = "#E7E6E8";}
if ($tl13 > 1 ){ $g13b = "#FFB640";} else { $g13b = "#E7E6E8";}
if ($tl13 > 2 ){ $g13c = "#FFB640";} else { $g13c = "#E7E6E8";}
if ($tl13 > 3 ){ $g13d = "#FFB640";} else { $g13d = "#E7E6E8";}
if ($tl13 > 4 ){ $g13e = "#FFB640";} else { $g13e = "#E7E6E8";}
if ($tl13 > 5 ){ $g13f = "#FFB640";} else { $g13f = "#E7E6E8";}
if ($tl14 > 0 ){ $g14a = "#FFB640";} else { $g14a = "#E7E6E8";}
if ($tl14 > 1 ){ $g14b = "#FFB640";} else { $g14b = "#E7E6E8";}
if ($tl14 > 2 ){ $g14c = "#FFB640";} else { $g14c = "#E7E6E8";}
if ($tl14 > 3 ){ $g14d = "#FFB640";} else { $g14d = "#E7E6E8";}
if ($tl14 > 4 ){ $g14e = "#FFB640";} else { $g14e = "#E7E6E8";}
if ($tl14 > 5 ){ $g14f = "#FFB640";} else { $g14f = "#E7E6E8";}
// Dummy Variable Graph Bar VIOLET
if ($tl15 > 0 ){ $g15a = "#BB6AFC";} else { $g15a = "#E7E6E8";}
if ($tl15 > 1 ){ $g15b = "#BB6AFC";} else { $g15b = "#E7E6E8";}
if ($tl15 > 2 ){ $g15c = "#BB6AFC";} else { $g15c = "#E7E6E8";}
if ($tl15 > 3 ){ $g15d = "#BB6AFC";} else { $g15d = "#E7E6E8";}
if ($tl15 > 4 ){ $g15e = "#BB6AFC";} else { $g15e = "#E7E6E8";}
if ($tl15 > 5 ){ $g15f = "#BB6AFC";} else { $g15f = "#E7E6E8";}
if ($tl16 > 0 ){ $g16a = "#BB6AFC";} else { $g16a = "#E7E6E8";}
if ($tl16 > 1 ){ $g16b = "#BB6AFC";} else { $g16b = "#E7E6E8";}
if ($tl16 > 2 ){ $g16c = "#BB6AFC";} else { $g16c = "#E7E6E8";}
if ($tl16 > 3 ){ $g16d = "#BB6AFC";} else { $g16d = "#E7E6E8";}
if ($tl16 > 4 ){ $g16e = "#BB6AFC";} else { $g16e = "#E7E6E8";}
if ($tl16 > 5 ){ $g16f = "#BB6AFC";} else { $g16f = "#E7E6E8";}
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information

// set header and footer fonts

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);


$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
   require_once(dirname(__FILE__).'/lang/eng.php');
   $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
// add a page
$pdf->AddPage();


$pdf->SetFont('helvetica', '', 6);

// -----------------------------------------------------------------------------

$tbl = <<<EOD
<div align="center"><img src="bps.jpg" width="150px"><BR>
<H3>BADAN PUSAT STATISTIK</H3><BR><BR><BR><BR>
<H1><B>LAPORAN <BR>
INDIVIDUAL</B></H1><BR>

<H3>
HASIL ASESMEN KOMPETENSI PEJABAT<BR>
BADAN PUSAT STATISTIK<BR>
REPUBLIK INDONESIA
</H3><BR><BR><BR>

<H1><B>PROVINSI ACEH</B></H1><BR><BR>

<H3><B><U>$struk</U></B></H3><BR><BR>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
<BR><BR><BR><BR><BR>

<H2><B>Nama Peserta :<BR></B>
<table>
<tr>
<td width="20%"></td>
<td width="60%">

<table cellspacing="0" cellpadding="1" border="1" align="center">
   <tr>
       <td colspan="2" align="center" height="15px"><B>$nama</B></td>
   </tr>
</table>
</td>
<td width="20%"></td>
</tr>
</table>
<BR>
<H2><B>NIP :<BR></B>
<table>
<tr>
<td width="20%"></td>
<td width="60%">

<table cellspacing="0" cellpadding="1" border="1"  align="center">
   <tr>
       <td colspan="2" align="center" height="15px"><B>$nip</B></td>
   </tr>
</table>
</td>
<td width="20%"></td>
</tr>
</table>

</H2>
<BR><BR><BR><BR><BR><BR><BR>
<H3>
Disampaikan Oleh :
<BR><BR>
<img src="ppsdm.jpg" width="150px">
<BR>

PT PRIMA PERSONA SUMBER DAYA MANDIRI
</H3>

</div>


EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// add a page
$pdf->AddPage();


$pdf->SetFont('helvetica', '', 6);

// -----------------------------------------------------------------------------

$tbl = <<<EOD

<table>
<tr>
<td width="15%" align="left"><p><img src="bps.jpg"></p></td>
<td width="70%" align="center"><H3><B><p>HASIL ASESMEN KOMPETENSI PEJABAT BADAN PUSAT STATISTIK (BPS)</p></B></H3></td>
<td width="15%" align="right"><p><img src="ppsdm.jpg"></p></td>
</tr>
<table>


<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td colspan="2" align="center" height="15px"><B>IDENTITAS PESERTA</B></td>
   </tr>
   <tr>

      <td>
      <table>
           <tr>
               <td>Eselon</td>
               <td>:</td>
               <td>$eselon</td>
           </tr>
           <tr>
               <td>Wilayah</td>
               <td>:</td>
               <td>$wilayah</td>
           </tr><tr>
               <td>NIP</td>
               <td>:</td>
               <td>$nip</td>
           </tr><tr>
               <td>Nama</td>
               <td>:</td>
               <td>$nama</td>
           </tr><tr>
               <td>Tgl Lahir</td>
               <td>:</td>
               <td>$ttl</td>
           </tr>
      </table>
      </td>

      <td>
           <table>
           <tr>
               <td>Jabatan</td>
               <td>:</td>
               <td>$jabatan</td>
           </tr>
           <tr>
               <td>Sekter</td>
               <td>:</td>
               <td>$satker</td>
           </tr><tr>
               <td>Golongan</td>
               <td>:</td>
               <td>$golongan</td>
           </tr><tr>
               <td>Pendidikan</td>
               <td>:</td>
               <td>$pendidikan</td>
           </tr><tr>
               <td>Tgl Tes</td>
               <td>:</td>
               <td>$tgltes</td>
           </tr>
           </table>
      </td>
   </tr>

</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->SetFont('helvetica', '', 5);

$tbl = <<<EOD
</BR>
<table>
<tr>
<td width="80%" >
<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#9E9E9E" colspan="2" rowspan="2" align="center" width="10%"><B>CLUSTER</B></td>
       <td colspan="2" rowspan="2" align="center" width="19%"><B>COMPETENCY</B></td>
       <td colspan="2" rowspan="2" align="center" width="51%"><B>DEFINISI OPERASIONAL</B></td>
       <td colspan="2" align="center" width="15%"><B>PROFICIENCY</B></td>
       <td colspan="2" rowspan="2" align="center" width="5%"><B>GAPS</B></td>
   </tr>
   <tr>
      <td bgcolor="#9E9E9E" align="center"><B>TARGET LEVEL</B></td>
      <td bgcolor="#E6E6E6" align="center"><B>ACTUAL LEVEL</B></td>
   </tr>

</table>

</td>
<td width="1%"></td>
<td width="19%">

</td>
</tr>
</table>

<table>
<tr>
<td width="80%" ></td>
<td width="1%"></td>
<td width="19%">
<table >
<tr>
       0
       <td align="center">1</td>
       <td align="center">2</td>
       <td align="center">3</td>
       <td align="center">4</td>
       <td align="center">5</td>
       <td align="center">6</td>
   </tr>
</table>
</td>
</tr>
</table>


<table>
<tr>
<td width="80%" >

<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#0AACFC" rowspan="4" align="center" width="10%"><B>CLUSTER</B></td>
       <td bgcolor="#CAEAFA" align="center" width="3%"><B>1</B></td>
       <td bgcolor="#CAEAFA" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td bgcolor="#CAEAFA" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td bgcolor="#0AACFC" align="center" width="7.5%"><B>$tl1</B></td>
       <td bgcolor="#5AD2FA" align="center" width="7.5%"><B>$al1</B></td>
       <td bgcolor="#CAEAFA" align="center" width="5%"><B>$gaps1</B></td>
   </tr>
   <tr>
       <td  bgcolor="#CAEAFA" align="center" width="3%"><B>2</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#0AACFC" align="center" width="7.5%"><B>$tl2</B></td>
       <td  bgcolor="#5AD2FA" align="center" width="7.5%"><B>$al2</B></td>
       <td  bgcolor="#CAEAFA" align="center" width="5%"><B>$gaps2</B></td>
   </tr>
   <tr>
       <td  bgcolor="#CAEAFA" align="center" width="3%"><B>3</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#0AACFC" align="center" width="7.5%"><B>$tl3</B></td>
       <td  bgcolor="#5AD2FA" align="center" width="7.5%"><B>$al3</B></td>
       <td  bgcolor="#CAEAFA" align="center" width="5%"><B>$gaps3</B></td>
   </tr>
   <tr>
       <td  bgcolor="#CAEAFA" align="center" width="3%"><B>4</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#CAEAFA" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#0AACFC" align="center" width="7.5%"><B>$tl4</B></td>
       <td  bgcolor="#5AD2FA" align="center" width="7.5%"><B>$al4</B></td>
       <td  bgcolor="#CAEAFA" align="center" width="5%"><B>$gaps4</B></td>
   </tr>
</table>

<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#F70A59" rowspan="5" align="center" width="10%"><B>CLUSTER</B></td>
       <td bgcolor="#FCBDF8" align="center" width="3%"><B>5</B></td>
       <td bgcolor="#FCBDF8" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td bgcolor="#FCBDF8" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td bgcolor="#F70A59" align="center" width="7.5%"><B>$tl5</B></td>
       <td bgcolor="#FC568D" align="center" width="7.5%"><B>$al5</B></td>
       <td bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps5</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FCBDF8" align="center" width="3%"><B>6</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#F70A59" align="center" width="7.5%"><B>$tl6</B></td>
       <td  bgcolor="#FC568D" align="center" width="7.5%"><B>$al6</B></td>
       <td  bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps6</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FCBDF8" align="center" width="3%"><B>7</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#F70A59" align="center" width="7.5%"><B>$tl7</B></td>
       <td  bgcolor="#FC568D" align="center" width="7.5%"><B>$al7</B></td>
       <td  bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps7</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FCBDF8" align="center" width="3%"><B>8</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#F70A59" align="center" width="7.5%"><B>$tl8</B></td>
       <td  bgcolor="#FC568D" align="center" width="7.5%"><B>$al8</B></td>
       <td  bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps8</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FCBDF8" align="center" width="3%"><B>9</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#FCBDF8" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#F70A59" align="center" width="7.5%"><B>$tl9</B></td>
       <td  bgcolor="#FC568D" align="center" width="7.5%"><B>$al9</B></td>
       <td  bgcolor="#FCBDF8" align="center" width="5%"><B>$gaps9</B></td>
   </tr>
</table>

<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#02DB4E" rowspan="5" align="center" width="10%"><B>CLUSTER</B></td>
       <td bgcolor="#B9FCA7" align="center" width="3%"><B>10</B></td>
       <td bgcolor="#B9FCA7" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td bgcolor="#B9FCA7" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td bgcolor="#02DB4E" align="center" width="7.5%"><B>$tl10</B></td>
       <td bgcolor="#3CFA7E" align="center" width="7.5%"><B>$al10</B></td>
       <td bgcolor="#B9FCA7" align="center" width="5%"><B>$gaps10</B></td>
   </tr>
</table>

<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#FC7303" rowspan="4" align="center" width="10%"><B>CLUSTER</B></td>
       <td bgcolor="#FAD29B" align="center" width="3%"><B>11</B></td>
       <td bgcolor="#FAD29B" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td bgcolor="#FAD29B" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td bgcolor="#FC7303" align="center" width="7.5%"><B>$tl11</B></td>
       <td bgcolor="#FFB640" align="center" width="7.5%"><B>$al11</B></td>
       <td bgcolor="#FAD29B" align="center" width="5%"><B>$gaps11</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>12</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl12</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al12</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps12</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>13</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl13</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al13</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps13</B></td>
   </tr>
   <tr>
       <td  bgcolor="#FAD29B" align="center" width="3%"><B>14</B></td>
       <td  bgcolor="#FAD29B" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#FAD29B" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#FC7303" align="center" width="7.5%"><B>$tl14</B></td>
       <td  bgcolor="#FFB640" align="center" width="7.5%"><B>$al14</B></td>
       <td  bgcolor="#FAD29B" align="center" width="5%"><B>$gaps14</B></td>
   </tr>
</table>

<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td bgcolor="#9717FF" rowspan="4" align="center" width="10%"><B>CLUSTER</B></td>
       <td bgcolor="#E0BDFC" align="center" width="3%"><B>15</B></td>
       <td bgcolor="#E0BDFC" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td bgcolor="#E0BDFC" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td bgcolor="#9717FF" align="center" width="7.5%"><B>$tl15</B></td>
       <td bgcolor="#BB6AFC" align="center" width="7.5%"><B>$al15</B></td>
       <td bgcolor="#E0BDFC" align="center" width="5%"><B>$gaps15</B></td>
   </tr>
   <tr>
       <td  bgcolor="#E0BDFC" align="center" width="3%"><B>16</B></td>
       <td  bgcolor="#E0BDFC" align="left" width="16%"><B>Berorientasi pada Kualitas</B></td>
       <td  bgcolor="#E0BDFC" align="left" width="51%">Kemampuan menyesuaikan dan melengkapi sudut pandang pribadi menggunakan metode elektrik untuk menyempurnakan cara berpikir dan menemukan pendekatan yang lebih sesuai.</td>
       <td  bgcolor="#9717FF" align="center" width="7.5%"><B>$tl16</B></td>
       <td  bgcolor="#BB6AFC" align="center" width="7.5%"><B>$al16</B></td>
       <td  bgcolor="#E0BDFC" align="center" width="5%"><B>$gaps16</B></td>
   </tr>

</table>




</td>
<td width="1%"></td>
<td width="19%">

<table cellspacing="2" cellpadding="0" border="0">
   <tr>
       <td bgcolor="$g1a" height="15.5px"></td>
       <td bgcolor="$g1b"></td>
       <td bgcolor="$g1c"></td>
       <td bgcolor="$g1d"></td>
       <td bgcolor="$g1e"></td>
       <td bgcolor="$g1f"></td>
   </tr>

   <tr>
       <td bgcolor="$g2a" height="15.5px"></td>
       <td bgcolor="$g2b"></td>
       <td bgcolor="$g2c"></td>
       <td bgcolor="$g2d"></td>
       <td bgcolor="$g2e"></td>
       <td bgcolor="$g2f"></td>
   </tr>

   <tr>
       <td bgcolor="$g3a" height="15.5px"></td>
       <td bgcolor="$g3b"></td>
       <td bgcolor="$g3c"></td>
       <td bgcolor="$g3d"></td>
       <td bgcolor="$g3e"></td>
       <td bgcolor="$g3f"></td>
   </tr>

   <tr>
       <td bgcolor="$g4a" height="15.5px"></td>
       <td bgcolor="$g4b"></td>
       <td bgcolor="$g4c"></td>
       <td bgcolor="$g4d"></td>
       <td bgcolor="$g4e"></td>
       <td bgcolor="$g4f"></td>
   </tr>

   <tr>
       <td bgcolor="$g5a" height="15.5px"></td>
       <td bgcolor="$g5b"></td>
       <td bgcolor="$g5c"></td>
       <td bgcolor="$g5d"></td>
       <td bgcolor="$g5e"></td>
       <td bgcolor="$g5f"></td>
   </tr>

   <tr>
       <td bgcolor="$g6a" height="15.5px"></td>
       <td bgcolor="$g6b"></td>
       <td bgcolor="$g6c"></td>
       <td bgcolor="$g6d"></td>
       <td bgcolor="$g6e"></td>
       <td bgcolor="$g6f"></td>
   </tr>

   <tr>
       <td bgcolor="$g7a" height="15.5px"></td>
       <td bgcolor="$g7b"></td>
       <td bgcolor="$g7c"></td>
       <td bgcolor="$g7d"></td>
       <td bgcolor="$g7e"></td>
       <td bgcolor="$g7f"></td>
   </tr>

   <tr>
       <td bgcolor="$g8a" height="15.5px"></td>
       <td bgcolor="$g8b"></td>
       <td bgcolor="$g8c"></td>
       <td bgcolor="$g8d"></td>
       <td bgcolor="$g8e"></td>
       <td bgcolor="$g8f"></td>
   </tr>

   <tr>
       <td bgcolor="$g9a" height="15.5px"></td>
       <td bgcolor="$g9b"></td>
       <td bgcolor="$g9c"></td>
       <td bgcolor="$g9d"></td>
       <td bgcolor="$g9e"></td>
       <td bgcolor="$g9f"></td>
   </tr>

   <tr>
       <td bgcolor="$g10a" height="15.5px"></td>
       <td bgcolor="$g10b"></td>
       <td bgcolor="$g10c"></td>
       <td bgcolor="$g10d"></td>
       <td bgcolor="$g10e"></td>
       <td bgcolor="$g10f"></td>
   </tr>

   <tr>
       <td bgcolor="$g11a" height="15.5px"></td>
       <td bgcolor="$g11b"></td>
       <td bgcolor="$g11c"></td>
       <td bgcolor="$g11d"></td>
       <td bgcolor="$g11e"></td>
       <td bgcolor="$g11f"></td>
   </tr>

   <tr>
       <td bgcolor="$g12a" height="15.5px"></td>
       <td bgcolor="$g12b"></td>
       <td bgcolor="$g12c"></td>
       <td bgcolor="$g12d"></td>
       <td bgcolor="$g12e"></td>
       <td bgcolor="$g12f"></td>
   </tr>

   <tr>
       <td bgcolor="$g13a" height="15.5px"></td>
       <td bgcolor="$g13b"></td>
       <td bgcolor="$g13c"></td>
       <td bgcolor="$g13d"></td>
       <td bgcolor="$g13e"></td>
       <td bgcolor="$g13f"></td>
   </tr>

   <tr>
       <td bgcolor="$g14a" height="15.5px"></td>
       <td bgcolor="$g14b"></td>
       <td bgcolor="$g14c"></td>
       <td bgcolor="$g14d"></td>
       <td bgcolor="$g14e"></td>
       <td bgcolor="$g14f"></td>
   </tr>

   <tr>
       <td bgcolor="$g15a" height="15.5px"></td>
       <td bgcolor="$g15b"></td>
       <td bgcolor="$g15c"></td>
       <td bgcolor="$g15d"></td>
       <td bgcolor="$g15e"></td>
       <td bgcolor="$g15f"></td>
   </tr>

   <tr>
       <td bgcolor="$g16a" height="15.5px"></td>
       <td bgcolor="$g16b"></td>
       <td bgcolor="$g16c"></td>
       <td bgcolor="$g16d"></td>
       <td bgcolor="$g16e"></td>
       <td bgcolor="$g16f"></td>
   </tr>

</table>

</td>
</tr>
</table>
<table>
<tr>
<td width="80%" ></td>
<td width="1%"></td>
<td width="19%">
<table>
<tr>
       0
       <td align="center">1</td>
       <td align="center">2</td>
       <td align="center">3</td>
       <td align="center">4</td>
       <td align="center">5</td>
       <td align="center">6</td>
   </tr>
</table>
</td>
</tr>
</table>


<table cellspacing="0" cellpadding="1" border="1" width="80%">
   <tr>
       <td align="center" width="80%"><h3><B>TOTAL</B></h3></td>
       <td bgcolor="#9E9E9E" align="center" width="7.5%"><B>$tltot</B></td>
       <td bgcolor="#E6E6E6" align="center" width="7.5%"><B>$altot</B></td>
       <td align="center" width="5%"><B>$gapstot</B></td>
   </tr>

</table>

EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->SetFont('helvetica', '', 6);

// -----------------------------------------------------------------------------

$tbl = <<<EOD

<table cellspacing="0" cellpadding="0" border="0">
   <tr>
       <td colspan="3" align="center" height="15px"><B></B></td>
   </tr>
   <tr>
      <td width="49%">

      <table border="1">
           <tr>
               <td bgcolor="#5AD2FA" colspan="2" align="center" width="65%"><B>INDEX</B></td>
               <td bgcolor="#5AD2FA" align="center" width="35%"><B>KETERANGAN</B></td>
           </tr>
           <tr>
               <td width="50%">JOB FIT</td>
               <td align="center" width="15%">85,7%</td>
               <td width="35%">Cukup Sesuai</td>
           </tr>
           <tr>
               <td>POTENSI PENGEMBANGAN DIRI</td>
               <td align="center">4,3</td>
               <td>Beragam</td>
           </tr>
           <tr>
               <td>KEMAMPUAN BELAJAR</td>
               <td align="center">5,3</td>
               <td>Medium / Soft Learner</td>
           </tr>
      </table>

      <br><br>

      <table border="1">
           <tr>
               <td bgcolor="#5AD2FA" align="right"><B>PENJELASAN</B></td>

           </tr>
           <tr>
               <td>
               <BR><p>Reza Septian Perdana, ST telah dapat menampilkan kemampuan dan karakter dominan cukup sesuai dengan tingkat kemahiran yang dipersyaratkan dalam jabatannya saat ini.</p><BR>
               </td>

           </tr>
           <tr>
               <td>
               <BR><p></p><BR>
               </td>

           </tr>
           <tr>
               <td>
               <BR><p></p><BR>
               </td>

           </tr>
           <tr>
               <td>
               <BR><p></p><BR>
               </td>

           </tr>
           <tr>
               <td bgcolor="#5AD2FA" align="right"><B>REKOMENDASI</B></td>

           </tr>
           <tr>
               <td>
               <BR><p></p><BR>
               </td>

           </tr>
      </table>

      </td>

      <td width="2%"></td>

      <td width="49%">

           <table border="1">
           <tr>
               <td><BR><BR><BR><BR><BR></td>
           </tr>
          </table>
      </td>

   </tr>

</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// add a page
$pdf->AddPage();


$pdf->SetFont('helvetica', '', 6);

// -----------------------------------------------------------------------------

$tbl = <<<EOD

<table>
<tr>
<td width="15%" align="left"><p><img src="bps.jpg"></p></td>
<td width="70%" align="center"><H3><B><p>HASIL ASESMEN KOMPETENSI PEJABAT BADAN PUSAT STATISTIK (BPS)</p></B></H3></td>
<td width="15%" align="right"><p><img src="ppsdm.jpg"></p></td>
</tr>
<table>


<table cellspacing="0" cellpadding="1" border="1">
   <tr>
       <td colspan="2" align="center" height="15px"><B>IDENTITAS PESERTA</B></td>
   </tr>
   <tr>

      <td>

      <table>
           <tr>
               <td>Eselon</td>
               <td>:</td>
               <td>$eselon</td>
           </tr>
           <tr>
               <td>Wilayah</td>
               <td>:</td>
               <td>$wilayah</td>
           </tr><tr>
               <td>NIP</td>
               <td>:</td>
               <td>$nip</td>
           </tr><tr>
               <td>Nama</td>
               <td>:</td>
               <td>$nama</td>
           </tr><tr>
               <td>Tgl Lahir</td>
               <td>:</td>
               <td>$ttl</td>
           </tr>
      </table>
      </td>

      <td>
           <table>
           <tr>
               <td>Jabatan</td>
               <td>:</td>
               <td>$jabatan</td>
           </tr>
           <tr>
               <td>Sakter</td>
               <td>:</td>
               <td>$satker</td>
           </tr><tr>
               <td>Golongan</td>
               <td>:</td>
               <td>$golongan</td>
           </tr><tr>
               <td>Pendidikan</td>
               <td>:</td>
               <td>$pendidikan</td>
           </tr><tr>
               <td>Tgl Tes</td>
               <td>:</td>
               <td>$tgltes</td>
           </tr>
           </table>
      </td>
   </tr>

</table>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
<p><img src="example.polar.png"></p>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
<div align="right">
Jakarta, $tglsekarang<BR><BR><BR><BR><BR><BR><BR>
<U>($ttd)</U><BR>
No. HIMPSI: $himpsi
</div>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
