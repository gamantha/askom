<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Observation;
use app\models\PsikotesResult;
use app\models\Result;
use app\models\Pegawai;
use app\models\ResultCompetency;
use app\models\RefScale;
use app\models\RefUraian;
use app\models\RefCompetency;
use app\models\RefProjectConfig;
use app\models\RefGolonganPegawaiAdjustment;
use app\models\PsikotesSubtestResult;

/* @var $this yii\web\View */
/* @var $model app\models\Assessment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Assessments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'assessment_name',
            'project_id',
            [
             'label' => 'Nama',
             'value' => explode('_',$model->assessment_name)[0],
            ],
            [
             'label' => 'Golongan',
             'value' => Pegawai::find()->andWhere(['nama_pegawai' => explode('_',$model->assessment_name)[0]])->One()->golongan_pegawai,
            ],
        ],
    ]) ?>

    <?php
    $observation = Observation::find()->andWhere(['assessment_id' => $model->id])->One();
        $psikotes = PsikotesResult::find()->andWhere(['assessment_id' => $model->id])->One();
                $psikotes_result = PsikotesSubtestResult::find()->andWhere(['psikotes_result_id' => $psikotes->id])->All();
                $result = Result::find()->andWhere(['observation_id' => $observation->id])->One();

    echo '<pre>';
    //print_r($observation);
    echo '<h2>PSIKOTES</h2>';
    echo '<br/>psikotes id : ' . $psikotes->id;
    echo '<br/>no meja : ' . $psikotes->no_meja;
        echo '<br/>pegawai id : ' . $psikotes->pegawai_id;
       echo '<br/>kode buku : ' . $psikotes->kode_buku;
       echo '<br/>kode daerah : ' . $psikotes->kode_daerah;
       echo '<br/>asesor id : ' . $psikotes->asesor_id;


    echo '<h2>OBSERVATION</h2>';
    echo '<br/>observation id : ' . $observation->id;
        echo '<br/>warteg depth : ' . $observation->warteg_depth;
            echo '<br/>warteg detail : ' . $observation->warteg_detail;
                echo '<br/>warteg finesse : ' . $observation->warteg_finesse;

    echo '</pre>';
        echo '<h1>OBSERVATION RESULT</h1>';
        $project_id = $model->project_id;
        $project_config = RefProjectConfig::find()->andWhere(['project_id' => $project_id])->One();
                $competency_list = RefCompetency::find()->andWhere(['competency_set' => $project_config->competency_set_id])->orderBy(['order' => SORT_ASC])->All();

        $distinct_competency_list = RefCompetency::find()->select(['competency'])->distinct()->andWhere(['competency_set' => $project_config->competency_set_id])->orderBy(['order' => SORT_ASC])->All();

        foreach($distinct_competency_list as $distinct_competency_list_key => $distinct_competency_list_value) {
              $uraian_array = [];
              $ks_array =[];
         $a = 0;
         $sigma = 0;
         echo '<br/>';
         echo '<h2>' . $distinct_competency_list_value->competency . '</h2>';
                 $competency_list = RefCompetency::find()->andWhere(['competency_set' => $project_config->competency_set_id])
                 ->andWhere(['competency' => $distinct_competency_list_value->competency])
                 ->orderBy(['order' => SORT_ASC])->All();


                 foreach($competency_list as $competency_key => $competency_value)
                 {
                  echo '<br/>';

                                  $result_competency = ResultCompetency::find()->andWhere(['observation_id' => $observation->id])->andWhere(['competency_set_id' => $project_config->competency_set_id])
                                  ->andWhere(['competency' => $distinct_competency_list_value->competency])
                                  ->andWhere(['sub_competency' => $competency_value->sub_competency])->One();
                 echo $competency_value->competency . ' : ' . $competency_value->sub_competency . ' : ' .  $result_competency->result . $result_competency->description;
               //  if ($result_competency->description != '') {
                  //array_push($uraian_array, $result_competency->result . $result_competency->description);
                  $uraian_array[$competency_value->sub_competency] = $result_competency->result . $result_competency->description;
                 //}
                 if (($competency_value->sub_competency == 'knowledge') ||  ($competency_value->sub_competency == 'skill')) {
                   $ks_array[$competency_value->sub_competency] = $result_competency->result;
                 } else {
                  $a = $a + $result_competency->result;
                 }
                    $sigma = $sigma + $result_competency->result;

                 }
                  echo '<br/>A = ' . $a;
                  echo '<br/><pre>Kode uraian : ';
                  print_r($uraian_array);
                  $uraian_final ='';
                  $uraian_ks_final = '';
                  foreach($uraian_array as $uraian_array_key => $uraian_array_value) {
              //     echo $uraian_array_key;
                   $uraian = RefUraian::find()
                   ->andWhere(['competency_set_id' => $project_config->competency_set_id])
                   ->andWhere(['competency' => $distinct_competency_list_value->competency])
                     ->andWhere(['sub_competency' => $uraian_array_key])
                     ->andWhere(['kode' => $uraian_array_value])
                     ->andWhere(['seed' => '1'])   //DISINI FUNGSI UNTUK RANDOM
                   ->One();

                   if (isset($uraian)) {
                      $uraian_final = $uraian_final . ' ' . $uraian->uraian;
                   }

                  }

                   echo 'Uraian A : ' . $uraian_final;
                   $uraianks = RefUraian::find()
                   ->andWhere(['competency_set_id' => $project_config->competency_set_id])
                   ->andWhere(['competency' => $distinct_competency_list_value->competency])
                     ->andWhere(['sub_competency' => 'knowledge'])
                     ->andWhere(['kode' => $ks_array['knowledge'] . $ks_array['skill']])
                     ->andWhere(['seed' => '1'])   //DISINI FUNGSI UNTUK RANDOM
                   ->One();

                   if (isset($uraianks)) {
                      $uraian_ks_final = $uraianks->uraian;
                   }

                   echo '<br/>Uraian K-S : ' . $uraian_ks_final;

                  echo '</pre>';
                 echo '<br/>Sigma = ' . $sigma;
                 echo '<br/>';
                 $refgolonganpegawai = RefGolonganPegawaiAdjustment::find()
                 ->andWhere(['competency_set_id' => $project_config->competency_set_id])
                 ->andWhere(['golongan_pegawai'=> Pegawai::find()->andWhere(['nama_pegawai' => explode('_',$model->assessment_name)[0]])->One()->golongan_pegawai])
                 ->andWhere(['competency' => $distinct_competency_list_value->competency])
                   ->andWhere(['sub_competency' => 'NONE'])
                   ->One();
                 echo '<br/>Target sigma = ' . (isset($refgolonganpegawai->target) ? $refgolonganpegawai->target : 0) ;
                 $gap = (isset($refgolonganpegawai->target) ? $refgolonganpegawai->target - $sigma : 0 - $sigma);
                   echo '<br/>GAP = ' .  $gap;
                echo '<br/>Minimal = ' . (isset($refgolonganpegawai->minimum) ? $refgolonganpegawai->minimum : 0) ;
                echo '<br/>Adjustment = ' . (isset($refgolonganpegawai->adjustment) ? $refgolonganpegawai->adjustment : 0) ;
                echo '<br/>asesor adjustment = ' . $a;



        }


    echo '<h1>PSIKOTES RESULT</h1>';
    $cfit_array = ['CFIT 1', 'CFIT 2', 'CFIT 3', 'CFIT 4'];
    $truecount_array=[];
    $total_cfit = 0;
    foreach ($psikotes_result as $psikotes_subtest_result_key => $psikotes_subtest_result_value) {
     echo '<br/><br/>' . $psikotes_subtest_result_value->id . ' : ' . $psikotes_subtest_result_value->psikotes_subtest_name;
     if (in_array($psikotes_subtest_result_value->psikotes_subtest_name, $cfit_array)) {
       $total_cfit = $total_cfit + $psikotes_subtest_result_value->true_count;
     }
          $truecount_array[$psikotes_subtest_result_value->psikotes_subtest_name] = $psikotes_subtest_result_value->true_count;
          echo '<br/>True : ' . $psikotes_subtest_result_value->true_count;
          echo '<br/>False : ' . $psikotes_subtest_result_value->false_count;
          echo '<br/>Blank : ' . $psikotes_subtest_result_value->blank_count;

    }

 echo '<pre>';
 $cfit_iq = RefScale::find()->andWhere(['scale_name' =>  'iq'])->andWhere(['unscaled' => $total_cfit])->One();
 $subtest5_scaled = RefScale::find()->andWhere(['scale_name' =>  'subtest5'])->andWhere(['<=','unscaled',$truecount_array['SUBTEST 5']])->orderBy(['unscaled'=>SORT_DESC])->One();
  $subtest6_scaled = RefScale::find()->andWhere(['scale_name' =>  'subtest6'])->andWhere(['unscaled' => $truecount_array['SUBTEST 6']])->One();
   $subtest7_scaled = RefScale::find()->andWhere(['scale_name' =>  'subtest7'])->andWhere(['unscaled' => $truecount_array['SUBTEST 7']])->One();
               echo '<br/>Total CFIT : ' . $total_cfit;
                             echo '<br/>IQ (MAX 130): ' . $cfit_iq->scaled;
               echo '<br/>SUBTEST 5 : ' . $truecount_array['SUBTEST 5'];
               echo '<br/>SUBTEST 5 scaled : ' . $subtest5_scaled->scaled;
               echo '<br/>SUBTEST 6 : ' . $truecount_array['SUBTEST 6'];
               echo '<br/>SUBTEST 6 scaled : ' . $subtest6_scaled->scaled;
               echo '<br/>SUBTEST 7 : ' . $truecount_array['SUBTEST 7'];
               echo '<br/>SUBTEST 7 scaled : ' . $subtest7_scaled->scaled;
  echo '</pre> SUBTEST 4 : ';


echo $truecount_array['CFIT 4'];
     ?>

</div>
