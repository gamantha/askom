<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Upload File'), ['upload', 'project_id' => $_REQUEST['project_id']], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'project_id',
            'file_location:ntext',
            'created_at',
            [
             'label' => 'assessment_id',
           'value' => function($data){return '1';},
            ],
            [
             'label' => 'competency_set_id',
             'value' => function($data){return '1';},
            ],
            'modified_at',

            //['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{processall} {undoprocess}',
                          'buttons'=>[
                            'process_observasi' => function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-glass"></span>', Url::to(['observation/process_observasi','id'=>$model->id,'aid'=>'1','csid'=>'1']), [
                                      'title' => Yii::t('yii', 'Process observasi'),
                              ]);
                            },
                            'process_linkage' => function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-heart"></span>', Url::to(['observation/process_linkage','id'=>$model->id,'aid'=>'1','csid'=>'1']), [
                                      'title' => Yii::t('yii', 'Process linkage'),
                              ]);

                            },

                            'process_psikotes' => function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-asterisk"></span>',
                              Url::to(['observation/process_psikotes','id'=>$model->id,'aid'=>'1','csid'=>'1'])
                              , [
                                      'title' => Yii::t('yii', 'Process psikotes'),
                              ]);

                            },
                            'processall' => function ($url, $model) {
                              return Html::a('<span>proses data</span>',
                              Url::to(['assessment/processall','id'=>$model->id, 'project_id' => $_REQUEST['project_id'], 'start_row'=> 0, 'max_rows' => 500])
                              , [
                                      'title' => Yii::t('yii', 'Process psikotes'),
                              ]);

                            },
                            'undoprocess' => function ($url, $model) {
                              return Html::a('<span>UNDO</span>',
                              Url::to(['assessment/undoprocess','id'=>$model->id, 'project_id' => $_REQUEST['project_id']])
                              , [     'data-confirm' => 'are you sure?',
                                      'title' => Yii::t('yii', 'Undo Process'),
                              ]);

                            },
                        ]
                          ],


        ],
    ]); ?>

</div>
