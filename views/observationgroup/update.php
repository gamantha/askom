<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ObservationGroup */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Observation Group',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Observation Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="observation-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
