<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObservationGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="observation-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'observation_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_meja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meja_awal')->textInput() ?>

    <?= $form->field($model, 'meja_akhir')->textInput() ?>

    <?= $form->field($model, 'no_meja')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
