<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ObservationGroup */

$this->title = Yii::t('app', 'Create Observation Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Observation Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="observation-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
