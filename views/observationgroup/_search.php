<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObservationGroupSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="observation-group-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'observation_id') ?>

    <?= $form->field($model, 'group_meja') ?>

    <?= $form->field($model, 'meja_awal') ?>

    <?= $form->field($model, 'meja_akhir') ?>

    <?php // echo $form->field($model, 'no_meja') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
