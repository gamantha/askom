<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RefCompetencySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-competency-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'competency_set') ?>

    <?= $form->field($model, 'competency') ?>

    <?= $form->field($model, 'sub_competency') ?>

    <?= $form->field($model, 'false') ?>

    <?= $form->field($model, 'true') ?>

    <?php // echo $form->field($model, 'linkage') ?>

    <?php // echo $form->field($model, 'order') ?>

    <?php // echo $form->field($model, 'uselinkage') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
