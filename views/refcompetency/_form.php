<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RefCompetency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-competency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'competency_set_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'competency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sub_competency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'trait_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'trait_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'linkage')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
