<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefCompetency */

$this->title = Yii::t('app', 'Create Ref Competency');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Competencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-competency-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
