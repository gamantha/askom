<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "institusi".
 *
 * @property integer $id
 * @property string $nama_institusi
 *
 * @property Jabatan[] $jabatans
 */
class Institusi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institusi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_institusi'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_institusi' => Yii::t('app', 'Nama Institusi'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJabatans()
    {
        return $this->hasMany(Jabatan::className(), ['institusi_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return InstitusiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InstitusiQuery(get_called_class());
    }
}
