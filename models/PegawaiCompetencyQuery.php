<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PegawaiCompetency]].
 *
 * @see PegawaiCompetency
 */
class PegawaiCompetencyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PegawaiCompetency[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PegawaiCompetency|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}