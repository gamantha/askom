<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "psikotes_file".
 *
 * @property integer $id
 * @property string $file_location
 * @property string $created_at
 * @property string $modified_at
 *
 * @property PsikotesData[] $psikotesDatas
 */
class PsikotesFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psikotes_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_location'], 'string'],
            [['created_at', 'modified_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_location' => 'File Location',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsikotesDatas()
    {
        return $this->hasMany(PsikotesData::className(), ['file_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PsikotesFileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PsikotesFileQuery(get_called_class());
    }
}
