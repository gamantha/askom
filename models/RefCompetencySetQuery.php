<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RefCompetencySet]].
 *
 * @see RefCompetencySet
 */
class RefCompetencySetQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RefCompetencySet[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RefCompetencySet|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}