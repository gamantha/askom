<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RefSaran]].
 *
 * @see RefSaran
 */
class RefSaranQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RefSaran[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RefSaran|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}