<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_gol_asesor".
 *
 * @property integer $id
 * @property string $nama_golongan_asesor
 * @property string $kategori_golongan
 */
class RefGolAsesor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_gol_asesor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nama_golongan_asesor', 'kategori_golongan'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_golongan_asesor' => Yii::t('app', 'Nama Golongan Asesor'),
            'kategori_golongan' => Yii::t('app', 'Kategori Golongan'),
        ];
    }

    /**
     * @inheritdoc
     * @return RefGolAsesorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefGolAsesorQuery(get_called_class());
    }
}
