<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RefCompetency;

/**
 * RefCompetencySearch represents the model behind the search form about `app\models\RefCompetency`.
 */
class RefCompetencySearch extends RefCompetency
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competency_set', 'competency', 'sub_competency', 'false', 'true', 'linkage', 'uselinkage'], 'safe'],
            [['order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefCompetency::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order' => $this->order,
        ]);

        $query->andFilterWhere(['like', 'competency_set', $this->competency_set])
            ->andFilterWhere(['like', 'competency', $this->competency])
            ->andFilterWhere(['like', 'sub_competency', $this->sub_competency])
            ->andFilterWhere(['like', 'false', $this->false])
            ->andFilterWhere(['like', 'true', $this->true])
            ->andFilterWhere(['like', 'linkage', $this->linkage])
            ->andFilterWhere(['like', 'uselinkage', $this->uselinkage]);

        return $dataProvider;
    }
}
