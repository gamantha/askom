<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_psikotes_subtest".
 *
 * @property string $psikotes_set
 * @property string $subtest_name
 * @property integer $order
 *
 * @property PsikotesSubtestResult[] $psikotesSubtestResults
 * @property RefProjectConfig[] $refProjectConfigs
 */
class RefPsikotesSubtest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_psikotes_subtest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['psikotes_set', 'subtest_name'], 'required'],
            [['order'], 'integer'],
            [['psikotes_set', 'subtest_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'psikotes_set' => Yii::t('app', 'Psikotes Set'),
            'subtest_name' => Yii::t('app', 'Subtest Name'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsikotesSubtestResults()
    {
        return $this->hasMany(PsikotesSubtestResult::className(), ['psikotes_set' => 'psikotes_set', 'psikotes_subtest_name' => 'subtest_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefProjectConfigs()
    {
        return $this->hasMany(RefProjectConfig::className(), ['psikotes_set_id' => 'psikotes_set']);
    }

    /**
     * @inheritdoc
     * @return RefPsikotesSubtestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefPsikotesSubtestQuery(get_called_class());
    }
}
