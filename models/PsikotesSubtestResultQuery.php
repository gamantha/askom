<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PsikotesSubtestResult]].
 *
 * @see PsikotesSubtestResult
 */
class PsikotesSubtestResultQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PsikotesSubtestResult[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PsikotesSubtestResult|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
