<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ResultCompetency]].
 *
 * @see ResultCompetency
 */
class ResultCompetencyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ResultCompetency[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ResultCompetency|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
