<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_golongan_pegawai".
 *
 * @property string $golongan_pegawai
 * @property string $tipe_jabatan
 * @property string $rumpun
 * @property string $golkeg1
 * @property string $golkeg2
 *
 * @property Pegawai[] $pegawais
 * @property RefGolonganPegawaiAdjustment[] $refGolonganPegawaiAdjustments
 */
class RefGolonganPegawai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_golongan_pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['golongan_pegawai', 'tipe_jabatan', 'rumpun', 'golkeg1', 'golkeg2'], 'required'],
            [['golongan_pegawai', 'tipe_jabatan', 'rumpun', 'golkeg1', 'golkeg2'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'golongan_pegawai' => Yii::t('app', 'Golongan Pegawai'),
            'tipe_jabatan' => Yii::t('app', 'Tipe Jabatan'),
            'rumpun' => Yii::t('app', 'Rumpun'),
            'golkeg1' => Yii::t('app', 'Golkeg1'),
            'golkeg2' => Yii::t('app', 'Golkeg2'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['golongan_pegawai' => 'golongan_pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefGolonganPegawaiAdjustments()
    {
        return $this->hasMany(RefGolonganPegawaiAdjustment::className(), ['golongan_pegawai' => 'golongan_pegawai']);
    }

    /**
     * @inheritdoc
     * @return RefGolonganPegawaiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefGolonganPegawaiQuery(get_called_class());
    }
}
