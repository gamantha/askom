<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RefGolonganPegawai]].
 *
 * @see RefGolonganPegawai
 */
class RefGolonganPegawaiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RefGolonganPegawai[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RefGolonganPegawai|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
