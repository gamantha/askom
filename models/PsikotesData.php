<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "psikotes_data".
 *
 * @property integer $id
 * @property integer $file_id
 * @property string $data
 * @property string $status
 * @property string $created_at
 * @property string $modified_at
 *
 * @property PsikotesFile $file
 */
class PsikotesData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psikotes_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_id'], 'integer'],
            [['data', 'status'], 'string'],
            [['created_at', 'modified_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_id' => 'File ID',
            'data' => 'Data',
            'status' => 'Status',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(PsikotesFile::className(), ['id' => 'file_id']);
    }

    /**
     * @inheritdoc
     * @return PsikotesDataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PsikotesDataQuery(get_called_class());
    }
}
