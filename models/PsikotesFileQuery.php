<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PsikotesFile]].
 *
 * @see PsikotesFile
 */
class PsikotesFileQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PsikotesFile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PsikotesFile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}