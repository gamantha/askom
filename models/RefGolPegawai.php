<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_gol_pegawai".
 *
 * @property integer $id
 * @property string $nama_golongan_pegawai
 * @property string $kategori_golongan
 *
 * @property Pegawai[] $pegawais
 * @property PegawaiCompetency[] $pegawaiCompetencies
 */
class RefGolPegawai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_gol_pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_golongan_pegawai', 'kategori_golongan'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_golongan_pegawai' => Yii::t('app', 'Nama Golongan Pegawai'),
            'kategori_golongan' => Yii::t('app', 'Kategori Golongan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['gol_pegawai_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiCompetencies()
    {
        return $this->hasMany(PegawaiCompetency::className(), ['gol_pegawai_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return RefGolPegawaiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefGolPegawaiQuery(get_called_class());
    }
}
