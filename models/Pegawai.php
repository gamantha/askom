<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pegawai".
 *
 * @property integer $id
 * @property string $nama_pegawai
 * @property string $nip
 * @property string $usia
 * @property string $tanggal_lahir
 * @property string $notes
 *
 * @property Jabatan[] $jabatans
 * @property PsikotesResult[] $psikotesResults
 */
class Pegawai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notes'], 'string'],
            [['nama_pegawai', 'nip', 'usia', 'tanggal_lahir'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_pegawai' => Yii::t('app', 'Nama Pegawai'),
            'nip' => Yii::t('app', 'Nip'),
            'usia' => Yii::t('app', 'Usia'),
            'tanggal_lahir' => Yii::t('app', 'Tanggal Lahir'),
            'notes' => Yii::t('app', 'Notes'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJabatans()
    {
        return $this->hasMany(Jabatan::className(), ['pegawai_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsikotesResults()
    {
        return $this->hasMany(PsikotesResult::className(), ['pegawai_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PegawaiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PegawaiQuery(get_called_class());
    }
}
