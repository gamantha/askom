<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "assessment".
 *
 * @property string $id
 * @property string $assessment_name
 * @property string $project_id
 *
 * @property Project $project
 * @property Observation[] $observations
 * @property PsikotesResult[] $psikotesResults
 */
class Assessment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assessment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['assessment_name', 'project_id'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'project_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'assessment_name' => Yii::t('app', 'Assessment Name'),
            'project_id' => Yii::t('app', 'Project ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservations()
    {
        return $this->hasMany(Observation::className(), ['assessment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsikotesResults()
    {
        return $this->hasMany(PsikotesResult::className(), ['assessment_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return AssessmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AssessmentQuery(get_called_class());
    }
}
