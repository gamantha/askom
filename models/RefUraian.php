<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_uraian".
 *
 * @property string $competency_set_id
 * @property string $competency
 * @property string $sub_competency
 * @property string $kode
 * @property string $uraian
 * @property integer $seed
 *
 * @property RefCompetency $competencySet
 */
class RefUraian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_uraian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competency_set_id', 'competency', 'sub_competency', 'kode', 'seed'], 'required'],
            [['uraian'], 'string'],
            [['seed'], 'integer'],
            [['competency_set_id', 'competency', 'sub_competency', 'kode'], 'string', 'max' => 255],
            [['competency_set_id'], 'exist', 'skipOnError' => true, 'targetClass' => RefCompetency::className(), 'targetAttribute' => ['competency_set_id' => 'competency_set']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'competency_set_id' => Yii::t('app', 'Competency Set ID'),
            'competency' => Yii::t('app', 'Competency'),
            'sub_competency' => Yii::t('app', 'Sub Competency'),
            'kode' => Yii::t('app', 'Kode'),
            'uraian' => Yii::t('app', 'Uraian'),
            'seed' => Yii::t('app', 'Seed'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetencySet()
    {
        return $this->hasOne(RefCompetency::className(), ['competency_set' => 'competency_set_id']);
    }

    /**
     * @inheritdoc
     * @return RefUraianQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefUraianQuery(get_called_class());
    }
}
