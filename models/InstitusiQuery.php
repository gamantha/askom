<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Institusi]].
 *
 * @see Institusi
 */
class InstitusiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Institusi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Institusi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
