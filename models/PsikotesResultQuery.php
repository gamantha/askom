<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PsikotesResult]].
 *
 * @see PsikotesResult
 */
class PsikotesResultQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PsikotesResult[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PsikotesResult|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
