<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PegawaiExt]].
 *
 * @see PegawaiExt
 */
class PegawaiExtQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PegawaiExt[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PegawaiExt|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
