<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_saran".
 *
 * @property integer $id
 * @property integer $competency_set_id
 * @property string $competency_name
 * @property string $uraian
 * @property integer $seed
 */
class RefSaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_saran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competency_set_id', 'seed'], 'integer'],
            [['uraian'], 'string'],
            [['competency_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'competency_set_id' => Yii::t('app', 'Competency Set ID'),
            'competency_name' => Yii::t('app', 'Competency Name'),
            'uraian' => Yii::t('app', 'Uraian'),
            'seed' => Yii::t('app', 'Seed'),
        ];
    }

    /**
     * @inheritdoc
     * @return RefSaranQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefSaranQuery(get_called_class());
    }
}
