<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_golongan_asesor".
 *
 * @property string $golongan_asesor
 * @property string $competency_set_id
 * @property string $competency_name
 * @property string $sub_competency
 * @property integer $value
 *
 * @property Asesor[] $asesors
 */
class RefGolonganAsesor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_golongan_asesor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['golongan_asesor', 'competency_set_id', 'competency_name', 'sub_competency'], 'required'],
            [['value'], 'integer'],
            [['golongan_asesor', 'competency_set_id', 'competency_name', 'sub_competency'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'golongan_asesor' => Yii::t('app', 'Golongan Asesor'),
            'competency_set_id' => Yii::t('app', 'Competency Set ID'),
            'competency_name' => Yii::t('app', 'Competency Name'),
            'sub_competency' => Yii::t('app', 'Sub Competency'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsesors()
    {
        return $this->hasMany(Asesor::className(), ['golongan_asesor' => 'golongan_asesor']);
    }

    /**
     * @inheritdoc
     * @return RefGolonganAsesorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefGolonganAsesorQuery(get_called_class());
    }
}
