<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PsikotesData]].
 *
 * @see PsikotesData
 */
class PsikotesDataQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PsikotesData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PsikotesData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}