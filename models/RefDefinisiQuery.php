<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RefDefinisi]].
 *
 * @see RefDefinisi
 */
class RefDefinisiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RefDefinisi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RefDefinisi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
