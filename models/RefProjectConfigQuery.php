<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RefProjectConfig]].
 *
 * @see RefProjectConfig
 */
class RefProjectConfigQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RefProjectConfig[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RefProjectConfig|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
