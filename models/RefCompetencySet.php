<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_competency_set".
 *
 * @property integer $id
 * @property string $competency_set_name
 * @property string $competency_list
 *
 * @property Observation[] $observations
 * @property PegawaiCompetency[] $pegawaiCompetencies
 * @property RefCompetency[] $refCompetencies
 */
class RefCompetencySet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_competency_set';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competency_set_name', 'competency_list'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'competency_set_name' => Yii::t('app', 'Competency Set Name'),
            'competency_list' => Yii::t('app', 'Competency List'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservations()
    {
        return $this->hasMany(Observation::className(), ['competency_set_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiCompetencies()
    {
        return $this->hasMany(PegawaiCompetency::className(), ['competency_set_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefCompetencies()
    {
        return $this->hasMany(RefCompetency::className(), ['competency_set_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return RefCompetencySetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefCompetencySetQuery(get_called_class());
    }
}
