<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pegawai_ext".
 *
 * @property string $id
 * @property string $nip
 * @property string $key
 * @property string $value
 */
class PegawaiExt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pegawai_ext';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nip', 'key', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nip' => Yii::t('app', 'Nip'),
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @inheritdoc
     * @return PegawaiExtQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PegawaiExtQuery(get_called_class());
    }
}
