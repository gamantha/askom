<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "result_competency".
 *
 * @property string $observation_id
 * @property string $competency_set_id
 * @property string $competency
 * @property string $sub_competency
 * @property string $result
 * @property string $description
 *
 * @property Observation $observation
 * @property RefCompetency $competencySet
 */
class ResultCompetency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'result_competency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['observation_id', 'competency_set_id', 'competency', 'sub_competency'], 'required'],
            [['observation_id'], 'integer'],
            [['competency_set_id', 'competency', 'sub_competency', 'result', 'description'], 'string', 'max' => 255],
            [['observation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Observation::className(), 'targetAttribute' => ['observation_id' => 'id']],
            [['competency_set_id', 'competency', 'sub_competency'], 'exist', 'skipOnError' => true, 'targetClass' => RefCompetency::className(), 'targetAttribute' => ['competency_set_id' => 'competency_set', 'competency' => 'competency', 'sub_competency' => 'sub_competency']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'observation_id' => Yii::t('app', 'Observation ID'),
            'competency_set_id' => Yii::t('app', 'Competency Set ID'),
            'competency' => Yii::t('app', 'Competency'),
            'sub_competency' => Yii::t('app', 'Sub Competency'),
            'result' => Yii::t('app', 'Result'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservation()
    {
        return $this->hasOne(Observation::className(), ['id' => 'observation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetencySet()
    {
        return $this->hasOne(RefCompetency::className(), ['competency_set' => 'competency_set_id', 'competency' => 'competency', 'sub_competency' => 'sub_competency']);
    }

    /**
     * @inheritdoc
     * @return ResultCompetencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ResultCompetencyQuery(get_called_class());
    }
}
