<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "psikotes_result".
 *
 * @property string $id
 * @property string $assessment_id
 * @property string $no_meja
 * @property integer $pegawai_id
 * @property string $kode_buku
 * @property string $tanggal_tes
 * @property string $kode_daerah
 * @property string $asesor_id
 *
 * @property Assessment $assessment
 * @property Pegawai $pegawai
 * @property PsikotesSubtestResult[] $psikotesSubtestResults
 */
class PsikotesResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psikotes_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['assessment_id', 'pegawai_id'], 'integer'],
            [['no_meja', 'kode_buku', 'tanggal_tes', 'kode_daerah', 'asesor_id'], 'string', 'max' => 255],
            [['assessment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Assessment::className(), 'targetAttribute' => ['assessment_id' => 'id']],
            [['pegawai_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'assessment_id' => Yii::t('app', 'Assessment ID'),
            'no_meja' => Yii::t('app', 'No Meja'),
            'pegawai_id' => Yii::t('app', 'Pegawai ID'),
            'kode_buku' => Yii::t('app', 'Kode Buku'),
            'tanggal_tes' => Yii::t('app', 'Tanggal Tes'),
            'kode_daerah' => Yii::t('app', 'Kode Daerah'),
            'asesor_id' => Yii::t('app', 'Asesor ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessment()
    {
        return $this->hasOne(Assessment::className(), ['id' => 'assessment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsikotesSubtestResults()
    {
        return $this->hasMany(PsikotesSubtestResult::className(), ['psikotes_result_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PsikotesResultQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PsikotesResultQuery(get_called_class());
    }
}
