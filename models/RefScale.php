<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_scale".
 *
 * @property string $scale_name
 * @property integer $unscaled
 * @property integer $scaled
 */
class RefScale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_scale';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scale_name', 'unscaled'], 'required'],
            [['unscaled', 'scaled'], 'integer'],
            [['scale_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'scale_name' => Yii::t('app', 'Scale Name'),
            'unscaled' => Yii::t('app', 'Unscaled'),
            'scaled' => Yii::t('app', 'Scaled'),
        ];
    }

    /**
     * @inheritdoc
     * @return RefScaleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefScaleQuery(get_called_class());
    }
}
