<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "observation".
 *
 * @property string $id
 * @property string $assessment_id
 * @property string $warteg_depth
 * @property string $warteg_detail
 * @property string $warteg_finesse
 *
 * @property Assessment $assessment
 * @property ObservationGroup[] $observationGroups
 * @property Result[] $results
 * @property ResultCompetency[] $resultCompetencies
 * @property RefCompetency[] $competencySets
 */
class Observation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'observation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['assessment_id'], 'integer'],
            [['warteg_depth', 'warteg_detail', 'warteg_finesse'], 'string', 'max' => 255],
            [['assessment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Assessment::className(), 'targetAttribute' => ['assessment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'assessment_id' => Yii::t('app', 'Assessment ID'),
            'warteg_depth' => Yii::t('app', 'Warteg Depth'),
            'warteg_detail' => Yii::t('app', 'Warteg Detail'),
            'warteg_finesse' => Yii::t('app', 'Warteg Finesse'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessment()
    {
        return $this->hasOne(Assessment::className(), ['id' => 'assessment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservationGroups()
    {
        return $this->hasMany(ObservationGroup::className(), ['observation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(Result::className(), ['observation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultCompetencies()
    {
        return $this->hasMany(ResultCompetency::className(), ['observation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetencySets()
    {
        return $this->hasMany(RefCompetency::className(), ['competency_set' => 'competency_set_id', 'competency' => 'competency', 'sub_competency' => 'sub_competency'])->viaTable('result_competency', ['observation_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ObservationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ObservationQuery(get_called_class());
    }
}
