<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "result".
 *
 * @property integer $id
 * @property string $observation_id
 * @property string $assessor_id
 * @property string $kode_daerah
 *
 * @property Asesor $assessor
 * @property Observation $observation
 */
class Result extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['observation_id'], 'integer'],
            [['assessor_id', 'kode_daerah'], 'string', 'max' => 255],
            [['assessor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asesor::className(), 'targetAttribute' => ['assessor_id' => 'id']],
            [['observation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Observation::className(), 'targetAttribute' => ['observation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'observation_id' => Yii::t('app', 'Observation ID'),
            'assessor_id' => Yii::t('app', 'Assessor ID'),
            'kode_daerah' => Yii::t('app', 'Kode Daerah'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessor()
    {
        return $this->hasOne(Asesor::className(), ['id' => 'assessor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservation()
    {
        return $this->hasOne(Observation::className(), ['id' => 'observation_id']);
    }

    /**
     * @inheritdoc
     * @return ResultQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ResultQuery(get_called_class());
    }
}
