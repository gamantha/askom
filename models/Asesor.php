<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asesor".
 *
 * @property string $id
 * @property string $golongan_asesor
 *
 * @property RefGolonganAsesor $golonganAsesor
 * @property PsikotesResult[] $psikotesResults
 * @property Result[] $results
 */
class Asesor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asesor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'golongan_asesor'], 'string', 'max' => 255],
            [['golongan_asesor'], 'exist', 'skipOnError' => true, 'targetClass' => RefGolonganAsesor::className(), 'targetAttribute' => ['golongan_asesor' => 'golongan_asesor']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'golongan_asesor' => Yii::t('app', 'Golongan Asesor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGolonganAsesor()
    {
        return $this->hasOne(RefGolonganAsesor::className(), ['golongan_asesor' => 'golongan_asesor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsikotesResults()
    {
        return $this->hasMany(PsikotesResult::className(), ['asesor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(Result::className(), ['assessor_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return AsesorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AsesorQuery(get_called_class());
    }
}
