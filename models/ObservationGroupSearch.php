<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObservationGroup;

/**
 * ObservationGroupSearch represents the model behind the search form about `app\models\ObservationGroup`.
 */
class ObservationGroupSearch extends ObservationGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'observation_id', 'meja_awal', 'meja_akhir', 'no_meja'], 'integer'],
            [['group_meja'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObservationGroup::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'observation_id' => $this->observation_id,
            'meja_awal' => $this->meja_awal,
            'meja_akhir' => $this->meja_akhir,
            'no_meja' => $this->no_meja,
        ]);

        $query->andFilterWhere(['like', 'group_meja', $this->group_meja]);

        return $dataProvider;
    }
}
