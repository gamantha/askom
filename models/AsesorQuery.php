<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Asesor]].
 *
 * @see Asesor
 */
class AsesorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Asesor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Asesor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
