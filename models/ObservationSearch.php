<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Observation;

/**
 * ObservationSearch represents the model behind the search form about `app\models\Observation`.
 */
class ObservationSearch extends Observation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'assessment_id'], 'integer'],
            [['warteg_depth', 'warteg_detail', 'warteg_finesse'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Observation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'assessment_id' => $this->assessment_id,
        ]);

        $query->andFilterWhere(['like', 'warteg_depth', $this->warteg_depth])
            ->andFilterWhere(['like', 'warteg_detail', $this->warteg_detail])
            ->andFilterWhere(['like', 'warteg_finesse', $this->warteg_finesse]);

        return $dataProvider;
    }
}
