<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RefGolonganPegawaiAdjustment]].
 *
 * @see RefGolonganPegawaiAdjustment
 */
class RefGolonganPegawaiAdjustmentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RefGolonganPegawaiAdjustment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RefGolonganPegawaiAdjustment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
