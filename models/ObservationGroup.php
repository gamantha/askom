<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "observation_group".
 *
 * @property string $observation_id
 * @property string $group_meja
 * @property integer $meja_awal
 * @property integer $meja_akhir
 * @property integer $no_meja
 *
 * @property Observation $observation
 */
class ObservationGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'observation_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['observation_id', 'no_meja'], 'required'],
            [['observation_id', 'meja_awal', 'meja_akhir', 'no_meja'], 'integer'],
            [['group_meja'], 'string', 'max' => 255],
            [['observation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Observation::className(), 'targetAttribute' => ['observation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'observation_id' => Yii::t('app', 'Observation ID'),
            'group_meja' => Yii::t('app', 'Group Meja'),
            'meja_awal' => Yii::t('app', 'Meja Awal'),
            'meja_akhir' => Yii::t('app', 'Meja Akhir'),
            'no_meja' => Yii::t('app', 'No Meja'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservation()
    {
        return $this->hasOne(Observation::className(), ['id' => 'observation_id']);
    }

    /**
     * @inheritdoc
     * @return ObservationGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ObservationGroupQuery(get_called_class());
    }
}
