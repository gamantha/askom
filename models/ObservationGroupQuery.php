<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ObservationGroup]].
 *
 * @see ObservationGroup
 */
class ObservationGroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ObservationGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ObservationGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
