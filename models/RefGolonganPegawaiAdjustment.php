<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_golongan_pegawai_adjustment".
 *
 * @property string $golongan_pegawai
 * @property string $competency_set_id
 * @property string $competency
 * @property string $sub_competency
 * @property string $target
 * @property string $minimum
 * @property integer $adjustment
 *
 * @property RefGolonganPegawai $golonganPegawai
 */
class RefGolonganPegawaiAdjustment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_golongan_pegawai_adjustment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['golongan_pegawai', 'competency_set_id', 'competency', 'sub_competency'], 'required'],
            [['adjustment'], 'integer'],
            [['golongan_pegawai', 'competency_set_id', 'competency', 'sub_competency', 'target', 'minimum'], 'string', 'max' => 255],
            [['golongan_pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => RefGolonganPegawai::className(), 'targetAttribute' => ['golongan_pegawai' => 'golongan_pegawai']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'golongan_pegawai' => Yii::t('app', 'Golongan Pegawai'),
            'competency_set_id' => Yii::t('app', 'Competency Set ID'),
            'competency' => Yii::t('app', 'Competency'),
            'sub_competency' => Yii::t('app', 'Sub Competency'),
            'target' => Yii::t('app', 'Target'),
            'minimum' => Yii::t('app', 'Minimum'),
            'adjustment' => Yii::t('app', 'Adjustment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGolonganPegawai()
    {
        return $this->hasOne(RefGolonganPegawai::className(), ['golongan_pegawai' => 'golongan_pegawai']);
    }

    /**
     * @inheritdoc
     * @return RefGolonganPegawaiAdjustmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefGolonganPegawaiAdjustmentQuery(get_called_class());
    }
}
