<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asesor_competency".
 *
 * @property string $id
 * @property integer $gol_asesor_id
 * @property integer $competency_set_id
 * @property string $competency_name
 * @property integer $value
 */
class AsesorCompetency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asesor_competency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gol_asesor_id', 'competency_set_id', 'value'], 'integer'],
            [['competency_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'gol_asesor_id' => Yii::t('app', 'Gol Asesor ID'),
            'competency_set_id' => Yii::t('app', 'Competency Set ID'),
            'competency_name' => Yii::t('app', 'Competency Name'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @inheritdoc
     * @return AsesorCompetencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AsesorCompetencyQuery(get_called_class());
    }
}
