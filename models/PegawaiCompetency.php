<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pegawai_competency".
 *
 * @property string $id
 * @property integer $gol_pegawai_id
 * @property integer $competency_set_id
 * @property string $competency_name
 * @property integer $target
 * @property integer $minimum
 *
 * @property RefGolPegawai $golPegawai
 * @property RefCompetencySet $competencySet
 */
class PegawaiCompetency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pegawai_competency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gol_pegawai_id', 'competency_set_id', 'target', 'minimum'], 'integer'],
            [['competency_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'gol_pegawai_id' => Yii::t('app', 'Gol Pegawai ID'),
            'competency_set_id' => Yii::t('app', 'Competency Set ID'),
            'competency_name' => Yii::t('app', 'Competency Name'),
            'target' => Yii::t('app', 'Target'),
            'minimum' => Yii::t('app', 'Minimum'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGolPegawai()
    {
        return $this->hasOne(RefGolPegawai::className(), ['id' => 'gol_pegawai_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetencySet()
    {
        return $this->hasOne(RefCompetencySet::className(), ['id' => 'competency_set_id']);
    }

    /**
     * @inheritdoc
     * @return PegawaiCompetencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PegawaiCompetencyQuery(get_called_class());
    }
}
