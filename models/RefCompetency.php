<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_competency".
 *
 * @property string $competency_set
 * @property string $competency
 * @property string $sub_competency
 * @property string $false
 * @property string $true
 * @property string $linkage
 * @property integer $order
 * @property string $uselinkage
 *
 * @property RefProjectConfig[] $refProjectConfigs
 * @property RefUraian[] $refUraians
 * @property ResultCompetency[] $resultCompetencies
 * @property Observation[] $observations
 */
class RefCompetency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_competency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competency_set', 'competency', 'sub_competency'], 'required'],
            [['order'], 'integer'],
            [['uselinkage'], 'string'],
            [['competency_set', 'competency', 'sub_competency', 'false', 'true', 'linkage'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'competency_set' => Yii::t('app', 'Competency Set'),
            'competency' => Yii::t('app', 'Competency'),
            'sub_competency' => Yii::t('app', 'Sub Competency'),
            'false' => Yii::t('app', 'False'),
            'true' => Yii::t('app', 'True'),
            'linkage' => Yii::t('app', 'Linkage'),
            'order' => Yii::t('app', 'Order'),
            'uselinkage' => Yii::t('app', 'Uselinkage'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefProjectConfigs()
    {
        return $this->hasMany(RefProjectConfig::className(), ['competency_set_id' => 'competency_set']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefUraians()
    {
        return $this->hasMany(RefUraian::className(), ['competency_set_id' => 'competency_set']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultCompetencies()
    {
        return $this->hasMany(ResultCompetency::className(), ['competency_set_id' => 'competency_set', 'competency' => 'competency', 'sub_competency' => 'sub_competency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservations()
    {
        return $this->hasMany(Observation::className(), ['id' => 'observation_id'])->viaTable('result_competency', ['competency_set_id' => 'competency_set', 'competency' => 'competency', 'sub_competency' => 'sub_competency']);
    }

    /**
     * @inheritdoc
     * @return RefCompetencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefCompetencyQuery(get_called_class());
    }
}
