<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RefUraian]].
 *
 * @see RefUraian
 */
class RefUraianQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RefUraian[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RefUraian|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
