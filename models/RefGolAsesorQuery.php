<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RefGolAsesor]].
 *
 * @see RefGolAsesor
 */
class RefGolAsesorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RefGolAsesor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RefGolAsesor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}