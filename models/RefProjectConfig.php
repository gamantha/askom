<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_project_config".
 *
 * @property integer $project_id
 * @property string $competency_set_id
 * @property string $psikotes_set_id
 *
 * @property Project $project
 * @property RefCompetency $competencySet
 * @property RefPsikotesSubtest $psikotesSet
 */
class RefProjectConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_project_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id'], 'integer'],
            [['competency_set_id', 'psikotes_set_id'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'project_id']],
            [['competency_set_id'], 'exist', 'skipOnError' => true, 'targetClass' => RefCompetency::className(), 'targetAttribute' => ['competency_set_id' => 'competency_set']],
            [['psikotes_set_id'], 'exist', 'skipOnError' => true, 'targetClass' => RefPsikotesSubtest::className(), 'targetAttribute' => ['psikotes_set_id' => 'psikotes_set']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => Yii::t('app', 'Project ID'),
            'competency_set_id' => Yii::t('app', 'Competency Set ID'),
            'psikotes_set_id' => Yii::t('app', 'Psikotes Set ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['project_id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetencySet()
    {
        return $this->hasOne(RefCompetency::className(), ['competency_set' => 'competency_set_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsikotesSet()
    {
        return $this->hasOne(RefPsikotesSubtest::className(), ['psikotes_set' => 'psikotes_set_id']);
    }

    /**
     * @inheritdoc
     * @return RefProjectConfigQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefProjectConfigQuery(get_called_class());
    }
}
