<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_definisi".
 *
 * @property string $key
 * @property string $value
 * @property string $type
 */
class RefDefinisi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_definisi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'type'], 'required'],
            [['value'], 'string'],
            [['key', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @inheritdoc
     * @return RefDefinisiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefDefinisiQuery(get_called_class());
    }
}
