<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "psikotes_subtest_result".
 *
 * @property string $id
 * @property string $psikotes_set
 * @property string $psikotes_subtest_name
 * @property string $psikotes_result_id
 * @property string $data
 * @property integer $true_count
 * @property integer $false_count
 * @property integer $blank_count
 *
 * @property PsikotesResult $psikotesResult
 * @property RefPsikotesSubtest $psikotesSet
 */
class PsikotesSubtestResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'psikotes_subtest_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['psikotes_result_id', 'true_count', 'false_count', 'blank_count'], 'integer'],
            [['data'], 'string'],
            [['psikotes_set', 'psikotes_subtest_name'], 'string', 'max' => 255],
            [['psikotes_result_id'], 'exist', 'skipOnError' => true, 'targetClass' => PsikotesResult::className(), 'targetAttribute' => ['psikotes_result_id' => 'id']],
            [['psikotes_set', 'psikotes_subtest_name'], 'exist', 'skipOnError' => true, 'targetClass' => RefPsikotesSubtest::className(), 'targetAttribute' => ['psikotes_set' => 'psikotes_set', 'psikotes_subtest_name' => 'subtest_name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'psikotes_set' => Yii::t('app', 'Psikotes Set'),
            'psikotes_subtest_name' => Yii::t('app', 'Psikotes Subtest Name'),
            'psikotes_result_id' => Yii::t('app', 'Psikotes Result ID'),
            'data' => Yii::t('app', 'Data'),
            'true_count' => Yii::t('app', 'True Count'),
            'false_count' => Yii::t('app', 'False Count'),
            'blank_count' => Yii::t('app', 'Blank Count'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsikotesResult()
    {
        return $this->hasOne(PsikotesResult::className(), ['id' => 'psikotes_result_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsikotesSet()
    {
        return $this->hasOne(RefPsikotesSubtest::className(), ['psikotes_set' => 'psikotes_set', 'subtest_name' => 'psikotes_subtest_name']);
    }

    /**
     * @inheritdoc
     * @return PsikotesSubtestResultQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PsikotesSubtestResultQuery(get_called_class());
    }
}
