<?php   
 /* CAT:Mathematical */

 /* pChart library inclusions */
 include("../class/pData.class.php");
 include("../class/pDraw.class.php");
 include("../class/pImage.class.php");
 include("../class/pScatter.class.php");

 /* Create the pData object */
 $myData = new pData();  

 /* Define all the data series */
 $myData->addPoints(array(2),"X1");
 $myData->addPoints(array(1),"Y1");

 /* Create the X axis */
 $myData->setAxisName(0,"Aspek Potensi Pengembangan Diri (X)");
$myData->setFixedScale(0,10,5);
 $myData->setAxisXY("0",AXIS_X);
 $myData->setAxisPosition(0,AXIS_POSITION_BOTTOM);
 $myData->setAxisDisplay(0,AXIS_FORMAT_METRIC,9);
 

 /* Create the Y axis */
 $myData->setSerieOnAxis("Y1",1);
 $myData->setAxisName(1,"Aspek Kemampuan Belajar (Y)");

 $myData->setAxisXY(1,AXIS_Y);
 $myData->setAxisPosition(1,AXIS_POSITION_LEFT);
 
 /* Create the scatter chart binding */
 $myData->setScatterSerie("X1","Y1",0);
 $myData->setScatterSerieDrawable(1,FALSE);
 $myData->setScatterSerieDrawable(2,FALSE);
 $myData->setScatterSerieDrawable(3,FALSE);


 /* Create the pChart object */
 $myPicture = new pImage(360,360,$myData);

 /* Draw the background */
 //$Settings = array("R"=>170, "G"=>183, "B"=>87, "Dash"=>1, "DashR"=>190, "DashG"=>203, "DashB"=>107);
 //$myPicture->drawFilledRectangle(0,0,800,582,$Settings);

 /* Overlay with a gradient */
 //$Settings = array("StartR"=>219, "StartG"=>231, "StartB"=>139, "EndR"=>1, "EndG"=>138, "EndB"=>68, "Alpha"=>50);
 //$myPicture->drawGradientArea(0,0,800,582,DIRECTION_VERTICAL,$Settings);

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,799,581,array("R"=>0,"G"=>0,"B"=>0));

 /* Write the title */
 //$myPicture->setFontProperties(array("FontName"=>"../fonts/Forgotte.ttf","FontSize"=>23));
 //$myPicture->drawText(55,50,"Anscombe's Quartet drawing example",array("R"=>255,"G"=>255,"B"=>255));
 //$myPicture->drawText(55,65,"This example demonstrate the importance of graphing data before analysing it. (The line of best fit is the same for all datasets)",array("FontSize"=>12,"R"=>255,"G"=>255,"B"=>255));

 /* Set the default font */
 $myPicture->setFontProperties(array("FontName"=>"../fonts/pf_arma_five.ttf","FontSize"=>6));
 
 /* Create the Scatter chart object */
 $myScatter = new pScatter($myPicture,$myData);

 /* Turn on shadow computing */
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>30));

 /* Draw the 4th chart */
 $myData->setScatterSerieDrawable(2,FALSE);
 $myData->setScatterSerieDrawable(3,TRUE);
 $myPicture->setGraphArea(30,10,350,330);
 $myScatter->drawScatterScale(array("XMargin"=>5,"YMargin"=>5,"Floating"=>TRUE,"DrawSubTicks"=>TRUE));
 $myScatter->drawScatterPlotChart();

 //$myScatter->drawScatterBestFit();

 /* Render the picture (choose the best way) */
 $myPicture->autoOutput("pictures/example.drawAnscombeQuartet.png");
?>
