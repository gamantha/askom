<?php
namespace app\config;

use Yii;
use yii\base\BootstrapInterface;

/*
/* The base class that you use to retrieve the settings from the database
*/

class settings implements BootstrapInterface {

    private $db;

    public function __construct() {
        //$this->db = Yii::$app->db;
    }

    /**
    * Bootstrap method to be called during application bootstrap stage.
    * Loads all the settings into the Yii::$app->params array
    * @param Application $app the application currently running
    */

    public function bootstrap($app) {

Yii::$app->params['settings']['jumlahmejadalamsebaris'] = 10;
Yii::$app->params['settings']['jumlahkompetensi'] = 5;
Yii::$app->params['settings']['kolomawalkompetensi'] = 3;
Yii::$app->params['settings']['barisawalkompetensi'] = 3;

Yii::$app->params['settings']['jumlahsubtest'] = 8;
Yii::$app->params['settings']['kolomawalsubtest'] = 11;
Yii::$app->params['settings']['barisawalsubtest'] = 4;


/*
$jumlahmejadalamsebaris = 10;
$jumlahkompetensi = 5;
$kolomawalkompetensi = 3;
$barisawalkompetensi = 3;
$assessment_id = 1; //BPS
$competency_set_id = $id; //BPS

*/
/*
        // Get settings from database
        $sql = $this->db->createCommand("SELECT setting_name,setting_value FROM settings");
        $settings = $sql->queryAll();

        // Now let's load the settings into the global params array

        foreach ($settings as $key => $val) {
            Yii::$app->params['settings'][$val['setting_name']] = $val['setting_value'];
        }

        */
    }

}

?>